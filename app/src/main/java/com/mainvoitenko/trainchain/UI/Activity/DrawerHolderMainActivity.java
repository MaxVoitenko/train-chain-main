package com.mainvoitenko.trainchain.UI.Activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawerHolderMainActivity {
    @BindView(R.id.nameForNavigation) TextView nameForNavigation;
    @BindView(R.id.myQualAccount) TextView myQualAccount;
    @BindView(R.id.searchButton) Button searchButton;
    @BindView(R.id.searchFriendText) EditText searchFriendText;
    @BindView(R.id.photoMy) ImageView photoMy;

    DrawerHolderMainActivity(View view) {
        ButterKnife.bind(this, view);
    }
}
