package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

public class HolderForAccount extends RecyclerView.ViewHolder {

    @BindView(R.id.friendsCount) TextView friendsCount;
    @BindView(R.id.followersCount) TextView followersCount;
    @BindView(R.id.urlTextView) TextView urlTextView;
    @BindView(R.id.cityTextView) TextView cityTextView;
    @BindView(R.id.heightTextView) TextView heightTextView;
//    @Nullable @BindView(R.id.countTrainFriend) TextView countTrainFriend;
    @BindView(R.id.buttonFriend) TextView buttonFriend;
    @BindView(R.id.buttonFollows) TextView buttonFollows;


    public HolderForAccount(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getFriendsCount() {
        return friendsCount;
    }

    public TextView getFollowersCount() {
        return followersCount;
    }

    public TextView getUrlTextView() {
        return urlTextView;
    }

    public TextView getCityTextView() {
        return cityTextView;
    }

    public TextView getHeightTextView() {
        return heightTextView;
    }

//    public TextView getCountTrainFriend() {
//        return countTrainFriend;
//    }

    public TextView getButtonFriend() {
        return buttonFriend;
    }

    public TextView getButtonFollows() {
        return buttonFollows;
    }
}