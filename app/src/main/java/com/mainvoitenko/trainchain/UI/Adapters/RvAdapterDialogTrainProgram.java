package com.mainvoitenko.trainchain.UI.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForDialogTrainProgram;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RvAdapterDialogTrainProgram extends RecyclerView.Adapter<HolderForDialogTrainProgram> {

    private ArrayList<ItemGrouper> itemsProgram;
    private OnTrainingProgramClickListener onTrainingProgramClickListener;
    public void setOnTrainingProgramClickListener(OnTrainingProgramClickListener onTrainingProgramClickListener){
        this.onTrainingProgramClickListener = onTrainingProgramClickListener;}

    public RvAdapterDialogTrainProgram(ArrayList<ItemGrouper> itemsProgram) {
        this.itemsProgram=itemsProgram;
    }

    @NonNull
    @Override
    public HolderForDialogTrainProgram onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        holder = new HolderForDialogTrainProgram(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_dialog_program_train, parent, false));
        return (HolderForDialogTrainProgram) holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderForDialogTrainProgram holder, int position) {
        final TrainPrograms trainPrograms = (TrainPrograms) itemsProgram.get(holder.getAdapterPosition());
        holder.getTextView().setText(trainPrograms.getTitle());
        Picasso.with(holder.itemView.getContext()).load(trainPrograms.getResourceBackground()).into(holder.getImageViewBack());
        holder.getTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTrainingProgramClickListener != null) {
                    onTrainingProgramClickListener.setOnTrainingProgramClickListener(trainPrograms.getProgramId(),null,-1,null,holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsProgram.size();
    }
}
