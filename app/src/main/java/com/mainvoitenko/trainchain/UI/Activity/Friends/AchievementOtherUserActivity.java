package com.mainvoitenko.trainchain.UI.Activity.Friends;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mainvoitenko.trainchain.Model.Achievements;
import com.mainvoitenko.trainchain.Providers.Net.AchievementsProvider.AchievementProvider;
import com.mainvoitenko.trainchain.Providers.Net.AchievementsProvider.FriendAchievementProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterAchievementFriend;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AchievementOtherUserActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, FriendAchievementProvider.Callback {

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    @BindView(R.id.swipe_container) SwipeRefreshLayout swipe_container;
    @BindView(R.id.progress_pic) ProgressBar progress_pic;
    @BindView(R.id.recyclerView) RecyclerView achievement_recycler_friend;

    private ArrayList<ItemGrouper> achievements;
    private String personId;
    private RvAdapterAchievementFriend recyclerAdapterForAchievementFriendEqual;

    private FriendAchievementProvider achievementProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activivty_friend_achievements);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myRef.keepSynced(true);
        swipe_container.setOnRefreshListener(this);
        progress_pic.setVisibility(View.VISIBLE);
        achievements = new ArrayList<>();
        recyclerAdapterForAchievementFriendEqual = new RvAdapterAchievementFriend(achievements);

        Intent intent = getIntent();
        personId = intent.getStringExtra(SettingsConstants.PERSON_ID_INTENT);

        checkAchievement();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        achievement_recycler_friend.setLayoutManager(mLayoutManager);
        achievement_recycler_friend.setHasFixedSize(true);
    }

    private void checkAchievement() {
        progress_pic.setVisibility(View.VISIBLE);
        achievementProvider = new FriendAchievementProvider(user.getUid(), personId, this);
        achievementProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(achievementProvider);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        checkAchievement();
    }

    @Override
    public void callingBackAchievementsFriend(ArrayList<ItemGrouper> allAchievements) {
        achievements.clear();
        if(allAchievements!=null) {
            achievements.addAll(allAchievements);
        }
        achievements.add(null);
        achievement_recycler_friend.setAdapter(recyclerAdapterForAchievementFriendEqual);
        recyclerAdapterForAchievementFriendEqual.notifyDataSetChanged();
        progress_pic.setVisibility(View.GONE);
        swipe_container.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(achievementProvider);
        super.onDestroy();
    }
}
