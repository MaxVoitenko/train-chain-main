package com.mainvoitenko.trainchain.UI.CustomView;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Chronometer;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


public class CustomChronometrDialog extends Dialog{

    @BindView(R.id.chronometer)Chronometer mChronometer;

    public CustomChronometrDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.dialog_custom_chronometr);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        mChronometer.start();
    }

    @Optional @OnClick(R.id.chronometer)
    public void OnChronometerClick(View v){
        dismiss();
    }
}
