package com.mainvoitenko.trainchain.UI.Activity.MyAccount;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;
import com.mainvoitenko.trainchain.Providers.GetPermissions;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.Providers.PhotoRedactor;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


public class ChangePictureActivity extends AppCompatActivity{

    @BindView(R.id.imageProfile) ImageView mIVpicture;
    @BindView(R.id.btn_add_picture) ImageButton mBTNaddPicture;
    @BindView(R.id.deletePicProf) ImageButton deletePicProf;
    @BindView(R.id.progress_pic) ProgressBar progress_pic;

    private static final int REQUEST_CODE_TAKE_PHOTO = 103;
    private StorageReference mStorageRef;
    private FirebaseUser user = SingleTonUser.getInstance().getUser();


    private RemoveCacheData removeCacheData;
    private PhotoRedactor photoRedactor;

    private String currentPhotoPath;
    private static final int REQUEST_IMAGE_CAPTURE = 1001;

    private String path;
    private String mImageUri = "";
    private String bitmapUri = "-1";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_image);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progress_pic.setVisibility(View.VISIBLE);
        removeCacheData = new RemoveCacheData(this);

        mStorageRef = FirebaseStorage.getInstance().getReference();

        LoadPictures loadPictures = new LoadPictures(mStorageRef,this,mIVpicture,progress_pic);
        loadPictures.addFullPicToPrifile(SettingsConstants.USER_PICTURES,user.getUid());

    }

    @Optional @OnClick(R.id.deletePicProf)
    public void DeletePictureClick(View v) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = builder.create();
        builder.setTitle(R.string.photo_dial_title);
        builder.setMessage(R.string.photo_dial);

        builder.setPositiveButton(R.string.yes_dialog  , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mStorageRef.child(SettingsConstants.USER_PICTURES).child(user.getUid()).delete();
                mIVpicture.setImageBitmap(null);
                removeCacheData.deleteOnePic(user.getUid());

            }
        });
        builder.setNegativeButton(R.string.cancel_dialog,null);
        builder.show();
    }

    @Optional  @OnClick(R.id.btn_add_picture)
    public void AddPictureClick(View v) {
        Toast.makeText(this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
        if(GetPermissions.checkPermissions(this)){
            addPhoto();
        }
    }

    //Метод для добавления фото
    private void addPhoto() {
            try {
                List<Intent> intentList = new ArrayList<>();
                Intent chooserIntent = null;
                Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File photoFile = null;
                photoRedactor = new PhotoRedactor(this);
                photoFile = photoRedactor.createImageFile();
                currentPhotoPath = photoFile.getAbsolutePath();
                if (photoFile != null && takePhotoIntent.resolveActivity(getPackageManager()) != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.mainvoitenko.trainchain.fileprovider",
                            photoFile);
                    takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);


                intentList = addIntentsToList(this, intentList, pickIntent);
                intentList = addIntentsToList(this, intentList, takePhotoIntent);

                if (!intentList.isEmpty()) {
                    chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),"Choose your image source");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[]{}));
                }
                startActivityForResult(chooserIntent, REQUEST_CODE_TAKE_PHOTO);
            }
        } catch (IOException e) {
                e.printStackTrace();
            }
    }
    //Получаем абсолютный путь файла из Uri
    private String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }

    private List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode , resultCode , data);
        switch (requestCode){
            case REQUEST_CODE_TAKE_PHOTO:
                if(resultCode == RESULT_OK) {
                    if (data != null && data.getData() != null) {
                        mImageUri = getRealPathFromURI(data.getData());
                        bitmapUri = String.valueOf(photoRedactor.compressBitmap(Uri.parse(mImageUri),path));
                        Picasso.with(this).load(bitmapUri).into(mIVpicture);

                     } else if (mImageUri != null) {
                        photoRedactor.showFiles(getFilesDir());
//                        mImageUri = Uri.fromFile(mTempPhoto).toString();
                        bitmapUri = photoRedactor.compressBitmap(Uri.parse(currentPhotoPath),path).toString();
                        Picasso.with(this).load(bitmapUri).into(mIVpicture);
                       }
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.okay_change_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                onBackPressed();
                break;
            default:
                if(bitmapUri!= null){
                    uploadFileInFireBaseStorage(bitmapUri, this);
                }else{
                    onBackPressed();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void uploadFileInFireBaseStorage (String uri, final Context context){
        progress_pic.setVisibility(View.VISIBLE);
        UploadTask uploadTask = mStorageRef.child(SettingsConstants.USER_PICTURES).child(user.getUid()).putFile(Uri.parse(uri));//todo here
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred());
                removeCacheData.deleteOnePic(user.getUid());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                context.getContentResolver().delete(Uri.parse(bitmapUri), null, null);
//                Uri donwoldUri = taskSnapshot.getMetadata().getDownloadUrl();
                progress_pic.setVisibility(View.GONE);
                onBackPressed();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progress_pic.setVisibility(View.GONE);
                onBackPressed();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isCameraPermissionGranted = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean isWritePermissionGranted = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if(isCameraPermissionGranted && isWritePermissionGranted) {
            addPhoto();
        }
    }
}