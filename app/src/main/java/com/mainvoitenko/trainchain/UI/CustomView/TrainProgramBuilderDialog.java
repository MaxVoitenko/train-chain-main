package com.mainvoitenko.trainchain.UI.CustomView;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.mainvoitenko.trainchain.Interface.CallbackInterface.CallBackDialogTrainProgram;
import com.mainvoitenko.trainchain.Interface.Clickers.OnDialogTrainProgramTwoClickListener;
import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.MyTrainProgramsProvider;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.TrainProgramsTxtProvider;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;
import com.mainvoitenko.trainchain.Providers.TrainingHard;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterDialogTrainProgram;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterDialogTrainProgramsTwo;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class TrainProgramBuilderDialog extends DialogFragment implements MyTrainProgramsProvider.Callback
                                        ,OnTrainingProgramClickListener
                                        ,TrainProgramsTxtProvider.Callback
                                        ,OnDialogTrainProgramTwoClickListener{

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.cancel) Button cancel;
    @BindView(R.id.progress_pic) ProgressBar progress_pic;

    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();
    private CallBackDialogTrainProgram callBackDialogTrainProgram;
    private TrainProgramsTxtProvider trainProgramsTxtProvider = null;
    private MyTrainProgramsProvider myTrainProgramsProvider= null;

    public void setCallBackDialogTrainProgram(CallBackDialogTrainProgram callBackDialogTrainProgram){
        this.callBackDialogTrainProgram = callBackDialogTrainProgram;}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_train_programs, container, false);
        ButterKnife.bind(this, v);

        progress_pic.setVisibility(View.GONE);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        if(!sharedPref.getBoolean(getString(R.string.Update_with_autoAdder_shared_prefere), false)){
            RemoveCacheData removeCacheData = new RemoveCacheData(getActivity());
            removeCacheData.deleteAllPrograms();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getString(R.string.Update_with_autoAdder_shared_prefere), true);
        editor.apply();}

        programLoader();
        return v;
    }

    @Optional @OnClick({R.id.cancel})
    public void onCancelClick(View v){
        if(trainProgramsTxtProvider==null){
            dismiss();
        }else{
            myRef.removeEventListener(myTrainProgramsProvider);
            trainProgramsTxtProvider = null;
            myTrainProgramsProvider=null;
            programLoader();
        }
    }

    private void programLoader(){
        progress_pic.setVisibility(View.VISIBLE);
        myTrainProgramsProvider = new MyTrainProgramsProvider(SingleTonUser.getInstance().getUser().getUid());
        myTrainProgramsProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(myTrainProgramsProvider);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public void callingBackTrainProgram(ArrayList<ItemGrouper> itemsProgram) {
        progress_pic.setVisibility(View.GONE);
        for (int i = 0; i < itemsProgram.size(); i++) {
            if (((TrainPrograms) itemsProgram.get(i)).getAutoAdder().equals("-")) {
                itemsProgram.remove(i);
                i--;
            }
        }
        if (itemsProgram != null) {
            if (itemsProgram.size() == 0) {
                Toast.makeText(getActivity(), R.string.no_train_program_dialog, Toast.LENGTH_SHORT).show();
                dismiss();
            } else {
                RvAdapterDialogTrainProgram rvAdapterDialogTrainProgram = new RvAdapterDialogTrainProgram(itemsProgram);
                rvAdapterDialogTrainProgram.setOnTrainingProgramClickListener(this);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(rvAdapterDialogTrainProgram);
            }
        }
    }

    @Override
    public void setOnTrainingProgramClickListener(String idTrain, View v, int type, String title, int position) {
        progress_pic.setVisibility(View.VISIBLE);
        trainProgramsTxtProvider = new TrainProgramsTxtProvider(getActivity());
        trainProgramsTxtProvider.registerCallBack(this);
        trainProgramsTxtProvider.loadTxtProgram(idTrain);
    }

    @Override
    public void callingBackTrainProgramsTxt(StringBuilder stringBuilder) {
        progress_pic.setVisibility(View.GONE);
        if (stringBuilder != null) {
            ArrayList<String> itemsDay = getTrainings(stringBuilder);
            RvAdapterDialogTrainProgramsTwo rvAdapterDialogTrainProgramsTwo = new RvAdapterDialogTrainProgramsTwo(itemsDay);
            rvAdapterDialogTrainProgramsTwo.setOnDialogTrainProgramTwoClickListener(this);
            recyclerView.setAdapter(rvAdapterDialogTrainProgramsTwo);
        }
    }


    private ArrayList<String> getTrainings(StringBuilder stringBuilder){
        ArrayList<String> itemsDay = new ArrayList<>();
        String s = stringBuilder.toString();
        Pattern p = Pattern.compile("1\\. ");
        for (String retval : s.split("\n\n")) {
            Matcher m = p.matcher(retval);
            if(m.find()) {
                    itemsDay.add(retval);}}
    return itemsDay;}

    @Override
    public void OnDialogTrainProgramTwo(String programDay) {
        if (callBackDialogTrainProgram != null) {
            callBackDialogTrainProgram.OnCallBackDialogTrainProgram(programDay);
            dismiss();
        }
    }

    @Override
    public void onStop() {
        myRef.removeEventListener(myTrainProgramsProvider);
        super.onStop();
    }
}