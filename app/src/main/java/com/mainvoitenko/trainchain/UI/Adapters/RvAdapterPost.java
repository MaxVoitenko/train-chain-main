package com.mainvoitenko.trainchain.UI.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Account.Activity.SearchAccountActivity;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.Training.Entity.CardioEx;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.Entity.HardEx;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForAccountFriend;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForPost;
import com.mainvoitenko.trainchain.Account.Entity.AccountFriend;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.Providers.PhotoRedactor;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.Interface.Clickers.OnPictureClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


public class RvAdapterPost extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ItemGrouper> items = new ArrayList();
    private final int TYPE_ITEM_HARD = 1;
    private final int TYPE_ITEM_CARDIO = 2;
    private final int TYPE_ITEM_GROUP = 3;
    private final int TYPE_ITEM_FRIEND_ACCOUNT = 4;
    private final int MOREE = 1408;
    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private Context context;
    private OnUniversalClick onUniversalClick;
    private OnPictureClickListener onPictureClickListener;
    private StorageReference mStorageRef;
    private int myEqual = 1;
    private Boolean withpic;

    public RvAdapterPost(ArrayList<ItemGrouper> items, Context context, boolean withPic) {
        this.items = items;
        this.context = context;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        this.withpic = withPic;
    }

    public RvAdapterPost(ArrayList<ItemGrouper> items, Context context) {
        this(items, context, true);
    }

    public void getMyEqual(int myEqual) {
        this.myEqual = myEqual;
    }

    public void setOnUniversalClick(OnUniversalClick onUniversalClick) {
        this.onUniversalClick = onUniversalClick;
    }

    public void setOnPictureClickListener(OnPictureClickListener onPictureClickListener) {
        this.onPictureClickListener = onPictureClickListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_ITEM_HARD:
                holder = new HolderForPost(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_post_hard, parent, false));
                break;
            case TYPE_ITEM_CARDIO:
                holder = new HolderForPost(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_post_cardio, parent, false));
                break;
            case TYPE_ITEM_GROUP:
                holder = new HolderForPost(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_post_group, parent, false));
                break;
            case TYPE_ITEM_FRIEND_ACCOUNT:
                holder = new HolderForAccountFriend(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_account_friend, parent, false));
                break;
            default:
                holder = new MoreItemss(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_moreee, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int type = getItemViewType(holder.getAdapterPosition());

        if (type == TYPE_ITEM_FRIEND_ACCOUNT) {
            final HolderForAccountFriend holderForAccountFriend = (HolderForAccountFriend) holder;
            final AccountFriend friendAccount = (AccountFriend) items.get(holderForAccountFriend.getAdapterPosition());

//                holderForAccountFriend.getNameTextView().setText(friendAccount.getName());
            holderForAccountFriend.getCityTextView().setText(friendAccount.getCityGym());
            holderForAccountFriend.getHeightTextView().setText(friendAccount.getHeightWeight());
            holderForAccountFriend.getCountTrainFriend().setText(MessageFormat.format("{0} '(' {1} ')'", holderForAccountFriend.itemView.getResources().getString(R.string.count_training_friends), friendAccount.getCountTrain()));
            holderForAccountFriend.getFollowersCount().setText(friendAccount.getCountMyFollowers());
            View.OnClickListener followClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SearchAccountActivity.class);
                    intent.putExtra(SettingsConstants.SEARCH_ID_INTENT_STRING, friendAccount.getPersonId());
                    intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.MY_FOLLOWERS);
                    context.startActivity(intent);
                }
            };
            holderForAccountFriend.getFollowersCount().setOnClickListener(followClick);
            holderForAccountFriend.getButtonFollows().setOnClickListener(followClick);

            holderForAccountFriend.getFriendsCount().setText(friendAccount.getCountMyFriends());
            View.OnClickListener friendClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SearchAccountActivity.class);
                    intent.putExtra(SettingsConstants.SEARCH_ID_INTENT_STRING, friendAccount.getPersonId());
                    intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.MY_FRIEND);
                    context.startActivity(intent);
                }
            };
            holderForAccountFriend.getFriendsCount().setOnClickListener(friendClick);
            holderForAccountFriend.getButtonFriend().setOnClickListener(friendClick);


            updateAccount(holderForAccountFriend, friendAccount);


        } else if (type == MOREE) {
            MoreItemss holderMore = (MoreItemss) holder;
        } else {
            final HolderForPost holderFPost = (HolderForPost) holder;
            FullTraining fullTraining = (FullTraining) items.get(holderFPost.getAdapterPosition());
            showOtherData(fullTraining, holderFPost);
            showMenuPost(holderFPost);
            switch (type) {
                case TYPE_ITEM_HARD: {
                    holderFPost.getHardLiner().setText("");
                    showHardPart(fullTraining.getHardEx(), holderFPost.getHardLiner());
                    break;
                }
                case TYPE_ITEM_CARDIO: {
                    holderFPost.getCardioLiner().setText("");
                    showCardPart(fullTraining.getCardioEx(), holderFPost.getCardioLiner());
                    break;
                }
                case TYPE_ITEM_GROUP: {
                    holderFPost.getCardioLiner().setText("");
                    holderFPost.getHardLiner().setText("");
                    showCardPart(fullTraining.getCardioEx(), holderFPost.getCardioLiner());
                    showHardPart(fullTraining.getHardEx(), holderFPost.getHardLiner());
                    break;
                }
            }
        }
    }

    private void showHardPart(List<HardEx> hardExList, TextView textView) {
        List<String> list = new ArrayList<>();
        String[] nameExHard = new String[hardExList.size()];
        String[] timeRestExHard = new String[hardExList.size()];
        List<List<Integer>> countRestExHard = new ArrayList<>();
        List<List<Float>> weightRestExHard = new ArrayList<>();
        for (int i = 0; i < hardExList.size(); i++) {
            nameExHard[i] = hardExList.get(i).getNameExercise();
            timeRestExHard[i] = hardExList.get(i).getTimerExercise();
            for (int d = 0; d < SettingsConstants.COUNT_FOR_HARD_EXERCISE; d++) {
                countRestExHard.add(i, new ArrayList<>(hardExList.get(i).getMyExerciseCount()));
                weightRestExHard.add(i, new ArrayList<>(hardExList.get(i).getMyExerciseWeght()));
            }
            list.add((i + 1) + ". " + nameExHard[i] + " (" + timeRestExHard[i] + ") ");

            int sendDataWithoutZero = 0;
            for (int tp = 0; tp < SettingsConstants.COUNT_FOR_HARD_EXERCISE; tp++) {
                if (weightRestExHard.get(i).get(sendDataWithoutZero) != 00 && countRestExHard.get(i).get(sendDataWithoutZero) != 0) {
                    sendDataWithoutZero++;
                }
            }
            switch (sendDataWithoutZero) {
                case 0:
                    list.add("no value");
                    break;
                case 1:
                    list.add(MessageFormat.format("    {0}x{1}", weightRestExHard.get(i).get(0), countRestExHard.get(i).get(0)));
                    break;
                case 2:
                    list.add(MessageFormat.format("    {0}x{1}  {2}x{3}", weightRestExHard.get(i).get(0), countRestExHard.get(i).get(0),
                            weightRestExHard.get(i).get(1), countRestExHard.get(i).get(1)));
                    break;
                case 3:
                    list.add(MessageFormat.format("    {0}x{1}  {2}x{3}  {4}x{5}", weightRestExHard.get(i).get(0), countRestExHard.get(i).get(0),
                            weightRestExHard.get(i).get(1), countRestExHard.get(i).get(1), weightRestExHard.get(i).get(2), countRestExHard.get(i).get(2)));
                    break;
                case 4:
                    list.add(MessageFormat.format("    {0}x{1}  {2}x{3}  {4}x{5}  {6}x{7}", weightRestExHard.get(i).get(0), countRestExHard.get(i).get(0),
                            weightRestExHard.get(i).get(1), countRestExHard.get(i).get(1), weightRestExHard.get(i).get(2), countRestExHard.get(i).get(2),
                            weightRestExHard.get(i).get(3), countRestExHard.get(i).get(3)));
                    break;
                case 5:
                    list.add(MessageFormat.format("    {0}x{1}  {2}x{3}  {4}x{5}  {6}x{7}  {8}x{9}",
                            weightRestExHard.get(i).get(0), countRestExHard.get(i).get(0),
                            weightRestExHard.get(i).get(1), countRestExHard.get(i).get(1), weightRestExHard.get(i).get(2), countRestExHard.get(i).get(2),
                            weightRestExHard.get(i).get(3), countRestExHard.get(i).get(3), weightRestExHard.get(i).get(4), countRestExHard.get(i).get(4)));
                    break;
                case 6:
                    list.add(MessageFormat.format("    {0}x{1}  {2}x{3}  {4}x{5}  {6}x{7}  {8}x{9}  {10}x{11}",
                            weightRestExHard.get(i).get(0), countRestExHard.get(i).get(0),
                            weightRestExHard.get(i).get(1), countRestExHard.get(i).get(1), weightRestExHard.get(i).get(2), countRestExHard.get(i).get(2),
                            weightRestExHard.get(i).get(3), countRestExHard.get(i).get(3), weightRestExHard.get(i).get(4), countRestExHard.get(i).get(4),
                            weightRestExHard.get(i).get(5), countRestExHard.get(i).get(5)));

                    break;
            }
        }
        for (int i = 0; i < list.size(); i++) {
            if (textView.getText().toString().isEmpty()) {
                textView.setText(list.get(i));
            } else {
                textView.setText(textView.getText() + "\n" + list.get(i).toString());
            }
        }
    }

   private void showCardPart(List<CardioEx> data, TextView textView) {
       List<String> list = new ArrayList<>();
       String[] nameExCard = new String[data.size()];
       String[] timeExCard = new String[data.size()];
       for (int i = 0; i < data.size(); i++) {
           nameExCard[i] = data.get(i).getName();
           timeExCard[i] = data.get(i).getTime();
           list.add((i+1)+ ". "+  nameExCard[i]  +", " + timeExCard[i] + ". ");
       }
       for (int i = 0; i < list.size(); i++) {
            if (textView.getText().toString().isEmpty()) {
                textView.setText(list.get(i));
            } else {
                textView.setText(textView.getText() + "\n" + list.get(i).toString());
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) != null) {
            int type = items.get(position).getItemType();
            if (type == TYPE_ITEM_HARD) return TYPE_ITEM_HARD;
            else if (type == TYPE_ITEM_CARDIO) return TYPE_ITEM_CARDIO;
            else if (type == TYPE_ITEM_GROUP) return TYPE_ITEM_GROUP;
            else if (type == TYPE_ITEM_FRIEND_ACCOUNT) return TYPE_ITEM_FRIEND_ACCOUNT;
            else return MOREE;
        } else {
            return MOREE;
        }
    }

    private void showMenuPost(final HolderForPost holderFPost) {
        if (isFriendPost(holderFPost)) {
            holderFPost.getMenu_post().setVisibility(View.GONE);
        } else {
            holderFPost.getMenu_post().setVisibility(View.VISIBLE);
            holderFPost.getMenu_post().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(holderFPost.itemView.getContext(), v);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(final MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.share_post:
                                    Toast.makeText(holderFPost.itemView.getContext(), R.string.wait_fr, Toast.LENGTH_SHORT).show();
                                    sharePost(holderFPost);
                                    return true;
                                case R.id.delete_post:
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(holderFPost.itemView.getContext());
                                    final AlertDialog alertDialog = builder.create();
                                    builder.setTitle(R.string.train_title);
                                    builder.setMessage(R.string.del_train);
                                    builder.setPositiveButton(R.string.yes_dialog, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.child(user.getUid()).child(SettingsConstants.TRAINING).child(items.get(holderFPost.getAdapterPosition()).getId()).exists()) {
                                                        myRef.child(user.getUid()).child(SettingsConstants.TRAINING).child(items.get(holderFPost.getAdapterPosition()).getId()).removeValue();
                                                        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
                                                        mStorageRef.child(SettingsConstants.TRAINING_PICTURES).child(items.get(holderFPost.getAdapterPosition()).getId()).delete();
                                                        items.remove(holderFPost.getAdapterPosition());
                                                        notifyItemRemoved(holderFPost.getAdapterPosition());
                                                        Toast.makeText(context, R.string.train_delete_toast, Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });
                                        }
                                    });
                                    builder.setNegativeButton(R.string.cancel_dialog, null);
                                    builder.show();
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.popup_for_post, popup.getMenu());
                    popup.show();
                }
            });
        }
    }

    private void sharePost(final HolderForPost holderFPost) {
        holderFPost.itemView.setDrawingCacheEnabled(true);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Bitmap mainBitmap = Bitmap.createBitmap(holderFPost.itemView.getDrawingCache());
        int nWidth = mainBitmap.getWidth();
        int nHeight = mainBitmap.getHeight();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 100;
        Bitmap bMap = BitmapFactory.decodeResource(holderFPost.itemView.getResources(), R.drawable.for_screen, options);
        int positionX = nWidth - bMap.getWidth() - 16;
        int positionY = nHeight - bMap.getHeight() - 16;
        Canvas canvas = new Canvas(mainBitmap);
        canvas.drawBitmap(bMap, positionX, positionY, paint);

        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        File imageFile = null;
        PhotoRedactor photoRedactor = new PhotoRedactor(holderFPost.itemView.getContext());
        try {
            imageFile = photoRedactor.createImageFile();
            if (photoRedactor.saveBitmapToFile(mainBitmap, imageFile)) {
                if (imageFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(holderFPost.itemView.getContext(),
                            "com.mainvoitenko.trainchain.fileprovider",
                            imageFile);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                    intent.setType("image/*");
                    holderFPost.itemView.getContext().startActivity(Intent.createChooser(intent, "Share with Friends"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showOtherData(final FullTraining fullTraining, final HolderForPost holderFPost) {
        holderFPost.getKpwNow().setText(fullTraining.getKpw());
        holderFPost.getTonnageNow().setText(fullTraining.getTonnageTraining());
        holderFPost.getIntencityNow().setText(fullTraining.getIntencityTraining());
        holderFPost.getTimeForTrain().setText(fullTraining.getTimeTraining());
        holderFPost.getMyWeightFromProfile().setText(fullTraining.getWeightTraining());
        holderFPost.getData_person_post().setText(fullTraining.getDataTraining());
        holderFPost.getPicture_training().setImageDrawable(null);
        int countEx = 0;
//        if(fullTraining.getCardioEx()!=null){}

//        if (fullTraining.getCardioEx().size() > 0) {
//            holderFPost.getExCount().setText(String.valueOf(fullTraining.getCardioEx().size()));
//            countEx = fullTraining.getCardioEx().size();
//        } else if (fullTraining.getHardEx().size() > 0) {
//            holderFPost.getExCount().setText(String.valueOf(fullTraining.getHardEx().size()));
//            countEx = fullTraining.getHardEx().size();
//        } else {
//            holderFPost.getExCount().setText(fullTraining.getCardioEx().size() + " + " + fullTraining.getHardEx().size());
//            countEx = (fullTraining.getCardioEx().size() + fullTraining.getHardEx().size());
//        }

        if (withpic) {
            LoadPictures loadPictures = new LoadPictures(mStorageRef, holderFPost.itemView.getContext()
                    , holderFPost.getPicture_training());
                loadPictures.addFullPicToPrifile(SettingsConstants.TRAINING_PICTURES,((FullTraining)items.get(holderFPost.getAdapterPosition())).getInternetId());
        }
        Drawable drawable = null;
        switch (fullTraining.getEmotion()) {
            case "0":
                drawable = context.getResources().getDrawable(R.drawable.ic_emotion1);
                break;
            case "1":
                drawable = context.getResources().getDrawable(R.drawable.ic_emotion2);
                break;
            case "2":
                drawable = context.getResources().getDrawable(R.drawable.ic_emotion3);
                break;
            case "3":
                drawable = context.getResources().getDrawable(R.drawable.ic_emotion4);
                break;
            case "4":
                drawable = context.getResources().getDrawable(R.drawable.ic_emotion5);
                break;
        }
        holderFPost.getEmotionImage().setImageDrawable(drawable);

        holderFPost.getPicture_training().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onPictureClickListener != null && holderFPost.getPicture_training().getDrawable() != null) {
                    onPictureClickListener.setOnPictureClickListener(fullTraining.getBitmapUri()
                            , holderFPost.getPicture_training()
                            , holderFPost.getAdapterPosition());
                }
            }
        });
//                if(!isFriendPost(holderFPost) || (myEqual== SettingsConstants.QUAL_TRAINER && isFriendPost(holderFPost))) {
//                    getColorOtherData(holderFPost, fullTraining, countEx);
//                }
//            }
//        }).run();
    }

//    private void getColorOtherData(HolderForPost holderFPost, FullTraining trainingOtherData, int countEx) {
//        holderFPost.getKpwNow().setTextColor(context.getResources().getColor(R.color.level_midle));
//        holderFPost.getTonnageNow().setTextColor(context.getResources().getColor(R.color.level_midle));
//        holderFPost.getIntencityNow().setTextColor(context.getResources().getColor(R.color.level_midle));
//        holderFPost.getMyWeightFromProfile().setTextColor(context.getResources().getColor(R.color.level_midle));
//        holderFPost.getExCount().setTextColor(context.getResources().getColor(R.color.level_midle));
//        holderFPost.getTimeForTrain().setTextColor(context.getResources().getColor(R.color.level_midle));
//        if ((holderFPost.getAdapterPosition() != 0)) {
//            if (items.get(holderFPost.getAdapterPosition() - 1) != null) {
//                FullTraining trainOtherData = null;
//                int countExBefore = 0;
//                switch (getItemViewType(holderFPost.getAdapterPosition() - 1)) {
//                    case MOREE:
//                        return;
//                    case TYPE_ITEM_FRIEND_ACCOUNT:
//                        return;
//                    case TYPE_ITEM_HARD:
//                        BaseHardProvider loadHard = (BaseHardProvider) items.get(holderFPost.getAdapterPosition() - 1);
//                        ArrayList<String> dataOne = loadHard.getExerciseHard();
////                        trainOtherData = loadHard.getOtherData();
//                        countExBefore = dataOne.size() / 2;
//                        break;
//                    case TYPE_ITEM_CARDIO:
//                        FullTraining trainingData = (FullTraining) items.get(holderFPost.getAdapterPosition());
//                        getDataThree(trainingData, holderFPost);
//                        Log.d("mLog", "hehehehe");
////                        BaseCardioProvider baseCardioProvider = (BaseCardioProvider) items.get(holderFPost.getAdapterPosition() - 1);
////                        ArrayList<String> dataTwo = baseCardioProvider.getExerciseCard();
////                        trainOtherData = baseCardioProvider.getOtherData();
////                        countExBefore= dataTwo.size();
//                        break;
//                    case TYPE_ITEM_GROUP:
//                        BaseGroupProvider baseGroupProvider = (BaseGroupProvider) items.get(holderFPost.getAdapterPosition() - 1);
//                        ArrayList<String> dataOneGr = baseGroupProvider.getExerciseHard();
//                        ArrayList<String> dataTwoGr = baseGroupProvider.getExerciseCard();
//                        trainOtherData = baseGroupProvider.getTrainingOtherData();
//                        countExBefore = (dataOneGr.size() / 2) + dataTwoGr.size();
//                        break;
//                }
//                if (trainOtherData != null) {
//                    FullTraining trainingOtherDataBefore = trainOtherData;
//                    if (Float.parseFloat(trainingOtherDataBefore.getTonnageTraining()) > Float.parseFloat(trainingOtherData.getTonnageTraining())) {
//                        holderFPost.getTonnageNow().setTextColor(context.getResources().getColor(R.color.icon_red));
//                        holderFPost.getTonnageNow().setText(holderFPost.getTonnageNow().getText() + " ↓");
//                    } else if (Float.parseFloat(trainingOtherDataBefore.getTonnageTraining()) < Float.parseFloat(trainingOtherData.getTonnageTraining())) {
//                        holderFPost.getTonnageNow().setTextColor(context.getResources().getColor(R.color.level_easy));
//                        holderFPost.getTonnageNow().setText(holderFPost.getTonnageNow().getText() + " ↑");
//                    }
//                    if (Integer.parseInt(trainingOtherDataBefore.getKpw()) > Integer.parseInt(trainingOtherData.getKpw())) {
//                        holderFPost.getKpwNow().setTextColor(context.getResources().getColor(R.color.icon_red));
//                        holderFPost.getKpwNow().setText(holderFPost.getKpwNow().getText() + " ↓");
//                    } else if (Integer.parseInt(trainingOtherDataBefore.getKpw()) < Integer.parseInt(trainingOtherData.getKpw())) {
//                        holderFPost.getKpwNow().setTextColor(context.getResources().getColor(R.color.level_easy));
//                        holderFPost.getKpwNow().setText(holderFPost.getKpwNow().getText() + " ↑");
//                    }
//                    if (Float.parseFloat(trainingOtherDataBefore.getIntencityTraining()) > Float.parseFloat(trainingOtherData.getIntencityTraining())) {
//                        holderFPost.getIntencityNow().setTextColor(context.getResources().getColor(R.color.icon_red));
//                        holderFPost.getIntencityNow().setText(holderFPost.getIntencityNow().getText() + " ↓");
//                    } else if (Float.parseFloat(trainingOtherDataBefore.getIntencityTraining()) < Float.parseFloat(trainingOtherData.getIntencityTraining())) {
//                        holderFPost.getIntencityNow().setTextColor(context.getResources().getColor(R.color.level_easy));
//                        holderFPost.getIntencityNow().setText(holderFPost.getIntencityNow().getText() + " ↑");
//                    }
//                    if (Float.parseFloat(trainingOtherDataBefore.getWeightTraining()) > Float.parseFloat(trainingOtherData.getWeightTraining())) {
//                        holderFPost.getMyWeightFromProfile().setTextColor(context.getResources().getColor(R.color.icon_red));
//                        holderFPost.getMyWeightFromProfile().setText(holderFPost.getMyWeightFromProfile().getText() + " ↓");
//                    } else if (Float.parseFloat(trainingOtherDataBefore.getWeightTraining()) < Float.parseFloat(trainingOtherData.getWeightTraining())) {
//                        holderFPost.getMyWeightFromProfile().setTextColor(context.getResources().getColor(R.color.level_easy));
//                        holderFPost.getMyWeightFromProfile().setText(holderFPost.getMyWeightFromProfile().getText() + " ↑");
//                    }
//                    if (countExBefore > countEx) {
//                        holderFPost.getExCount().setTextColor(context.getResources().getColor(R.color.icon_red));
//                        holderFPost.getExCount().setText(holderFPost.getExCount().getText() + " ↓");
//                    } else if (countExBefore < countEx) {
//                        holderFPost.getExCount().setTextColor(context.getResources().getColor(R.color.level_easy));
//                        holderFPost.getExCount().setText(holderFPost.getExCount().getText() + " ↑");
//                    }
//                    if (isTimeBeforBig(trainingOtherDataBefore.getTimeTraining(), trainingOtherData.getTimeTraining()) == 1) {
//                        holderFPost.getTimeForTrain().setTextColor(context.getResources().getColor(R.color.icon_red));
//                        holderFPost.getTimeForTrain().setText(holderFPost.getTimeForTrain().getText() + " ↓");
//                    } else if (isTimeBeforBig(trainingOtherDataBefore.getTimeTraining(), trainingOtherData.getTimeTraining()) == 3) {
//                        holderFPost.getTimeForTrain().setTextColor(context.getResources().getColor(R.color.level_easy));
//                        holderFPost.getTimeForTrain().setText(holderFPost.getTimeForTrain().getText() + " ↑");
//                    }
//                }
//            }
//        }
//    }

    public int isTimeBeforBig(String timeBefore, String time) {
        int timeint = 0;
        int timeBeforeint = 0;
        String[] splits = time.split(":");
        String[] splitsBefore = timeBefore.split(":");

        timeint = (Integer.parseInt(splits[0]) * 60 + Integer.parseInt(splits[1]));
        timeBeforeint = (Integer.parseInt(splitsBefore[0]) * 60 + Integer.parseInt(splitsBefore[1]));
        if (timeBeforeint > timeint) {
            return 1;
        } else if (timeBeforeint == timeint) {
            return 2;
        } else {
            return 3;
        }
    }

    private void updateAccount(final HolderForAccountFriend holderForAccountFriend, AccountFriend friendAccount) {
        if (isTrainer(friendAccount)) {
            holderForAccountFriend.getTextQualAcc().setText(context.getResources().getString(R.string.status_trainer));
            holderForAccountFriend.getUrlTextView().setVisibility(View.VISIBLE);
            holderForAccountFriend.getUrlTextView().setPaintFlags(holderForAccountFriend.getUrlTextView().getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holderForAccountFriend.getUrlTextView().setTextColor(holderForAccountFriend.itemView.getResources().getColor(R.color.link_program));
            holderForAccountFriend.getUrlTextView().setText(friendAccount.getMyUrlAdress());
            if (holderForAccountFriend.getUrlTextView().getText().toString().length() > 4) {
                holderForAccountFriend.getUrlTextView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(holderForAccountFriend.itemView.getContext(), R.string.wait_fr, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(holderForAccountFriend.getUrlTextView().getText().toString()));
                        holderForAccountFriend.itemView.getContext().startActivity(i);
                    }
                });
            }
        } else {
            holderForAccountFriend.getTextQualAcc().setText(context.getResources().getString(R.string.status_sport));
        }
    }

    public class MoreItemss extends RecyclerView.ViewHolder implements View.OnClickListener {
        public MoreItemss(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onUniversalClick != null) {
                onUniversalClick.onUniversalClickListener();
            }
        }
    }


    private boolean isFriendPost(final HolderForPost holderFPost) {
        if (items.get(holderFPost.getAdapterPosition()).ShowButtonMenu() == SettingsConstants.POST_WITHOUT_BUTTON_MENU) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isTrainer(final AccountFriend friendAccount) {
        if (friendAccount.getQualificationAccount().equals("2")) {
            return true;
        } else {
            return false;
        }
    }

}
