package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;


public  class HolderAchievementFriend extends RecyclerView.ViewHolder {

    @Nullable @BindView(R.id.imageAchevementFr) ImageView imageAchevementFr;
    @Nullable @BindView(R.id.imageAchevementFriend) ImageView imageAchevementFriend;
    @Nullable @BindView(R.id.questionAchivementFriend) ImageView questionAchivementFriend;
    @Nullable @BindView(R.id.questionAchivement) ImageView questionAchivement;
    @Nullable @BindView(R.id.nameAchevement) TextView nameAchevement;
    @Nullable @BindView(R.id.descriptionAchevement) TextView descriptionAchevement;

    public HolderAchievementFriend(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

    public ImageView getImageAchevementFr() {
        return imageAchevementFr;
    }

    public ImageView getImageAchevementFriend() {
        return imageAchevementFriend;
    }

    public ImageView getQuestionAchivementFriend() {
        return questionAchivementFriend;
    }

    public ImageView getQuestionAchivement() {
        return questionAchivement;
    }

    public TextView getNameAchevement() {
        return nameAchevement;
    }

    public TextView getDescriptionAchevement() {
        return descriptionAchevement;
    }
}