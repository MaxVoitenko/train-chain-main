package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public  class HolderForTrainPrograms extends RecyclerView.ViewHolder{

   @BindView(R.id.titleProgram) TextView titleProgram;
   @BindView(R.id.durationProgram) TextView durationProgram;
   @BindView(R.id.goalProgram) TextView goalProgram;
   @BindView(R.id.levelProgram) TextView levelProgram;
   @BindView(R.id.authorProgram) TextView authorProgram;
   @BindView(R.id.dayInWeekProgram) TextView dayInWeekProgram;
   @BindView(R.id.likeProgram) ImageButton likeProgram;
   @BindView(R.id.likeCount) TextView likeCount;
   @BindView(R.id.showFullProgram) TextView showFullProgram;
   @BindView(R.id.imageViewBack) ImageView imageViewBack;
   @BindView(R.id.addNameAutoProgram) ImageView addNameAutoProgram;


    public HolderForTrainPrograms(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getAddNameAutoProgram() {
        return addNameAutoProgram;
    }

    public TextView getTitleProgram() {
        return titleProgram;
    }

    public TextView getDurationProgram() {
        return durationProgram;
    }

    public TextView getGoalProgram() {
        return goalProgram;
    }

    public TextView getLevelProgram() {
        return levelProgram;
    }

    public TextView getAuthorProgram() {
        return authorProgram;
    }

    public TextView getDayInWeekProgram() {
        return dayInWeekProgram;
    }

    public ImageButton getLikeProgram() {
        return likeProgram;
    }

    public TextView getLikeCount() {
        return likeCount;
    }

    public TextView getShowFullProgram() {
        return showFullProgram;
    }

    public ImageView getImageViewBack() {
        return imageViewBack;
    }
}