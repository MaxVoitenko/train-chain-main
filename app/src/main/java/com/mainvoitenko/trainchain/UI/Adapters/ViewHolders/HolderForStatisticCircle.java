package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;
import com.razerdp.widget.animatedpieview.AnimatedPieView;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class HolderForStatisticCircle extends RecyclerView.ViewHolder {

    @BindView(R.id.animatedPieView) AnimatedPieView mAnimatedPieView;
    @BindView(R.id.hard_dat) TextView hard_dat;
    @BindView(R.id.card_dat) TextView card_dat;
    @BindView(R.id.group_dat) TextView group_dat;

    public HolderForStatisticCircle(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public AnimatedPieView getmAnimatedPieView() {
        return mAnimatedPieView;
    }

    public TextView getHard_dat() {
        return hard_dat;
    }

    public TextView getCard_dat() {
        return card_dat;
    }

    public TextView getGroup_dat() {
        return group_dat;
    }
}
