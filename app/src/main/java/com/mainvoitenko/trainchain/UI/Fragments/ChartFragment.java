package com.mainvoitenko.trainchain.UI.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mainvoitenko.trainchain.Providers.Net.ChartsProvider.ChartDataProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterCharts;
import com.mainvoitenko.trainchain.Model.StatisticAchievement;
import com.mainvoitenko.trainchain.Model.StatisticMainData;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.FixAppBarLayoutBehavior;

import java.util.ArrayList;

public class ChartFragment extends Fragment implements ChartDataProvider.Callback {

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private ArrayList<ItemGrouper> chartsItems= new ArrayList<>();

    private RecyclerView rvTrainPrograms;
    private ProgressBar progress_pic;
    private TextView kpwNow;
    private TextView tonnageNow;
    private TextView countNow;
    private TextView timeNow;

    private SeekBar seekBar;
    private TextView achiewement_text;

    private RvAdapterCharts rvAdapterCharts;

    private ChartDataProvider chartDataProvider;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_chart, container, false);

        rvTrainPrograms = mView.findViewById(R.id.rvTrainProg);
        progress_pic = mView.findViewById(R.id.progress_pic);

        kpwNow = mView.findViewById(R.id.kpwNow);
        tonnageNow = mView.findViewById(R.id.tonnageNow);
        countNow = mView.findViewById(R.id.countNow);
        timeNow = mView.findViewById(R.id.timeNow);

        seekBar = mView.findViewById(R.id.seekBar);
        achiewement_text = mView.findViewById(R.id.achiewement_text);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override public boolean onTouch(View v, MotionEvent event) { return true; }});

        progress_pic.setVisibility(View.VISIBLE);

        AppBarLayout abl = mView.findViewById(R.id.appbarfix);
        ((CoordinatorLayout.LayoutParams) abl.getLayoutParams()).setBehavior(new FixAppBarLayoutBehavior());

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvTrainPrograms.setLayoutManager(mLayoutManager);
        rvTrainPrograms.setNestedScrollingEnabled(false);
        rvAdapterCharts = new RvAdapterCharts(chartsItems);

        chartDataProvider = new ChartDataProvider(user.getUid(), getContext());
        chartDataProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(chartDataProvider);

//        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//
//            }@Override public void onCancelled(DatabaseError databaseError){}});

        return mView;
    }


    @Override
    public void callingBackCharts(StatisticMainData statisticMainData,
                                  StatisticAchievement statisticAchievement,
                                  ArrayList<ItemGrouper> charts) {
        if(statisticMainData!=null) {
            kpwNow.setText(String.valueOf(statisticMainData.getFullKpw()));
            tonnageNow.setText(String.valueOf(statisticMainData.getFullTonnage()));
            countNow.setText(String.valueOf(statisticMainData.getCountTraining()));
            timeNow.setText(String.valueOf(statisticMainData.getFullTime()));
        }
        if(statisticAchievement!=null) {
            seekBar.setMax(statisticAchievement.getCount());
            seekBar.setProgress(statisticAchievement.getIsSucc());
            achiewement_text.setText(getContext().getResources().getString(R.string.friend_achievement)
                    + " (" + statisticAchievement.getIsSucc()
                    + "/" + statisticAchievement.getCount() + ")");
        }
        if(charts!=null){
            chartsItems.clear();
            chartsItems.addAll(charts);
        }
        rvTrainPrograms.setAdapter(rvAdapterCharts);
        rvAdapterCharts.notifyDataSetChanged();
        progress_pic.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        myRef.removeEventListener(chartDataProvider);
        super.onDestroy();
    }
}
