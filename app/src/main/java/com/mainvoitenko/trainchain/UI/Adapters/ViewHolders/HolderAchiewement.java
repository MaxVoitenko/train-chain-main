package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public  class HolderAchiewement extends RecyclerView.ViewHolder{

    @BindView(R.id.imageAchevement) ImageView imageAchevement;
    @BindView(R.id.questionAchivement) ImageView questionAchivement;
    @BindView(R.id.nameAchevement) TextView nameAchevement;
    @BindView(R.id.descriptionAchevement) TextView descriptionAchevement;

    public HolderAchiewement(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getImageAchevement() {
        return imageAchevement;
    }

    public ImageView getQuestionAchivement() {
        return questionAchivement;
    }

    public TextView getNameAchevement() {
        return nameAchevement;
    }

    public TextView getDescriptionAchevement() {
        return descriptionAchevement;
    }
}