package com.mainvoitenko.trainchain.UI.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Account.Activity.SearchAccountActivity;
import com.mainvoitenko.trainchain.Account.Entity.AccountMy;
import com.mainvoitenko.trainchain.Account.ApiNet.MyAccountProvider;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
//import com.mainvoitenko.trainchain.ViewTC.Activity.SearchUserActivity;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.Account.Activity.AccountDataFragment;
import com.mainvoitenko.trainchain.UI.Fragments.ChartFragment;
import com.mainvoitenko.trainchain.UI.Fragments.SettingsFragment;
import com.mainvoitenko.trainchain.UI.Fragments.SupportFramgent;
import com.mainvoitenko.trainchain.UI.Fragments.TrainProgramsFragment;
import com.mainvoitenko.trainchain.Training.Activity.TraininigZoneFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainWorkSpace extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
                                                                ,MyAccountProvider.Callback {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    private View headerNavigation;
    private  DrawerHolderMainActivity drawerHolder;

    private android.support.v4.app.FragmentTransaction fragmentTransaction;
    private FirebaseUser user;
    private DatabaseReference myRef;
    private MyAccountProvider myAccountProvider;
    private TraininigZoneFragment traininigZone = new TraininigZoneFragment();
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_work_space);
        ButterKnife.bind(this);

        headerNavigation = navigationView.getHeaderView(0);
        drawerHolder = new DrawerHolderMainActivity(headerNavigation);

        user = SingleTonUser.getInstance().getUser();
        myRef = SingleTonUser.getInstance().getMyRef();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        updataDraweData();

        //______Поиск
        drawerHolder.searchButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainWorkSpace.this, SearchAccountActivity.class);
            intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT_STRING, drawerHolder.searchFriendText.getText().toString());
            intent.putExtra(SettingsConstants.SEARCH_DATA_INTENT, SettingsConstants.JUST_SEARCH);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainWorkSpace.this
                    , new Pair<View, String>(findViewById(R.id.searchFriendText), getString(R.string.transition_name_circle)));
            ActivityCompat.startActivity(MainWorkSpace.this, intent, options.toBundle());
        });
        //____________________ для навигации
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        //____________________ для навигации

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.containerForMainWorkSpace, traininigZone);
        fragmentTransaction.commit();
    }


    @SuppressLint("CheckResult")
    @SuppressWarnings("StatementWithEmptyBody")
    @Override //выбрал итем в НавигатореБаре
    public boolean onNavigationItemSelected(final MenuItem item) {

        new Handler().postDelayed(() -> {
            AccountDataFragment accountData;
            SupportFramgent faqAsk;
            SettingsFragment settingsFragment;
            ChartFragment chartFramgent;
            TrainProgramsFragment trainProgramsFragment;
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigationMyAccount: {
                    accountData = new AccountDataFragment();
                    if (!accountData.isAdded()) {
                        fragmentTransaction.replace(R.id.containerForMainWorkSpace, accountData);
                    }}
                break;
                case R.id.navigationTrainZone: {
                    if (!traininigZone.isAdded()) {
                        fragmentTransaction.replace(R.id.containerForMainWorkSpace, traininigZone);
                    }}
                break;
                case R.id.navigationAnaliz:{
                    chartFramgent = new ChartFragment();
                    if(!chartFramgent.isAdded()) {
                        fragmentTransaction.replace(R.id.containerForMainWorkSpace, chartFramgent);
                    }}
                break;
                case R.id.navigationSaveProg:{
                    trainProgramsFragment = new TrainProgramsFragment();
                    if(!trainProgramsFragment.isAdded()) {
                        fragmentTransaction.replace(R.id.containerForMainWorkSpace, trainProgramsFragment);
                    }}
                break;
                case R.id.navigationSetting:{
                    settingsFragment = new SettingsFragment();
                    if(!settingsFragment.isAdded()) {
                        fragmentTransaction.replace(R.id.containerForMainWorkSpace, settingsFragment);
                    }}
                break;
                case R.id.navigationSupport:{
                    faqAsk = new SupportFramgent();
                    if(!faqAsk.isAdded()) {
                        fragmentTransaction.replace(R.id.containerForMainWorkSpace,faqAsk );
                    }}
                break;
            }
            fragmentTransaction.commit(); //конец транзакции
        }, 350);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else{
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            if(!traininigZone.isAdded()){
                fragmentTransaction.replace(R.id.containerForMainWorkSpace, traininigZone);
                fragmentTransaction.commit(); //конец транзакции
            }else{
                Intent intentBack = new Intent();
                setResult(RESULT_OK, intentBack);
                intentBack.putExtra(SettingsConstants.END_ACTIVITY_EXIT_INTENT, 123);
                finish();
                super.onBackPressed();
            }
        }
    }

public void moveToTrainProgram(){
    TrainProgramsFragment trainProgramsFragment;
    fragmentTransaction = getSupportFragmentManager().beginTransaction();
    trainProgramsFragment = new TrainProgramsFragment();
    if(!trainProgramsFragment.isAdded()) {
        fragmentTransaction.replace(R.id.containerForMainWorkSpace, trainProgramsFragment);
        fragmentTransaction.commit(); //конец транзакции
    }
}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AccountDataFragment.REQUEST_NEW_DATA) {
            updataDraweData();
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        if (requestCode == LookingTrainProgramActivity.REQUEST_UPDATE_TRAININGPROGRAM) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
}

    private void updataDraweData() {
        LoadPictures loadPictures = new LoadPictures(mStorageRef,this, drawerHolder.photoMy);
        loadPictures.addFullPicToPrifile(SettingsConstants.USER_PICTURES, user.getUid());
        myAccountProvider = new MyAccountProvider(user.getUid(), this);
        myAccountProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(myAccountProvider);
    }

    @Override
    public void callingBack(AccountMy accountMy, int qualification) {
        if(accountMy!=null){
            drawerHolder.nameForNavigation.setText(accountMy.getName());
        if (accountMy.getQualificationAccount().equals("2")) {
            drawerHolder.myQualAccount.setText(getResources().getString(R.string.status_trainer));
        } else {
            drawerHolder.myQualAccount.setText(getResources().getString(R.string.status_sport));
        }
    }}

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(myAccountProvider);
        super.onDestroy();
    }
}
