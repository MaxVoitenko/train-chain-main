package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderForDialogTrainProgram extends RecyclerView.ViewHolder {
    @BindView(R.id.textView) TextView textView;
    @BindView(R.id.imageViewBack) ImageView imageViewBack;

    public HolderForDialogTrainProgram(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getImageViewBack() {
        return imageViewBack;
    }

    public TextView getTextView() {
        return textView;
    }
}
