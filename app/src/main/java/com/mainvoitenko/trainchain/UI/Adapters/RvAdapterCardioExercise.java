package com.mainvoitenko.trainchain.UI.Adapters;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderCardioExercice;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Training.Entity.CardioEx;

import java.text.MessageFormat;
import java.util.List;

public class RvAdapterCardioExercise extends RecyclerView.Adapter<HolderCardioExercice> {


    private View vv;
    private List<CardioEx> nameTimeCardio;


    public RvAdapterCardioExercise(List<CardioEx> nameTimeCardio){
        this.nameTimeCardio = nameTimeCardio;
    }

    @Override
    public HolderCardioExercice onCreateViewHolder(ViewGroup parent, int viewType) {
        vv = LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_constructor_cardio_exercise, parent, false);
        HolderCardioExercice mvh = new HolderCardioExercice(vv);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final HolderCardioExercice holder, int position) {
        holder.getNameForCardio().setText(nameTimeCardio.get(position).getName());
        holder.getTimeForCardio().setText(String.valueOf(nameTimeCardio.get(position).getTime()));

        holder.getDeleteCardio().setOnClickListener(v -> {
            final AlertDialog.Builder builder = new AlertDialog.Builder(holder.itemView.getContext());
            final AlertDialog alertDialog = builder.create();
            builder.setTitle(R.string.exercise_title);
            builder.setMessage(MessageFormat.format("{0}\n{1}", holder.itemView.getResources().getString(R.string.del_exercise),
                    holder.getNameForCardio().getText()));

            builder.setPositiveButton(R.string.yes_dialog  , (dialog, which) -> {
                    nameTimeCardio.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            });
            builder.setNegativeButton(R.string.cancel_dialog,null);
            builder.show();
        });
    }

    @Override
    public int getItemCount() {return nameTimeCardio.size();}

}
