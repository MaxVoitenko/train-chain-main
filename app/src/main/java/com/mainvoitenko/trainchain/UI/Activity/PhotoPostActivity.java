package com.mainvoitenko.trainchain.UI.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class PhotoPostActivity extends AppCompatActivity {
    @BindView(R.id.picture_training) RoundedImageView imageView;
    private StorageReference mStorageRef;
    private String trainPic;
    private String profilPic;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_post);
        ButterKnife.bind(this);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null){
            trainPic = bundle.getString(SettingsConstants.PHOTO_POST_INTENT);
            if(trainPic!=null && !trainPic.isEmpty()){
//                imageView.setRadius(47);
                LoadPictures loadPictures = new LoadPictures(mStorageRef,this,imageView);
                loadPictures.addFullPicToPrifile(SettingsConstants.TRAINING_PICTURES,trainPic);
            }else{
                profilPic = bundle.getString(SettingsConstants.PHOTO_PROFILE_INTENT );
                if(profilPic!=null && !profilPic.isEmpty()){
                    LoadPictures loadPictures = new LoadPictures(mStorageRef,this,imageView);
                    loadPictures.addFullPicToPrifile(SettingsConstants.USER_PICTURES,profilPic);
                }
            }
        }
    }
    @Optional @OnClick(R.id.picture_training)
    public void imageViewClick(View v) {
        onBackPressed();
    }
}

