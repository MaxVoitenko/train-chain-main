package com.mainvoitenko.trainchain.UI.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mainvoitenko.trainchain.Interface.Clickers.OnImageCardioClick;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Model.ImageCardio;

import java.util.ArrayList;


public class RvAdapterCardioExerciseImage extends RecyclerView.Adapter<RvAdapterCardioExerciseImage.myVH6>{

    private ArrayList<ImageCardio> exerciseImages = new ArrayList<>();
    private View v;
    private OnImageCardioClick onImageClickListener;

    public RvAdapterCardioExerciseImage(ArrayList<ImageCardio> exerciseImages, OnImageCardioClick onImageClickListener){
        this.exerciseImages = exerciseImages;
        this.onImageClickListener = onImageClickListener;
    }

    @Override
    public myVH6 onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder myVH= null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_image_cardio_exercise, parent, false);
        RvAdapterCardioExerciseImage.myVH6 mvh = new RvAdapterCardioExerciseImage.myVH6(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(myVH6 holder, final int position) {
    holder.imageCardio.setImageDrawable(exerciseImages.get(holder.getAdapterPosition()).getImageResources());
    holder.imageCardio.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onImageClickListener.onImageClickForName(position);
        }
    });
    }

    @Override
    public int getItemCount() {
        return exerciseImages.size();
    }


    public  class myVH6 extends RecyclerView.ViewHolder {
        ImageView imageCardio;

        public myVH6(View itemView) {
            super(itemView);
            imageCardio = itemView.findViewById(R.id.imageardio);
        }

    }

}
