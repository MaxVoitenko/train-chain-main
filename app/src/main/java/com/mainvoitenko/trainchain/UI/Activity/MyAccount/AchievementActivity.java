package com.mainvoitenko.trainchain.UI.Activity.MyAccount;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mainvoitenko.trainchain.Providers.Net.AchievementsProvider.AchievementProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterAchievement;
import com.mainvoitenko.trainchain.Model.Achievements;
import com.mainvoitenko.trainchain.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AchievementActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AchievementProvider.Callback {

    @BindView(R.id.progress_pic) ProgressBar progress_pic;
    @BindView(R.id.swipe_container) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView achievement_recycler;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private ArrayList<Achievements> achievements;
    private RvAdapterAchievement recyclerAdapterForAchievement;

    private AchievementProvider achievementProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activivty_friend_achievements);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myRef.keepSynced(true);
        refreshLayout.setOnRefreshListener(this);

        achievements = new ArrayList<>();
        recyclerAdapterForAchievement = new RvAdapterAchievement(achievements);
        checkAchievement();
        GridLayoutManager mLayoutManager = new GridLayoutManager(this,3);

        achievement_recycler.setLayoutManager(mLayoutManager);
        achievement_recycler.setHasFixedSize(true);
    }

private void checkAchievement(){
    progress_pic.setVisibility(View.VISIBLE);
    achievementProvider = new AchievementProvider(this,user.getUid());
    achievementProvider.registerCallBack(this);
    myRef.addListenerForSingleValueEvent(achievementProvider);
}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        checkAchievement();
    }

    @Override
    public void callingBackAchievements(ArrayList<Achievements> allAchievements) {
        achievements.clear();
        if(allAchievements!=null){
            achievements.addAll(allAchievements);
        }
        achievement_recycler.setAdapter(recyclerAdapterForAchievement);
        recyclerAdapterForAchievement.notifyDataSetChanged();
        progress_pic.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(achievementProvider);
        super.onDestroy();
    }
}