package com.mainvoitenko.trainchain.UI.Activity.Auth;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.jakewharton.rxbinding3.view.RxView;
import com.mainvoitenko.trainchain.Providers.Net.CheckAchievement;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Activity.MainWorkSpace;
import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class AuthorizationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final int GOOGLE_RESULT = 1002;
    private static final int OTHER_RESULT = 123;
    private GoogleApiClient mGoogleApiClient;

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.progress_pic)
    ProgressBar progress_pic;
    @BindView(R.id.instaMe)
    ImageView instaMe;
    @BindView(R.id.googleMe)
    ImageView googleMe;
    @BindView(R.id.linkedMe)
    ImageView linkedMe;
    @BindView(R.id.authorization)
    Button authorization;
    @BindView(R.id.registration)
    Button registration;
    @BindView(R.id.googleAuth)
    CardView googleAuth;


    private DatabaseReference myRef;
    private FirebaseUser user;
    private CompositeDisposable disposable;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        ButterKnife.bind(this);

        disposable = new CompositeDisposable();
        disposable.add(RxView.clicks(authorization)
                .filter(unit -> !email.getText().toString().isEmpty())
                .filter(unit -> !password.getText().toString().isEmpty())
                .subscribe(unit -> {
                    progress_pic.setVisibility(View.VISIBLE);
                    Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
                    signln(email.getText().toString(), password.getText().toString());
                }));

        disposable.add(RxView.clicks(registration)
                .subscribe(unit -> startActivity(new Intent(AuthorizationActivity.this, Registration.class))));

        disposable.add(RxView.clicks(googleAuth)
                .subscribe(unit -> {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                    progress_pic.setVisibility(View.VISIBLE);
                    progress_pic.bringToFront();
                    Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, GOOGLE_RESULT);
                }));

        disposable.add(RxView.clicks(instaMe)
                .subscribe(unit -> {
                    Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://www.instagram.com/maxvoitenko/"));
                    startActivity(i);
                }));

        disposable.add(RxView.clicks(googleMe)
                .subscribe(unit -> {
                    Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
                    Intent emailIntent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("mailto:?subject=" + "Вопрос по приложению Train Chain"
                            + "&body=" + "" + "&to=" + "mainvoitenko@gmail.com");
                    emailIntent.setData(data);
                    startActivity(Intent.createChooser(emailIntent, "send mail"));
                }));

        disposable.add(RxView.clicks(linkedMe)
                .subscribe(unit -> {
                    Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://www.linkedin.com/in/max-voitenko/"));
                    startActivity(i);
                }));


//____________________________________________________  обработчик входа и выхода (Отслеживает состояние)
        mAuth = FirebaseAuth.getInstance();
        FirebaseAuth.AuthStateListener mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                Intent intentAuth = new Intent(AuthorizationActivity.this, MainWorkSpace.class);
                SingleTonUser.getInstance().setUser();
                SingleTonUser.getInstance().setMyRef();
                startActivityForResult(intentAuth, 123);
            }
        };
        user = mAuth.getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference();

        if (user != null) {
            Intent intentAuth;
            intentAuth = new Intent(AuthorizationActivity.this, MainWorkSpace.class);
            SingleTonUser.getInstance().setUser();
            SingleTonUser.getInstance().setMyRef();
            startActivityForResult(intentAuth, 123);
        }
//____________________________________________________ гугл подключение (Отслеживает состояние)
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))//передать свой клиентский идентификатор сервера к requestIdTokenметоду. Чтобы найти идентификатор клиента OAuth 2.0:
                .requestEmail().build();
        //методе активности входа в систему , создайте GoogleApiClientобъект с доступом к API входа в Google и указанными вами параметрами
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(AuthorizationActivity.this, R.string.auth_not_successful, Toast.LENGTH_SHORT).show();
                    }
                }).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
//____________________________________________________
    }

//    @Optional @OnClick(R.id.authorization)
//    public void authorizationClick(View v) {
//        if (email.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
//            Toast.makeText(AuthorizationActivity.this, R.string.add_free_line, Toast.LENGTH_SHORT).show();
//        } else {
//            progress_pic.setVisibility(View.VISIBLE);
//            Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
//            signln(email.getText().toString(), password.getText().toString());
//        }
//    }

//    @Optional
//    @OnClick(R.id.registration)
//    public void registrationClick(View v) {
//        startActivity(new Intent(AuthorizationActivity.this, Registration.class);
//    }

//    @Optional
//    @OnClick(R.id.googleAuth)
//    public void googleAuthClick(View v) {
//        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
//        progress_pic.setVisibility(View.VISIBLE);
//        progress_pic.bringToFront();
//        Toast.makeText(AuthorizationActivity.this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, GOOGLE_RESULT);
//    }

//    @Optional
//    @OnClick(R.id.instaMe)
//    public void instaMeClick(View v) {
//        Toast.makeText(this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
//        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse("https://www.instagram.com/maxvoitenko/"));
//        startActivity(i);
//    }

//    @Optional
//    @OnClick(R.id.googleMe)
//    public void googleMeClick(View v) {
//        Toast.makeText(this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
//        Intent emailIntent = new Intent(android.content.Intent.ACTION_VIEW);
//        Uri data = Uri.parse("mailto:?subject=" + "Вопрос по приложению Train Chain"
//                + "&body=" + "" + "&to=" + "mainvoitenko@gmail.com");
//        emailIntent.setData(data);
//        startActivity(Intent.createChooser(emailIntent, "send mail"));
//    }

//    @Optional
//    @OnClick(R.id.linkedMe)
//    public void linkedMeClick(View v) {
//        Toast.makeText(this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
//        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse("https://www.linkedin.com/in/max-voitenko/"));
//        startActivity(i);
//    }

    @Override
    protected void onStop() {
        disposable.dispose();
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OTHER_RESULT) {
            if (resultCode == RESULT_OK && data!=null) {
                    int endAllActivity = 0;
                    Bundle bundle = data.getExtras();
                    endAllActivity = bundle.getInt(SettingsConstants.END_ACTIVITY_EXIT_INTENT, 0);
                    if (endAllActivity == 123) {finish();}
            } else {
                progress_pic.setVisibility(View.GONE);
            }
        } else if (requestCode == GOOGLE_RESULT) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                progress_pic.setVisibility(View.GONE);
            }
        } else {
            progress_pic.setVisibility(View.GONE);
        }
    }

    private void signln(String email, final String password) {         //Метод !авторизация!
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {//todo here
                if (task.isSuccessful()) {               //если таск завершится успешно
                    Toast.makeText(AuthorizationActivity.this, R.string.autho_is_successful, Toast.LENGTH_SHORT).show();
                    Intent intentAuth = new Intent(AuthorizationActivity.this, MainWorkSpace.class);
                    progress_pic.setVisibility(View.GONE);
                    SingleTonUser.getInstance().setUser();
                    SingleTonUser.getInstance().setMyRef();
                    startActivityForResult(intentAuth, 123);
                } else {
                    progress_pic.setVisibility(View.GONE);
                    Toast.makeText(AuthorizationActivity.this, R.string.auth_not_successful, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {         // гугл авторизация
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            user = mAuth.getCurrentUser();
                            myRef.child(user.getUid()).runTransaction(new Transaction.Handler() {
                                @Override
                                public Transaction.Result doTransaction(MutableData mutableData) {
                                    mutableData.child(SettingsConstants.NAME).setValue(user.getDisplayName());
                                    mutableData.child(SettingsConstants.PERSONAL_ID).setValue(user.getUid());

                                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            CheckAchievement checkAchievement = new CheckAchievement(dataSnapshot, user.getUid(), AuthorizationActivity.this);
                                            checkAchievement.firstTen();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });

                                    return Transaction.success(mutableData);
                                }

                                @Override
                                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                    Toast.makeText(AuthorizationActivity.this, R.string.autho_is_successful, Toast.LENGTH_SHORT).show();
                                    Intent intentAuth = new Intent(AuthorizationActivity.this, MainWorkSpace.class);
                                    progress_pic.setVisibility(View.GONE);
                                    SingleTonUser.getInstance().setUser();
                                    SingleTonUser.getInstance().setMyRef();
                                    startActivityForResult(intentAuth, 123);
                                }
                            });

                        } else {
                            progress_pic.setVisibility(View.GONE);
                            Toast.makeText(AuthorizationActivity.this, R.string.auth_not_successful, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}