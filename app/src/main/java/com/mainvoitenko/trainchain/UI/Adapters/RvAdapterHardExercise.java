package com.mainvoitenko.trainchain.UI.Adapters;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TimePicker;

import com.mainvoitenko.trainchain.Training.Entity.HardEx;
import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForHardExercice;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class RvAdapterHardExercise extends RecyclerView.Adapter<HolderForHardExercice> {

    private List<HardEx> training;
    private View v;
    private List<Float> myExWeight= null;
    private List<Integer> myExCount= null;
    private String nameExercise= null;
    private String timerExercise= null;




    public RvAdapterHardExercise(List<HardEx> training) {
        this.training = training;
    }

    @Override
    public HolderForHardExercice onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder myVH= null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_constructor_hard_exercise, parent, false);
        HolderForHardExercice mvh = new HolderForHardExercice(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final HolderForHardExercice HolderForHardExercice, final int position) {
        HolderForHardExercice.getNameExercise().setText(training.get(HolderForHardExercice.getAdapterPosition()).getNameExercise());
        HolderForHardExercice.getTimerExerciseBtn().setText(training.get(HolderForHardExercice.getAdapterPosition()).getTimerExercise());
        myExWeight = training.get(HolderForHardExercice.getAdapterPosition()).getWeghtExercise();
        myExCount =  training.get(HolderForHardExercice.getAdapterPosition()).getCountExercise();
        String[] hard_exercise = HolderForHardExercice.itemView.getResources().getStringArray(R.array.hard_exercise);

        for(int i = 0; i< SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++) {
            HolderForHardExercice.getCount().get(i).setText(String.valueOf(myExCount.get(i)));
            HolderForHardExercice.getWeight().get(i).setText(String.valueOf(myExWeight.get(i)));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(HolderForHardExercice.itemView.getContext(), R.layout.conteiner_drop_down_text_view, hard_exercise);
        HolderForHardExercice.getNameExercise().setAdapter(adapter);

        updateEditTextTrainClick(HolderForHardExercice);
        HolderForHardExercice.getTimerExerciseBtn().setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        TimePickerDialog timePickerDialog =  new TimePickerDialog(HolderForHardExercice.itemView.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                HolderForHardExercice.getTimerExerciseBtn().setText(String.format(("%02d:%02d"),hourOfDay,minute));
                training.get(HolderForHardExercice.getAdapterPosition()).setTimerExercise(HolderForHardExercice.getTimerExerciseBtn().getText().toString());
            }
        },00,00,true);
        timePickerDialog.show();
    }
});

        removeClick(HolderForHardExercice);
       blockTrainClick(HolderForHardExercice);

    }




    @Override
    public int getItemCount() {
        return training.size();
    }



    private void blockTrainClick(final HolderForHardExercice HolderForHardExercice) {
        HolderForHardExercice.getNumberExercise().setOnClickListener(v -> {
            if(HolderForHardExercice.getNameExercise().isEnabled() == false){
                for(int i=0; i<SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++){
//                        HolderForHardExercice.getCount()[i].setEnabled(true);
                    HolderForHardExercice.getCount().get(i).setEnabled(true);
//                        HolderForHardExercice.getWeight()[i].setEnabled(true);
                    HolderForHardExercice.getWeight().get(i).setEnabled(true);
                }
                HolderForHardExercice.getNameExercise().setEnabled(true);
                HolderForHardExercice.getTimerExerciseBtn().setEnabled(true);
                HolderForHardExercice.getDeleteEx().setEnabled(true);
                HolderForHardExercice.getNumberExercise().setBackground(HolderForHardExercice.itemView.getResources().getDrawable(R.drawable.ic_lock_on));
            }else{
                for(int i=0; i<SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++){
//                        HolderForHardExercice.getCount()[i].setEnabled(false);
                    HolderForHardExercice.getCount().get(i).setEnabled(false);
//                        HolderForHardExercice.getWeight()[i].setEnabled(false);
                    HolderForHardExercice.getWeight().get(i).setEnabled(false);
                }
                HolderForHardExercice.getNameExercise().setEnabled(false);
                HolderForHardExercice.getTimerExerciseBtn().setEnabled(false);
                HolderForHardExercice.getDeleteEx().setEnabled(false);
                HolderForHardExercice.getNumberExercise().setBackground(HolderForHardExercice.itemView.getResources().getDrawable(R.drawable.ic_lock_off));
            }
        });
    }

    private void updateEditTextTrainClick(final HolderForHardExercice HolderForHardExercice) {
        TextWatcher mTW = new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                if(s.toString().equals(".")){
                    s.clear();
                }

                myExWeight=null;
                nameExercise= null;
                myExCount= null;
                timerExercise=null;
//                myExWeight = new ArrayList<>(SettingsConstants.COUNT_FOR_HARD_EXERCISE);
//                myExCount = new ArrayList<>(SettingsConstants.COUNT_FOR_HARD_EXERCISE);
                myExWeight = new ArrayList<>(Arrays.asList(0F,0F,0F,0F,0F,0F));
                myExCount = new ArrayList<>(Arrays.asList(0,0,0,0,0,0));

//                myExWeight= new float[SettingsConstants.COUNT_FOR_HARD_EXERCISE];
//                myExCount= new int[SettingsConstants.COUNT_FOR_HARD_EXERCISE];
                for (int i = 0; i < SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++) {
//                    if (!HolderForHardExercice.getCount()[i].getText().toString().equals("")) {
                    if (!HolderForHardExercice.getCount().get(i).getText().toString().equals("")) {
//                        myExCount[i] = Integer.valueOf(HolderForHardExercice.getCount()[i].getText().toString());
                        myExCount.set(i,Integer.valueOf(HolderForHardExercice.getCount().get(i).getText().toString()));
                    } else {
                        myExCount.set(i,0);
                    }
//                    if (!HolderForHardExercice.getWeight()[i].getText().toString().equals("")) {
                    if (!HolderForHardExercice.getWeight().get(i).getText().toString().equals("")) {
//                        myExWeight[i] = Float.valueOf(HolderForHardExercice.getWeight()[i].getText().toString());
                        myExWeight.set(i,Float.valueOf(HolderForHardExercice.getWeight().get(i).getText().toString()));
                    } else {
                        myExWeight.set(i,0f);
                    }
                }
                nameExercise = String.valueOf(HolderForHardExercice.getNameExercise().getText());
                timerExercise = String.valueOf(HolderForHardExercice.getTimerExerciseBtn().getText());

//                training.setExercise(HolderForHardExercice.getAdapterPosition(), nameExercise, myExWeight, myExCount, timerExercise);
                training.get(HolderForHardExercice.getAdapterPosition()).setCountExercise(myExCount);
                training.get(HolderForHardExercice.getAdapterPosition()).setWeghtExercise(myExWeight);
                training.get(HolderForHardExercice.getAdapterPosition()).setNameExercise(nameExercise);
                training.get(HolderForHardExercice.getAdapterPosition()).setTimerExercise(timerExercise);
            }
        };

        View.OnFocusChangeListener onFocusChListener= new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus== true){
                    if(v instanceof EditText){
                        ((EditText)v).setText("");
                    }}
            }
        };
        for (int i = 0; i < SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++) {
//            HolderForHardExercice.getCount()[i].addTextChangedListener(mTW);
            HolderForHardExercice.getCount().get(i).addTextChangedListener(mTW);
//            HolderForHardExercice.getCount()[i].setOnFocusChangeListener(onFocusChListener);
            HolderForHardExercice.getCount().get(i).setOnFocusChangeListener(onFocusChListener);
            HolderForHardExercice.getWeight().get(i).addTextChangedListener(mTW);
//            HolderForHardExercice.getWeight()[i].addTextChangedListener(mTW);
//            HolderForHardExercice.getWeight()[i].setOnFocusChangeListener(onFocusChListener);
            HolderForHardExercice.getWeight().get(i).setOnFocusChangeListener(onFocusChListener);
        }
        HolderForHardExercice.getNameExercise().addTextChangedListener(mTW);
    }

    private void removeClick(final HolderForHardExercice HolderForHardExercice) {
        HolderForHardExercice.getDeleteEx().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(HolderForHardExercice.itemView.getContext());
                final AlertDialog alertDialog = builder.create();
                builder.setTitle(R.string.exercise_title);
                builder.setMessage(R.string.del_exercise);

                builder.setPositiveButton(R.string.yes_dialog, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        training.deleteExercise(HolderForHardExercice.getAdapterPosition());
                        training.remove(HolderForHardExercice.getAdapterPosition());
                        notifyItemRemoved(HolderForHardExercice.getAdapterPosition());
                    }
                });
                builder.setNegativeButton(R.string.cancel_dialog,null);
                builder.show();
            }
        });
    }
}



