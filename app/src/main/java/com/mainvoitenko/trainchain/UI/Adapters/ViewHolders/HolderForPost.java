package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mainvoitenko.trainchain.R;



public class HolderForPost extends RecyclerView.ViewHolder {
    private ImageView emotionImage;
    private TextView data_person_post;
    private TextView tonnageNow;
    private TextView kpwNow;
    private TextView intencityNow;
    private TextView timeForTrain;
    private TextView exCount;
    private TextView myWeightFromProfile;
    private TextView cardioLiner;
    private TextView hardLiner;
    private Button menu_post;
    private ImageView picture_training;

    public HolderForPost(View itemView)  {
        super(itemView);
        emotionImage = itemView.findViewById(R.id.emotionImage);
        data_person_post = itemView.findViewById(R.id.data_person_post);
        tonnageNow = itemView.findViewById(R.id.tonnageNow);
        kpwNow = itemView.findViewById(R.id.kpwNow);
        intencityNow = itemView.findViewById(R.id.intencityNow);
        timeForTrain = itemView.findViewById(R.id.timeForTrain);
        exCount = itemView.findViewById(R.id.exCount);
        myWeightFromProfile = itemView.findViewById(R.id.myWeightFromProfile);
        menu_post = itemView.findViewById(R.id.menu_post);
        cardioLiner = itemView.findViewById(R.id.cardioLiner);
        hardLiner = itemView.findViewById(R.id.hardLiner);
        picture_training = itemView.findViewById(R.id.picture_training);
    }

    public ImageView getEmotionImage() {
        return emotionImage;
    }

    public TextView getData_person_post() {
        return data_person_post;
    }

    public TextView getTonnageNow() {
        return tonnageNow;
    }

    public TextView getKpwNow() {
        return kpwNow;
    }

    public TextView getIntencityNow() {
        return intencityNow;
    }

    public TextView getTimeForTrain() {
        return timeForTrain;
    }

    public TextView getExCount() {
        return exCount;
    }

    public TextView getMyWeightFromProfile() {
        return myWeightFromProfile;
    }

    public TextView getCardioLiner() {
        return cardioLiner;
    }

    public TextView getHardLiner() {
        return hardLiner;
    }

    public Button getMenu_post() {
        return menu_post;
    }

    public ImageView getPicture_training() {
        return picture_training;
    }
}