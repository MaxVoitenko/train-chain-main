package com.mainvoitenko.trainchain.UI.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterSupport;
import com.mainvoitenko.trainchain.Model.AsksAndAnswers;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.FixAppBarLayoutBehavior;

import java.util.ArrayList;


public class SupportFramgent extends Fragment implements View.OnClickListener {
    private RecyclerView rvAsk;
    private ImageButton askBtn;
    private EditText editAsk;
    private ArrayList<AsksAndAnswers> asks_and_answers = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_support, container,false);

        asks_and_answers.clear();
        rvAsk = mView.findViewById(R.id.rvAsk);
        askBtn = mView.findViewById(R.id.askBtn);
        editAsk = mView.findViewById(R.id.editAsk);
        askBtn.setOnClickListener(this);
        String[] ask_texsts = getResources().getStringArray(R.array.questions_faq); //имена
        String[] answers_texsts = getResources().getStringArray(R.array.answers_faq); //имена
        RvAdapterSupport rvAdapterSupport = new RvAdapterSupport(asks_and_answers);

        AppBarLayout abl = mView.findViewById(R.id.appbarfix);
        ((CoordinatorLayout.LayoutParams) abl.getLayoutParams()).setBehavior(new FixAppBarLayoutBehavior());

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rvAsk.setLayoutManager(mLayoutManager);
        rvAsk.setNestedScrollingEnabled(false);
        rvAsk.setHasFixedSize(true);
        ((SimpleItemAnimator) rvAsk.getItemAnimator()).setSupportsChangeAnimations(false);
        for(int i=0; i<ask_texsts.length; i++){
            asks_and_answers.add(new AsksAndAnswers(ask_texsts[i], answers_texsts[i], false));
        }
        rvAsk.setAdapter(rvAdapterSupport);
        rvAdapterSupport.notifyDataSetChanged();

        return mView;
    }
    @Override
    public void onClick(View v) {
        Toast.makeText(getContext(), R.string.wait_fr, Toast.LENGTH_SHORT).show();
        Intent emailIntent = new Intent(
                android.content.Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + "Вопрос по приложению Train Chain"
                + "&body=" + editAsk.getText().toString() + "&to=" + "mainvoitenko@gmail.com");
        emailIntent.setData(data);
        startActivity(Intent.createChooser(emailIntent, "send mail"));
    }
}
