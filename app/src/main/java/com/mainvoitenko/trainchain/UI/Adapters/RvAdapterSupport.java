package com.mainvoitenko.trainchain.UI.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolerForSupport;
import com.mainvoitenko.trainchain.Model.AsksAndAnswers;
import com.mainvoitenko.trainchain.R;

import java.util.ArrayList;

public class RvAdapterSupport extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<AsksAndAnswers> asks_and_answers = new ArrayList<>();
    private View v;

    public RvAdapterSupport(ArrayList<AsksAndAnswers> asks_and_answers) {
        this.asks_and_answers = asks_and_answers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_asks_and_answers, parent, false);
        HolerForSupport holerForSupport = new HolerForSupport(v);
        return holerForSupport;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final HolerForSupport holderAskAnswer = (HolerForSupport) holder;
        holderAskAnswer.getAskText().setText(asks_and_answers.get(holderAskAnswer.getAdapterPosition()).getAsk());
        if(asks_and_answers.get(holderAskAnswer.getAdapterPosition()).isOpen()) {
            holderAskAnswer.getAnswerText().setText(asks_and_answers.get(holderAskAnswer.getAdapterPosition()).getAnswer());
        }else{
            holderAskAnswer.getAnswerText().setText("");
        }
            holderAskAnswer.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(asks_and_answers.get(holderAskAnswer.getAdapterPosition()).isOpen()){
                    holderAskAnswer.getAnswerText().setText("");
                    asks_and_answers.get(holderAskAnswer.getAdapterPosition()).setOpen(false);
                }else{
                        holderAskAnswer.getAnswerText().setText(asks_and_answers.get(holderAskAnswer.getAdapterPosition()).getAnswer());
                    asks_and_answers.get(holderAskAnswer.getAdapterPosition()).setOpen(true);
                }
                notifyItemRangeChanged(holderAskAnswer.getAdapterPosition(), asks_and_answers.size());
                notifyItemChanged(holderAskAnswer.getAdapterPosition());

            }
        });
    }
    @Override
    public int getItemCount() {
        return asks_and_answers.size();
    }



}
