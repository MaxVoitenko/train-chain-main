package com.mainvoitenko.trainchain.UI.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderAchievementFriend;
import com.mainvoitenko.trainchain.Model.AchievementEqual;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;


public class RvAdapterAchievementFriend extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    View v;
    ArrayList<ItemGrouper> achievementEquals = new ArrayList<>();

    private final int TOOL_BAR_FOR_ACHIEVEMENT_FRIEND = 1408;
    private final int ACHIEVEMENT_FRIEND = SettingsConstants.GROUPER_FRIEND_ACHIEVEMENT;

    public RvAdapterAchievementFriend(ArrayList<ItemGrouper> achievementEquals) {
        this.achievementEquals = achievementEquals;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder= null;
        switch (viewType) {
            case ACHIEVEMENT_FRIEND:
                holder = new HolderAchievementFriend(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_achievement_friends, parent, false));
            break;
            case TOOL_BAR_FOR_ACHIEVEMENT_FRIEND:
                holder = new HolderAchievementFriend(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_achievement_toolbar, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(holder.getAdapterPosition());
        HolderAchievementFriend holderAchievementFriend = (HolderAchievementFriend) holder;
        if(type == ACHIEVEMENT_FRIEND) {
            AchievementEqual achievementEqualUpdate = (AchievementEqual) achievementEquals.get(holder.getAdapterPosition());
            holderAchievementFriend.getImageAchevementFr().setImageDrawable(achievementEqualUpdate.getResources());
            holderAchievementFriend.getImageAchevementFriend().setImageDrawable(achievementEqualUpdate.getResources());
            holderAchievementFriend.getNameAchevement().setText(achievementEqualUpdate.getName());
//            holderAchievementFriend.getDescriptionAchevement().setText(achievementEqualUpdate.getDescription());

            if (achievementEqualUpdate.getType() == 1) {
                holderAchievementFriend.getImageAchevementFr().setVisibility(View.VISIBLE);
                holderAchievementFriend.getQuestionAchivement().setVisibility(View.GONE);
                holderAchievementFriend.getNameAchevement().setTextColor(Color.YELLOW);
                holderAchievementFriend.getNameAchevement().setTextSize(20);
                holderAchievementFriend.getNameAchevement().setEnabled(true);
//                holderAchievementFriend.getDescriptionAchevement().setEnabled(true);
//                holderAchievementFriend.getDescriptionAchevement().setVisibility(View.VISIBLE);
                holderAchievementFriend.getNameAchevement().setShadowLayer(15, 0, 0, Color.BLACK);
            } else {
//                holderAchievementFriend.getDescriptionAchevement().setVisibility(View.GONE);
//                holderAchievementFriend.getDescriptionAchevement().setEnabled(false);
                holderAchievementFriend.getNameAchevement().setEnabled(false);
                holderAchievementFriend.getNameAchevement().setTextSize(15);
                holderAchievementFriend.getImageAchevementFr().setVisibility(View.GONE);
                holderAchievementFriend.getQuestionAchivement().setVisibility(View.VISIBLE);
                holderAchievementFriend.getNameAchevement().setTextColor(holderAchievementFriend.itemView.getResources().getColor(R.color.text_hard_gray));
                holderAchievementFriend.getNameAchevement().setShadowLayer(0, 0, 0, Color.BLACK);
            }

            if (achievementEqualUpdate.getTypeFriend() == 1) {
                holderAchievementFriend.getImageAchevementFriend().setVisibility(View.VISIBLE);
                holderAchievementFriend.getQuestionAchivementFriend().setVisibility(View.GONE);
            } else {
                holderAchievementFriend.getImageAchevementFriend().setVisibility(View.GONE);
                holderAchievementFriend.getQuestionAchivementFriend().setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return achievementEquals.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(achievementEquals.get(position)!=null){
        int type = achievementEquals.get(position).getItemType();
        if (type == ACHIEVEMENT_FRIEND) {return ACHIEVEMENT_FRIEND;}
        else {return TOOL_BAR_FOR_ACHIEVEMENT_FRIEND;}}
        else{return TOOL_BAR_FOR_ACHIEVEMENT_FRIEND;}
    }



}
