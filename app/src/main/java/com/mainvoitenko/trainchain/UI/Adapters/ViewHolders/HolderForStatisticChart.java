package com.mainvoitenko.trainchain.UI.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.mainvoitenko.trainchain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderForStatisticChart extends RecyclerView.ViewHolder{
    @BindView(R.id.chart_line) LineChart chart_line;

    public HolderForStatisticChart(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public LineChart getChart_line() {
        return chart_line;
    }
}