package com.mainvoitenko.trainchain.UI.Adapters;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mainvoitenko.trainchain.UI.Adapters.ViewHolders.HolderForTrainPrograms;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Interface.Clickers.OnLikeProgramClick;
import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Account.Activity.AccountOtherUserActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class RvAdapterTrainingPrograms extends RecyclerView.Adapter<HolderForTrainPrograms> {

    private ArrayList<TrainPrograms> trainPrograms;
    private View v;
    private OnLikeProgramClick onLikeProgramClick;
    private OnTrainingProgramClickListener onTrainingProgramClickListener;

    public RvAdapterTrainingPrograms(ArrayList<TrainPrograms> trainPrograms) {
        this.trainPrograms = trainPrograms;
    }

    public void setOnLikeProgramClick(OnLikeProgramClick onLikeProgramClick) {
        this.onLikeProgramClick = onLikeProgramClick; }

    public void setOnTrainingProgramClickListener(OnTrainingProgramClickListener onTrainingProgramClickListener){
        this.onTrainingProgramClickListener= onTrainingProgramClickListener;}

    @Override
    public HolderForTrainPrograms onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder myVH = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.conteiner_training_programs, parent, false);
        HolderForTrainPrograms HolderForTrainPrograms = new HolderForTrainPrograms(v);
        return HolderForTrainPrograms;
    }

    @Override
    public void onBindViewHolder(final HolderForTrainPrograms holder, int position) {
        final TrainPrograms accTrain = (TrainPrograms) trainPrograms.get(holder.getAdapterPosition());

        holder.getTitleProgram().setText(accTrain.getTitle());
        holder.getDurationProgram().setText(accTrain.getDuration());
        holder.getGoalProgram().setText(accTrain.getGoal());
        holder.getLevelProgram().setText(accTrain.getLevel());
        holder.getAuthorProgram().setText(accTrain.getAuthor());
        holder.getLikeCount().setText(accTrain.getLike());
        holder.getDayInWeekProgram().setText(accTrain.getDayInWeek());
        holder.getAddNameAutoProgram().setImageDrawable(null);
        if(!accTrain.getAutoAdder().equals("-")){
            holder.getAddNameAutoProgram().setImageDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_writing));
        }


        linkPerson(holder);
        levelColor(holder);
        LikeOrNot(holder);

        Picasso.with(holder.itemView.getContext()).load(accTrain.getResourceBackground()).into(holder.getImageViewBack());

        View.OnClickListener showProgram = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTrainingProgramClickListener != null) {
                    onTrainingProgramClickListener.setOnTrainingProgramClickListener(
                                    trainPrograms.get(holder.getAdapterPosition()).getProgramId()
                                    , holder.getImageViewBack()
                                    ,accTrain.getResourceBackground()
                                    ,trainPrograms.get(holder.getAdapterPosition()).getTitle()
                                    ,holder.getAdapterPosition());
                }
            }
        };

        holder.getShowFullProgram().setOnClickListener(showProgram);
        holder.getTitleProgram().setOnClickListener(showProgram);
        holder.getImageViewBack().setOnClickListener(showProgram);
    }

    @Override
    public int getItemCount() {
        return trainPrograms.size();
    }

    private  void  levelColor(HolderForTrainPrograms holder){
        if(trainPrograms.get(holder.getAdapterPosition()).getLevel().equals(holder.itemView.getResources().getString(R.string.level_easy_txt))) {
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.level_easy));
        }else if(trainPrograms.get(holder.getAdapterPosition()).getLevel().equals(holder.itemView.getResources().getString(R.string.level_midle_txt))){
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.level_midle));
        }else if(trainPrograms.get(holder.getAdapterPosition()).getLevel().equals(holder.itemView.getResources().getString(R.string.level_hard_txt))){
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.icon_red));
        }else{
            holder.getLevelProgram().setTextColor(holder.itemView.getResources().getColor(R.color.text_light_gray));
        }
    }

    private void LikeOrNot(final HolderForTrainPrograms holder) {
        if(trainPrograms.get(holder.getAdapterPosition()).getMyLike()== SettingsConstants.LIKE_NOW_YES){
            holder.getLikeProgram().setImageDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_add_to_favorite));
        }else{
            holder.getLikeProgram().setImageDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_remove_favorite));
        }
        holder.getLikeProgram().setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (onLikeProgramClick!= null) {
                            onLikeProgramClick.setOnLikeProgramClick(trainPrograms.get(holder.getAdapterPosition()).getProgramId(), -1);}

            }});
    }

    private void linkPerson(final HolderForTrainPrograms holder) {
        if(trainPrograms.get(holder.getAdapterPosition()).getPersonId().equals("-1")) {
            holder.getAuthorProgram().setTextColor(holder.itemView.getResources().getColor(R.color.text_light_gray));
        }else{
            holder.getAuthorProgram().setPaintFlags(holder.getAuthorProgram().getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.getAuthorProgram().setTextColor(holder.itemView.getResources().getColor(R.color.link_program));
            holder.getAuthorProgram().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(holder.itemView.getContext(), AccountOtherUserActivity.class);
                    intent.putExtra(SettingsConstants.PERSON_ID_INTENT, trainPrograms.get(holder.getAdapterPosition()).getPersonId());
                    holder.itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}