package com.mainvoitenko.trainchain.Providers;

import android.content.Context;

import com.squareup.picasso.Picasso;

import java.io.File;

import static android.os.Environment.DIRECTORY_DOCUMENTS;
import static android.os.Environment.DIRECTORY_PICTURES;

public class RemoveCacheData {
    Context context;

    public RemoveCacheData(Context context) {
        this.context = context;
    }

    public void deleteOnePic(String userId) {
        File temp = new File(context.getExternalFilesDir(DIRECTORY_PICTURES) + File.separator + "photoAccount");
        if (!temp.exists()) {
            temp.mkdirs();
        }
        final File dir = new File(temp, userId + ".jpg");
        if (dir.exists()) {
            dir.delete();
        }
        Picasso.with(context).invalidate(dir);
    }


    public void deleteAllPic(final int maximum) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                File temp = new File(context.getExternalFilesDir(DIRECTORY_PICTURES) + File.separator + "photoAccount");
                if (temp.exists()) {
                    if (getSizeCache(temp) > maximum) {
                        deleteRecursive(temp);
                    }
                }
            }
        });
        thread.start();
    }

    public void deleteAllPrograms() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                File temp = new File(context.getExternalFilesDir(DIRECTORY_DOCUMENTS) + File.separator + "trainPrograms");
                if (temp.exists()) {
                        deleteRecursive(temp);
                }
            }
        });
        thread.start();
    }

    private long getSizeCache(File file) {
        long size;
        if (file.isDirectory()) {
            size = 0;
            for (File child : file.listFiles()) {
                size += getSizeCache(child);
            }
        } else {
            size = file.length();
        }
        return size;
    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }
}
