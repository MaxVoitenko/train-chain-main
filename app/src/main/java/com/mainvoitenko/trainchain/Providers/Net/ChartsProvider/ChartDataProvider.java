package com.mainvoitenko.trainchain.Providers.Net.ChartsProvider;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Model.StatisticAchievement;
import com.mainvoitenko.trainchain.Model.StatisticChart;
import com.mainvoitenko.trainchain.Model.StatisticCircleChart;
import com.mainvoitenko.trainchain.Model.StatisticMainData;
import com.mainvoitenko.trainchain.Providers.Net.CheckAchievement;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class ChartDataProvider implements ValueEventListener {


    private Callback callback;
    public interface Callback{ void callingBackCharts(StatisticMainData statisticMainData,
                                                     StatisticAchievement statisticAchievement,
                                                     ArrayList<ItemGrouper> charts);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String personId;
    private Context context;

    public ChartDataProvider(String personId, Context context) {
        this.personId = personId;
        this.context = context;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<ItemGrouper> charts = new ArrayList<>();
        int isSucc=0;
        int MaxAchiewement =0;
        if (dataSnapshot.child(personId).child(SettingsConstants.ACHIEVEMENT).exists()) {
            isSucc= (int) dataSnapshot.child(personId).child(SettingsConstants.ACHIEVEMENT).getChildrenCount();
            MaxAchiewement = SettingsConstants.FULL_COUNT_ACHIEWEMENT;
        }
        ArrayList<Float> tonnageList = new ArrayList<>();
        ArrayList<Float> kpwList = new ArrayList<>();
        ArrayList<Float> intencityList = new ArrayList<>();
        ArrayList<Float> weightList = new ArrayList<>();
        ArrayList<String> totalTimeList = new ArrayList<>();

        if (dataSnapshot.child(personId).child(SettingsConstants.TRAINING).exists()) {
            tonnageList = getOtherData(dataSnapshot.child(personId), SettingsConstants.TONNAGE);
            kpwList = getOtherData(dataSnapshot.child(personId), SettingsConstants.KPW);
            intencityList = getOtherData(dataSnapshot.child(personId), SettingsConstants.INTENCITY);
            weightList = getOtherData(dataSnapshot.child(personId), SettingsConstants.MY_WEIGHT);
            totalTimeList =getOtherDataTime(dataSnapshot.child(personId));
        }
        StatisticMainData statisticMainData = new StatisticMainData((int)getFullData(kpwList)
                                                                    ,getFullData(tonnageList)
                                                                    ,summTime(totalTimeList)
                                                                    ,tonnageList.size());
        StatisticAchievement statisticAchievement = new StatisticAchievement(isSucc,MaxAchiewement);

        charts.add(getCirleData(dataSnapshot.child(personId)));
        summTime(totalTimeList);
        CheckAchievement checkAchievement = new CheckAchievement(dataSnapshot, personId, context);
        checkAchievement.firstKpwFull((int) getFullData(kpwList));
        checkAchievement.firstTonnageFull(getFullData(tonnageList));

        if (dataSnapshot.child(personId).child(SettingsConstants.TRAINING).exists()) {
            charts.add(new StatisticChart(tonnageList, context.getResources().getString(R.string.tonnage)));
            charts.add(new StatisticChart(kpwList, context.getResources().getString(R.string.kpw)));
            charts.add(new StatisticChart(intencityList, context.getResources().getString(R.string.intencity)));
            charts.add(new StatisticChart(weightList, context.getResources().getString(R.string.train_weight)));
        }
        callback.callingBackCharts(statisticMainData,statisticAchievement, charts);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {}

    private static String summTime(ArrayList<String> times) {
        if(times.size()!=0) {
            int total = 0;
            for (String time : times) {
                String[] splits = time.split(":");
                total += (Integer.parseInt(splits[0]) * 60 + Integer.parseInt(splits[1]));
            }
            return total / 60 + ":" + total % 60;
        }else{
            return "00:00";
        }
    }

    private float getFullData(ArrayList<Float> dataList){
        float data=0;
        for(int i=0; i<dataList.size(); i++){
            data = data + dataList.get(i);
        }
        return data;
    }

    public StatisticCircleChart getCirleData (DataSnapshot dataSnapshot){
        int hardData = 0;
        int cardData = 0;
        int groupData = 0;
        for (DataSnapshot postSnapshot : dataSnapshot.child(SettingsConstants.TRAINING).getChildren()) {
            if (!postSnapshot.child(SettingsConstants.HARD_EX).exists() &&
                    postSnapshot.child(SettingsConstants.CARDIO_EX).exists()) {
                cardData++;
            } else if (!postSnapshot.child(SettingsConstants.CARDIO_EX).exists() &&
                    postSnapshot.child(SettingsConstants.HARD_EX).exists()) {
                hardData++;
            } else if (postSnapshot.child(SettingsConstants.HARD_EX).exists() &&
                    postSnapshot.child(SettingsConstants.CARDIO_EX).exists()) {
                groupData++;
            }
        }
        return new StatisticCircleChart(hardData,cardData,groupData);
    }

    private ArrayList<Float> getOtherData(DataSnapshot dataSnapshot, String typeData) {
        ArrayList<Float> ontherData = new ArrayList();
        for (DataSnapshot postSnapshot : dataSnapshot.child(SettingsConstants.TRAINING).getChildren()) {
            ontherData.add(Float.parseFloat(postSnapshot.child(typeData).getValue().toString()));
        }
        return ontherData;
    }

    private ArrayList<String> getOtherDataTime(DataSnapshot dataSnapshot) {
        ArrayList<String> dataTime = new ArrayList();
        for (DataSnapshot postSnapshot : dataSnapshot.child(SettingsConstants.TRAINING).getChildren()) {
            dataTime.add(postSnapshot.child(SettingsConstants.TIME_TRAIN).getValue().toString());
        }
        return dataTime;
    }
}
