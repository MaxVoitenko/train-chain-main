package com.mainvoitenko.trainchain.Providers.Net.AchievementsProvider;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Model.Achievements;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class AchievementProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackAchievements(ArrayList<Achievements> allAchievements);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private Context context;
    private String personId;

    public AchievementProvider(Context context, String personId) {
        this.context = context;
        this.personId = personId;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        dataSnapshot= dataSnapshot.child(personId).child(SettingsConstants.ACHIEVEMENT);
        ArrayList<Achievements> achievements = new ArrayList<>();
        String[] achievement_name = context.getResources().getStringArray(R.array.achievement_name); //имена
        String[] achievement_description = context.getResources().getStringArray(R.array.achievement_description); //описания
        TypedArray achievement_image = context.getResources().obtainTypedArray(R.array.achievement_drawble);//картинки

        for (int i = 0; i< achievement_name.length; i++){
            if(dataSnapshot.child(String.valueOf(i)).exists()){
                achievements.add(new Achievements(achievement_name[i],achievement_description[i],achievement_image.getDrawable(i), SettingsConstants.ACHIEVEMENT_IS_DONE));
            }else{
                achievements.add(new Achievements(achievement_name[i],achievement_description[i],achievement_image.getDrawable(i),SettingsConstants.ACHIEVEMENT_IS_NOT_DONE));
            }
        }
        callback.callingBackAchievements(achievements);
    }
    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
    }
}
