package com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class MyTrainProgramsProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackTrainProgram(ArrayList<ItemGrouper> itemsProgram);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String idPerson;
    public MyTrainProgramsProvider(String idPerson) {
        this.idPerson = idPerson;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<ItemGrouper> items = new ArrayList<>();
        BaseTrainProgram trainProgram = new BaseTrainProgram(1);
        for (DataSnapshot postSnapshot : dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).getChildren()) {
            String idProgram = postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_ID).getValue().toString();
            if (dataSnapshot.child(idPerson).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(idProgram).exists()) {
                items.add(trainProgram.getTrainProg(postSnapshot));
            }
        }
        callback.callingBackTrainProgram(items);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
