package com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class OneTrainProgramProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackOneTrainProgram(TrainPrograms trainPrograms);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String programId;
    private String userId;

    public OneTrainProgramProvider(String programId, String userId) {
        this.programId = programId;
        this.userId = userId;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        BaseTrainProgram baseTrainProgram = new BaseTrainProgram(0);
        DataSnapshot postSnapshot = dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).child(programId);
        TrainPrograms trainPrograms = baseTrainProgram.getTrainProg(postSnapshot);
        if (dataSnapshot.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(programId).exists()) {
            trainPrograms.setMyLike(1);
        }else{
            trainPrograms.setMyLike(0);
        }
        callback.callingBackOneTrainProgram(trainPrograms);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
