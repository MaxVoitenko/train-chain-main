package com.mainvoitenko.trainchain.Providers;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import com.mainvoitenko.trainchain.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PhotoRedactor {

    Context context;

    public PhotoRedactor(Context context) {
        this.context = context;
    }


    public File createImageFile() throws IOException {
        String timeStamp = Long.toString(System.currentTimeMillis());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(context.getFilesDir(), "photos");
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(imageFileName,".jpg",storageDir);

//        File image = new File(storageDir,".jpg");

//  image.createNewFile()
        return image;
    }

    public Uri compressBitmap(Uri fileUri, String path) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.wait_fr));
        progressDialog.show();
        int MAX_IMAGE_SIZE = 200* 1024; // max final file size
        Bitmap bmpPic = BitmapFactory.decodeFile(fileUri.getPath());
        if ((bmpPic.getWidth() >= 600) && (bmpPic.getHeight() >= 600)) {
            BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
            bmpOptions.inSampleSize = 1;
            while ((bmpPic.getWidth() >= 600) && (bmpPic.getHeight() >= 600)) {
                bmpOptions.inSampleSize++;
                bmpPic = BitmapFactory.decodeFile(fileUri.getPath(), bmpOptions);
            }
        }
        int compressQuality = 40;
        int streamLength = MAX_IMAGE_SIZE;
        while (streamLength >= MAX_IMAGE_SIZE) {
            ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
            compressQuality -= 5;

            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
        }

        path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bmpPic, "Title", null);
        progressDialog.dismiss();
        return Uri.parse(path);
    }


    public void showFiles(File file){
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File item : files) {
                    showFiles(item);
                }
            }
        }
    }

    public boolean saveBitmapToFile(Bitmap source, File imageFile) {
        try {FileOutputStream fos = new FileOutputStream(imageFile);
            source.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();return false;
        } catch (IOException e) {
            e.printStackTrace();return false;
        }return true;
    }
}
