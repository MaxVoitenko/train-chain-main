package com.mainvoitenko.trainchain.Providers;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class GetPermissions {

    public static  final int REQUEST_CODE_PERMISSION_RECEIVE_CAMERA = 102;
//    private Context context;

//    public GetPermissions(Context context) {
//        this.context = context;
//    }

    public static boolean checkPermissions(Context context){
        boolean isCameraPermissionGranted = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean isWritePermissionGranted = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

        if(!isCameraPermissionGranted || !isWritePermissionGranted) {  //Если разрешения != true
            String[] permissions;//Разрешения которые хотим запросить у пользователя
            if (!isCameraPermissionGranted && !isWritePermissionGranted) {
                permissions = new String[] {android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            } else if (!isCameraPermissionGranted) {
                permissions = new String[] {android.Manifest.permission.CAMERA};
            } else {
                permissions = new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            }
            ActivityCompat.requestPermissions((Activity) context, permissions, REQUEST_CODE_PERMISSION_RECEIVE_CAMERA);
            return false;
        }else{
            return true;
        }
    }

}
