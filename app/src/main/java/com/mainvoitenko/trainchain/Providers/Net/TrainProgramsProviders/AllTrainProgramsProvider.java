package com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class AllTrainProgramsProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackAllTrainPrograms(ArrayList<TrainPrograms> allTrainPrograms);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String userId;

    public AllTrainProgramsProvider(String userId) {
        this.userId = userId;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<TrainPrograms> trainPrograms = new ArrayList<>();
        BaseTrainProgram trainProgram = new BaseTrainProgram(0);
        for (DataSnapshot postSnapshot : dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).getChildren()) {
            String id = postSnapshot.getKey();
            TrainPrograms tp = trainProgram.getTrainProg(postSnapshot);
            if (dataSnapshot.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(id).exists()) {
                tp.setMyLike(1);
            }else{
                tp.setMyLike(0);
            }
            trainPrograms.add(tp);
        }
//        int i=0;
//        for (DataSnapshot postSnapshot : dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).getChildren()) {
//            if (dataSnapshot.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(id).exists()) {
//                trainPrograms.get(i).setMyLike(1);
//            }else{
//                trainPrograms.get(i).setMyLike(0);
//            }
//            i++;
//        }
        callback.callingBackAllTrainPrograms(trainPrograms);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) { }
}
