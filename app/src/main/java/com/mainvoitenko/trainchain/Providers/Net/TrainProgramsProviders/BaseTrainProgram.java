package com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders;

import com.google.firebase.database.DataSnapshot;
import com.mainvoitenko.trainchain.Model.TrainPrograms;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class BaseTrainProgram {

    private int type;

    public BaseTrainProgram(int type) {
        this.type = type;
    }

    public TrainPrograms getTrainProg(DataSnapshot dataSnapshot){
        return new TrainPrograms(
                getTitle(dataSnapshot),
                getDuration(dataSnapshot),
                getGoal(dataSnapshot),
                getLevel(dataSnapshot),
                getAuthor(dataSnapshot),
                getId(dataSnapshot),
                getDayInWeek(dataSnapshot),
                getLike(dataSnapshot),
                type,
                getPersonId(dataSnapshot),
                getAutoAdder(dataSnapshot));
    }
    private String getTitle(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_TITLE).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_TITLE).getValue().toString();
        }else{ return "-";}
    };
    private String getDuration(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_DURATION).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_DURATION).getValue().toString();
        }else{ return "-";}
    };
    private String getGoal(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_GOAL).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_GOAL).getValue().toString();
        }else{ return "-";}
    };
    private String getLevel(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_LEVEL).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_LEVEL).getValue().toString();
        }else{ return "-";}
    };
    private String getAuthor(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_AUTHOR).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_AUTHOR).getValue().toString();
        }else{ return "-";}
    };
    //    private String getPrice(DataSnapshot postSnapshot){
//        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_PRICE).exists()){
//            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_PRICE).getValue().toString();
//        }else{ return "-";}
//    };
    private String getId(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_ID).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_ID).getValue().toString();
        }else{ return "-";}
    };
    private String getDayInWeek(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_DAY_IN_WEEK).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_DAY_IN_WEEK).getValue().toString();
        }else{ return "-";}
    };
    private String getLike(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_LIKE).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_LIKE).getValue().toString();
        }else{ return "-";}
    };
    private String getPersonId(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_PERSON_ID).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_PERSON_ID).getValue().toString();
        }else{ return "-";}
    };
    private String getAutoAdder(DataSnapshot postSnapshot){
        if(postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_AUTO_ADDER).exists()){
            return postSnapshot.child(SettingsConstants.TRAINING_PROGRAMS_AUTO_ADDER).getValue().toString();
        }else{ return "-";}
    };
}
