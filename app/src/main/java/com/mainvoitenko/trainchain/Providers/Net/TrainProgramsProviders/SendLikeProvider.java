package com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders;

import android.support.annotation.NonNull;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class SendLikeProvider implements ValueEventListener{

    private Callback callback;
    public interface Callback{ void callingBackTrainProgramsLike(int isLike);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private DatabaseReference myRef;
    private String userId;
    private String idProgram;

    public SendLikeProvider(DatabaseReference myRef, String userId, String idProgram) {
        this.myRef = myRef;
        this.userId = userId;
        this.idProgram = idProgram;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        Boolean result = loadLike(dataSnapshot);
        int isLike = SettingsConstants.LIKE_NOW_NO;
        if(result){
            isLike=SettingsConstants.LIKE_NOW_YES;
        }
        callback.callingBackTrainProgramsLike(isLike);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
    }

    public boolean loadLike(DataSnapshot dataSnapshot) {
        if (dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).child(idProgram).child(SettingsConstants.TRAINING_PROGRAMS_LIKE).exists()) {
            if (dataSnapshot.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(idProgram).exists()) {
                int like = Integer.parseInt(dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).child(idProgram).child(SettingsConstants.TRAINING_PROGRAMS_LIKE).getValue().toString());
                like--;
                if(like<0) {
                    myRef.child(SettingsConstants.TRAINING_PROGRAMS).child(idProgram).child(SettingsConstants.TRAINING_PROGRAMS_LIKE).setValue(0);
                    myRef.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(idProgram).removeValue();
                    return false;
                }else{
                    myRef.child(SettingsConstants.TRAINING_PROGRAMS).child(idProgram).child(SettingsConstants.TRAINING_PROGRAMS_LIKE).setValue(like);
                    myRef.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(idProgram).removeValue();
                    return false;
                }
            } else {
                int like = Integer.parseInt(dataSnapshot.child(SettingsConstants.TRAINING_PROGRAMS).child(idProgram).child(SettingsConstants.TRAINING_PROGRAMS_LIKE).getValue().toString());
                like++;
                myRef.child(SettingsConstants.TRAINING_PROGRAMS).child(idProgram).child(SettingsConstants.TRAINING_PROGRAMS_LIKE).setValue(like);
                myRef.child(userId).child(SettingsConstants.TRAINING_PROGRAMS_MY_LIKE).child(idProgram).setValue("1");
                return true;
            }
        } else {
            return false;
        }
    }
}
