package com.mainvoitenko.trainchain.Providers;

import com.mainvoitenko.trainchain.Training.Entity.HardEx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TrainingHard {

    private ArrayList<HardEx> training;
    private int fullKpw;
    private float fullTonnage;
    private List<Integer> countNull = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0));
    private List<Float> weightNull = new ArrayList<>(Arrays.asList(0F, 0F, 0F, 0F, 0F, 0F));

    public TrainingHard() {
        this.training = new ArrayList<>();
    }

    public void addNullExercise(){
        training.add(new HardEx("",weightNull, countNull, ""));
    }

    public void addExercise(String name, List<Integer> count){
        List<Integer> tempCount= new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0));
        if(count.size()<6){
            for(int i = 0; i < 6 ; i++){
                if(count.size()>i) {
//                    tempCount.get(i) = count.get(i);
                    tempCount.set(i,count.get(i));
                } }
        }
        training.add(new HardEx(name, weightNull, tempCount, ""));
    }

    public void  clearfullExercise(){
        training.clear();
    }
    public int getsize(){
             return training.size();
}

    public void setExercise(int index, String name, List<Float> weight, List<Integer> count, String timerEx){
       training.get(index).setCountExercise(count);
       training.get(index).setWeghtExercise(weight);
       training.get(index).setNameExercise(name);
       training.get(index).setTimerExercise(timerEx);
    }



    public  void setTime(int index, String timerEx){
        training.get(index).setTimerExercise(timerEx);
    }

    public void deleteExercise(int index){
        training.remove(index);
    }

    public List<Integer> getFullCountOneExercise(int index) {return training.get(index).getCountExercise();}

    public List<Float> getFullWeightOneExercise(int index) {
        return training.get(index).getWeghtExercise();
    }

    public String getNameOneExercise(int index){
        return training.get(index).getNameExercise();
    }

    public String getTimerOneExercise(int index){return  training.get(index).getTimerExercise();}

    public float getFullTonnage() {
        fullTonnage=0;
        for (int i = 0; i < training.size(); i++) {
            fullTonnage = fullTonnage + training.get(i).getTonnageExercise();
        }
        return fullTonnage;
    }
    public int getFullKpw() {
        fullKpw=0;
        for (int i = 0; i < training.size(); i++) {
            fullKpw = fullKpw + training.get(i).getKpwExercise();
        }
        return fullKpw;
    }
    public int size() {return training.size();}
    public ArrayList<HardEx> getTraining() {return training;}

}
