package com.mainvoitenko.trainchain.Settings;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class SingleTonUser {
    private static volatile SingleTonUser instance;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();

    private SingleTonUser(){

    }
    public static SingleTonUser getInstance(){
        if (instance == null)
            synchronized (SingleTonUser.class) {
                if (instance == null)
                    instance = new SingleTonUser();
            }
        return instance;
    }

    public FirebaseAuth getmAuth() {
        return mAuth;
    }

    public FirebaseUser getUser() {
        return user;
    }

    public DatabaseReference getMyRef() {
        return myRef;
    }

    public void setMyRef() {
        this.myRef = FirebaseDatabase.getInstance().getReference();;
    }

    public void setUser() {
        setmAuth();
        this.user = mAuth.getCurrentUser();
    }

    public void setmAuth() {
        this.mAuth = FirebaseAuth.getInstance();
    }
}
