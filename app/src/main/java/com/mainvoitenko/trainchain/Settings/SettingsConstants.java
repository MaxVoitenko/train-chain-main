package com.mainvoitenko.trainchain.Settings;


public class SettingsConstants {
    //Constants count
    public static final int COUNT_FOR_HARD_EXERCISE = 6;

    //Constants boolean
    public static final  int THIS_PERSON_ALREADY_ADD = 1;
    public static final  int THIS_PERSON_IS_NOT_ADD = 2;
    public static final  int POST_WITHOUT_BUTTON_MENU = 1;
    public static final  int POST_WITH_BUTTON_MENU = 0;
    public static final  int QUAL_TRAINER = 2;

    public static final  int LIKE_NOW_YES = 1;
    public static final  int LIKE_NOW_NO = 0;

//    TypeData
    public static final  int GROUPER_HARD_TRAIN_POST  = 1;
    public static final  int GROUPER_CARDIO_TRAIN_POST  = 2;
    public static final  int GROUPER_GROOP_TRAIN_POST  = 3;
    public static final  int GROUPER_FRIEND_ACCOUNT= 4;
    public static final  int GROUPER_MY_ACCOUNT= 5;
    public static final  int GROUPER_TRAINING= 6;
    public static final  int GROUPER_FRIEND_ACHIEVEMENT= 7;
    public static final  int GROUPER_STATISTIC_CIRCLE_CHART= 8;
    public static final  int GROUPER_STATISTIC_MAIN_DATA= 9;
    public static final  int GROUPER_STATISTIC_CHART= 10;
    public static final  int GROUPER_STATISTIC_ACHIEMENT = 11;
    public static final  int GROUPER_MOREE  = 1408;




    //SharedPreference HELPER
    public static final String SHOW_HELP_STATIC = "SHOW_HELP_STATIC";
    public static final String SHOW_HELP_ADD_HARD = "SHOW_HELP_ADD_HARD";
    public static final String SHOW_HELP_ADD_PHOTO= "SHOW_HELP_ADD_PHOTO";
    public static final String SHOW_HELP_ADD_TRAIN_TO_CACHE= "SHOW_HELP_ADD_TRAIN_TO_CACHE";


    //Constants intent
    public static final String END_ACTIVITY_EXIT_INTENT = "endAllActivity";
    public static final String PERSON_ID_INTENT = "PersonId";
    public static final String PROGRAM_ID_INTENT = "ProgramId";
    public static final String PROGRAM_TYPE_PIC_INTENT = "PROGRAM_TYPE_PIC_INTENT";
    public static final String PROGRAM_TITLE_INTENT = "PROGRAM_TITLE_INTENT";
    public static final String PROGRAM_POSITION = "PROGRAM_POSITION";
    public static final String ADD_LIKE = "ADD_LIKE";

    //Search INTENT
    public static final String SEARCH_DATA_INTENT = "searchDataFriend";
    public static final String SEARCH_DATA_INTENT_STRING = "SEARCH_DATA_INTENT_STRING";
    public static final String SEARCH_ID_INTENT_STRING = "SEARCH_ID_INTENT_STRING";
    public static final int MY_FRIEND = 86;
    public static final int MY_FOLLOWERS = 87;
    public static final int JUST_SEARCH= 88;


    //Train INTENT
    public static final String TIMETRAIN_INTENT = "Time_Train";
    public static final String WEIGHTTRAIN_INTENT = "Weight_Train";
    public static final String KPWTRAIN_INTENT = "kpw";
    public static final String TONNAGETRAIN_INTENT = "tonnage";
    public static final String INTENCITYTRAIN_INTENT = "intencity";
    public static final String COUNTTRAIN_INTENT = "count";
    public static final String END_THIS_TRAIN_INTENT = "endThisTraining";
    public static final String EMOTIONTRAIN_INTENT = "emotionTraining";
    public static final String DIRECTORY_PIC_INTENT = "pic_train_directory";
    public static final String THREE_INTENT = "THREE_INTENT";

    public static final String PHOTO_POST_INTENT = "PHOTO_POST_INTENT";
    public static final String PHOTO_PROFILE_INTENT = "PHOTO_PROFILE_INTENT ";

    public static final String TRAIN_PROGRAMS_INTENT_ID = "TRAIN_PROGRAMS_INTENT_ID";
    public static final String TRAIN_PROGRAMS_INTENT_NAME = "TRAIN_PROGRAMS_INTENT_NAME";

    //FIRE- BASE - LINKED
    public static final String ACCOUNT =  "Account";
    public static final String CITY = "City";
    public static final String QUALIFICATION = "QualificationAccount";
    public static final String GYM = "Gym";
    public static final String HEIGHT = "Height";
    public static final String WEIGHT = "Weight";
    public static final String ACHIEVEMENT = "Achievement";
    public static final String FRIENDS = "Friends";
    public static final String NAME = "Name";
    public static final String PERSONAL_ID = "PersonalId";
    public static final String TRAINING = "Training";
    public static final String COUNT_ALL_EXERCISE = "CountAllExercise";
    public static final String DATA = "Data";
    public static final String HARD_EX = "HardEx";
    public static final String COUNT = "Count";
    public static final String REST = "Rest";
    public static final String INTENCITY = "Intencity";
    public static final String KPW = "Kpw";
    public static final String TIME_TRAIN = "TimeTrain";
    public static final String EMOTION_TRAIN = "EmotionTrain";
    public static final String DIRECTORY_TRAIN = "DirectoryPictures";
    public static final String TONNAGE = "Tonnage";
    public static final String MY_WEIGHT = "myWeight";
    public static final String CARDIO_EX = "CardioEx";
    public static final String TIME = "Time";
    public static final String MY_URL_ADRESS= "UrlLinkAdress";
    public static final String FOLLOWERS= "Followers";

    public static final String TRAINING_PICTURES= "_Training Pictures";
    public static final String TRAINING_PROGRAMS = "_Training Programs";
    public static final String USER_PICTURES= "_User Pictures";

    public static final String TRAINING_PROGRAMS_TITLE = "Title";
    public static final String TRAINING_PROGRAMS_DURATION = "Duration";
    public static final String TRAINING_PROGRAMS_GOAL = "Goal";
    public static final String TRAINING_PROGRAMS_LEVEL = "Level";
    public static final String TRAINING_PROGRAMS_AUTHOR = "Author";
    public static final String TRAINING_PROGRAMS_PRICE = "Price";
    public static final String TRAINING_PROGRAMS_ID = "ProgramId";
    public static final String TRAINING_PROGRAMS_DAY_IN_WEEK = "DayInWeek";
    public static final String TRAINING_PROGRAMS_LIKE = "Like";
    public static final String TRAINING_PROGRAMS_PERSON_ID = "PersonIdProgram";
    public static final String TRAINING_PROGRAMS_MY_LIKE = "MyLike";
    public static final String TRAINING_PROGRAMS_AUTO_ADDER = "programAutoAdder";


    //SQLITE database
    public static final String DB_NAME = "SaveTrainingToCache";

    public static final String DB_TABLE_NAME_ONE = "CacheForTrainingOne";
    public static final String DB_HARD_PART = "CacheForTrainingOne";

    public static final String DB_TABLE_NAME_TWO = "CacheForTrainingTwo";
    public static final String DB_CARDIO_PART = "CacheForTrainingTwo";

    public static final String DB_TABLE_NAME_THREE = "CacheForTrainingThree";
    public static final String DB_OTHER_PART = "CacheForTrainingThree";
    public static final int DB_VERSION = 1;


    //ACHIEVEMENT

    public static final int ACHIEVEMENT_FIRST_TEN = 0;
    public static final int ACHIEVEMENT_FIRST_FIFTY = 1;
    public static final int ACHIEVEMENT_FIRST_ONE_HUNDRED = 2;

    public static final int ACHIEVEMENT_FIRST_FRIEND = 3;
    public static final int ACHIEVEMENT_FIVE_FRIENDS = 4;
    public static final int ACHIEVEMENT_TEN_FRIENDS = 5;

 public static final int ACHIEVEMENT_FIRST_FOLLOW= 6;
    public static final int ACHIEVEMENT_FIFTY_FOLLOW= 7;
    public static final int ACHIEVEMENT_ONEHUNG_FOLLOW= 8;

    public static final int ACHIEVEMENT_TEN_TRAIN = 9;
    public static final int ACHIEVEMENT_FIFTY_TRAIN = 10;
    public static final int ACHIEVEMENT_ONE_HUNDRED_TRAIN = 11;

    public static final int ACHIEVEMENT_MORNING_TRAIN = 12;
    public static final int ACHIEVEMENT_EVENING_TRAIN = 13;
    public static final int ACHIEVEMENT_GREAT_MONTH = 14;

    public static final int ACHIEVEMENT_FIRST_TONNAGE = 15;
    public static final int ACHIEVEMENT_TWO_TONNAGE = 16;
    public static final int ACHIEVEMENT_THREE_TONNAGE = 17;

    public static final int ACHIEVEMENT_FIRST_TONNAGE_FULL = 18;
    public static final int ACHIEVEMENT_TWO_TONNAGE_FULL = 19;
    public static final int ACHIEVEMENT_THREE_TONNAGE_FULL = 20;

    public static final int ACHIEVEMENT_FIRST_KPW = 21;
    public static final int ACHIEVEMENT_TWO_KPW = 22;
    public static final int ACHIEVEMENT_THREE_KPW = 23;

    public static final int ACHIEVEMENT_FIRST_KPW_FULL = 24;
    public static final int ACHIEVEMENT_TWO_KPW_FULL = 25;
    public static final int ACHIEVEMENT_THREE_KPW_FULL = 26;

    public static final int ACHIEVEMENT_TRAINER = 27;


    public static final  int ACHIEVEMENT_IS_DONE  = 1;
    public static final  int ACHIEVEMENT_IS_NOT_DONE  = 0;

    public static final  int FULL_COUNT_ACHIEWEMENT  = 28;



}
