package com.mainvoitenko.trainchain.Settings;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.FirebaseDatabase;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;

import io.fabric.sdk.android.Fabric;


public class App extends Application {

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        RemoveCacheData removeCacheData = new RemoveCacheData(getApplicationContext());
        removeCacheData.deleteAllPic(1000000);
    }

}
