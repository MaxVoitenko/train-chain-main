package com.mainvoitenko.trainchain.Model;

import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;


public class StatisticChart implements ItemGrouper {

    ArrayList<Float> data;
    String name;

    public StatisticChart(ArrayList<Float> data, String name) {
        this.data = data;
        this.name= name;
    }

    public ArrayList<Float> getData() {
        return data;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_STATISTIC_CHART;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }

    public String getName() {
        return name;
    }
}
