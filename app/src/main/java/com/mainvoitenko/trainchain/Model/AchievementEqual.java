package com.mainvoitenko.trainchain.Model;

import android.graphics.drawable.Drawable;

import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;


public class AchievementEqual extends Achievements implements ItemGrouper {

    private int typeFriend;
    private int typeForAchievement;

    public AchievementEqual(String name, String description, Drawable resources, int type) {
        super(name, description, resources, type);
    }

    public AchievementEqual(String name, String description, Drawable resources, int type, int typeFriend, int typeForAchievement) {
        super(name, description, resources, type);
        this.typeFriend = typeFriend;
        this.typeForAchievement = typeForAchievement;
    }

    public int getTypeFriend() {
        return typeFriend;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_FRIEND_ACHIEVEMENT;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }
}
