package com.mainvoitenko.trainchain.Model;

import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;


public class StatisticMainData implements ItemGrouper {
    private int fullKpw;
    private float fullTonnage;
    private String fullTime;
    private int countTraining;

    public StatisticMainData(int fullKpw, float fullTonnage, String fullTime, int countTraining) {
        this.fullKpw = fullKpw;
        this.fullTonnage = fullTonnage;
        this.fullTime = fullTime;
        this.countTraining = countTraining;
    }

    public float getFullTonnage() {
        return fullTonnage;
    }

    public int getCountTraining() {
        return countTraining;
    }

    public int getFullKpw() {
        return fullKpw;
    }

    public String getFullTime() {
        return fullTime;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_STATISTIC_MAIN_DATA;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }
}
