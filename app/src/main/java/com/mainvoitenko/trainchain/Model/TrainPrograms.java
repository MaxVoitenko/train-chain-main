package com.mainvoitenko.trainchain.Model;


import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class TrainPrograms implements ItemGrouper {
    private String title;
    private String Duration;
    private String Goal;
    private String Level;
    private String author;
    private String programId;
    private String like;
    private String dayInWeek;
    private int myLike;
    private String personId;
    private String autoAdder;

    private int type;
    private int resourceBackground;

    public TrainPrograms(String title, String duration, String goal, String level, String author
                   , String programId, String dayInWeek, String like, int myLike, String personId, String autoAdder) {
        this(title,duration,goal,level,author,programId,dayInWeek,like,myLike,-1,personId, autoAdder);
    }
    public TrainPrograms(String title, String duration, String goal, String level, String author
            , String programId, String dayInWeek, String like, int myLike, int type, String personId, String autoAdder) {
        this.title = title;
        this.Duration = duration;
        this.Goal = goal;
        this.Level = level;
        this.author = author;
        this.programId = programId;
        this.dayInWeek = dayInWeek;
        this.like = like;
        this.myLike = myLike;
        this.type = type;
        this.personId = personId;
        this.autoAdder = autoAdder;
    }

    public String getAutoAdder() {
        return autoAdder;
    }

    public void setAutoAdder(String autoAdder) {
        this.autoAdder = autoAdder;
    }

    public String getPersonId() {
        return personId;
    }

    public String getDayInWeek() {return dayInWeek;}

    public int getMyLike() {return myLike;}

    public void setMyLike(int myLike) {this.myLike = myLike;}

    public String getTitle() {
        return title;
    }

    public String getDuration() {
        return Duration;
    }

    public String getGoal() {
        return Goal;
    }

    public String getLevel() {
        return Level;
    }

    public String getAuthor() {
        return author;
    }


    public String getProgramId() {
        return programId;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_TRAINING;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }

    public int getResourceBackground() {
        this.resourceBackground = R.drawable.light_back;

        if (getGoal().contains("девуш")
                || getGoal().contains("Девуш")){
                if(getGoal().contains("Масс")
                    || getGoal().contains("масс")
                    || getGoal().contains("Объем")
                    || getGoal().contains("объем")){
                    resourceBackground = R.drawable.girl_run_back;
            }else if (getGoal().contains("Рельеф")
                    || getGoal().contains("Сушк")
                    || getGoal().contains("сушк")
                    || getGoal().contains("рельеф")){
                    resourceBackground = R.drawable.girl_cross_back;
            }else if (getGoal().contains("Сил")
                    || getGoal().contains("Жим")
                    || getGoal().contains("сил")
                    || getGoal().contains("жим")){
                    resourceBackground = R.drawable.girl_gym_back;
            }
        }else if(getGoal().contains("Парне")
                || getGoal().contains("парне")){
            if(getGoal().contains("Масс")
                    || getGoal().contains("масс")
                    || getGoal().contains("Объем")
                    || getGoal().contains("объем")){
                resourceBackground = R.drawable.boy_run_back;
            }else if (getGoal().contains("Рельеф")
                    || getGoal().contains("Сушк")
                    || getGoal().contains("сушк")
                    || getGoal().contains("рельеф")){
                resourceBackground = R.drawable.boy_cross_back;
            }else if (getGoal().contains("Сил")
                    || getGoal().contains("Жим")
                    || getGoal().contains("сил")
                    || getGoal().contains("жим")){
                resourceBackground = R.drawable.boy_gym_back;
            }
        }else{
            if(getGoal().contains("Масс")
                    || getGoal().contains("масс")
                    || getGoal().contains("Объем")
                    || getGoal().contains("объем")){
                resourceBackground = R.drawable.run_back;
            }else if (getGoal().contains("Рельеф")
                    || getGoal().contains("Сушк")
                    || getGoal().contains("сушк")
                    || getGoal().contains("рельеф")){
                resourceBackground = R.drawable.cross_back;
            }else if (getGoal().contains("Сил")
                    || getGoal().contains("Жим")
                    || getGoal().contains("сил")
                    || getGoal().contains("жим")){
                resourceBackground = R.drawable.gym_back;
            }
        }
        return resourceBackground;
    }
}
