package com.mainvoitenko.trainchain.Model;

import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;


public class StatisticCircleChart implements ItemGrouper {

    private int hardTrain=0;
    private int cardioTrain =0;
    private int groupTrain=0;

    public StatisticCircleChart(int hardTrain, int cardioTrain, int groupTrain) {
        this.hardTrain = hardTrain;
        this.cardioTrain = cardioTrain;
        this.groupTrain = groupTrain;
    }

    public int getHardTrain() {
        return hardTrain;
    }

    public int getCardioTrain() {
        return cardioTrain;
    }

    public int getGroupTrain() {
        return groupTrain;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_STATISTIC_CIRCLE_CHART;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }
}
