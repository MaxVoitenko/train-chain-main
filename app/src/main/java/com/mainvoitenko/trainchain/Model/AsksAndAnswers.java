package com.mainvoitenko.trainchain.Model;


public class AsksAndAnswers {
private String ask;
private String answer;
private boolean isOpen;

    public AsksAndAnswers(String ask, String answer, boolean isOpen) {
        this.ask = ask;
        this.answer = answer;
        this.isOpen = isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public String getAnswer() {
        return answer;
    }

    public String getAsk() {
        return ask;
    }
}
