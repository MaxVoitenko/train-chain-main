package com.mainvoitenko.trainchain.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class DbCachTraining extends SQLiteOpenHelper {


    public DbCachTraining(Context context) {
        super(context, SettingsConstants.DB_NAME,
                null, SettingsConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + SettingsConstants.DB_TABLE_NAME_ONE + " (" +
                SettingsConstants.DB_HARD_PART + " TEXT);");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + SettingsConstants.DB_TABLE_NAME_TWO+ " (" +
                SettingsConstants.DB_CARDIO_PART + " TEXT);");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + SettingsConstants.DB_TABLE_NAME_THREE + " (" +
                SettingsConstants.DB_OTHER_PART + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
