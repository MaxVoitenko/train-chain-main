package com.mainvoitenko.trainchain.Account.Entity;

import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class AccountFriend extends AccountMy implements ItemGrouper {
    private  String personId;
    private int friendOrNot;
    private String countTrain;
    private String qualAccount;

    public String getCountTrain() {
        return countTrain;
    }

    public void setCountTrain(String countTrain) {
        this.countTrain = countTrain;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public int getFriendOrNot() {
        return friendOrNot;
    }

    public void setFriendOrNot(int friendOrNot) {
        this.friendOrNot = friendOrNot;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }

    @Override
    public int getItemType() {
        return SettingsConstants.GROUPER_FRIEND_ACCOUNT;
    }

    public String getQualificationAccount() {
        return qualAccount;
    }

    public void setQualificationAccount(String qualificationAccount) {
        this.qualAccount = qualificationAccount;
    }

}
