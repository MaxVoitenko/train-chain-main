package com.mainvoitenko.trainchain.Account.ApiNet.SearchAccountsProviders;

import com.google.firebase.database.DataSnapshot;
import com.mainvoitenko.trainchain.Account.Entity.AccountFriend;
import com.mainvoitenko.trainchain.Account.ApiNet.BaseAccountDataProvider;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class BaseAccountSearchProvider {

    private String personId;

    public BaseAccountSearchProvider(String personId) {
        this.personId = personId;
    }

    public AccountFriend getDataForSearch(DataSnapshot dataSnapshot) {
        BaseAccountDataProvider baseAccountDataProvider = new BaseAccountDataProvider(personId, dataSnapshot);
        AccountFriend account = new AccountFriend();
        account.setName(baseAccountDataProvider.loadName());
        ArrayList temp = baseAccountDataProvider.loadCityAndGym();
        account.setCity(temp.get(0).toString());
        account.setGym(temp.get(1).toString());
        account.setQualificationAccount(baseAccountDataProvider.loadQualAccount());
        account.setPersonId(personId);
        account.setQualificationAccount(baseAccountDataProvider.loadQualAccount());
        if (account.getPersonId().equals(SettingsConstants.TRAINING_PROGRAMS)) {
            return null;
        } else {
            return account;
        }
    }
}
