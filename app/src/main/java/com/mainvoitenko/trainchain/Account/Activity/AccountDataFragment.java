package com.mainvoitenko.trainchain.Account.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Interface.Clickers.OnLikeProgramClick;
import com.mainvoitenko.trainchain.Account.ApiNet.MyAccountProvider;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.MyTrainProgramsProvider;
import com.mainvoitenko.trainchain.Providers.Net.TrainProgramsProviders.SendLikeProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterAccount;
import com.mainvoitenko.trainchain.Account.Entity.AccountMy;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnTrainingProgramClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.UI.Activity.MainWorkSpace;
import com.mainvoitenko.trainchain.UI.Activity.MyAccount.AchievementActivity;
import com.mainvoitenko.trainchain.UI.Activity.MyAccount.ChangePictureActivity;
import com.mainvoitenko.trainchain.UI.Activity.PhotoPostActivity;
import com.mainvoitenko.trainchain.UI.Activity.LookingTrainProgramActivity;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class AccountDataFragment extends Fragment implements RvAdapterAccount.OnIntentToPrograms
                                                , OnTrainingProgramClickListener
                                                , View.OnClickListener
                                                , SwipeRefreshLayout.OnRefreshListener
                                                , MyAccountProvider.Callback
                                                , MyTrainProgramsProvider.Callback
                                                , OnLikeProgramClick
                                                , SendLikeProvider.Callback {
    public static  final int REQUEST_NEW_DATA = 1551;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();
    private StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();

    private RvAdapterAccount rvAdapterAccount;
    private ArrayList<ItemGrouper> items = new ArrayList<>();

    private  View mView;

    private RecyclerView rvMyAcc;
    private ProgressBar progress_pic;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageButton getMyPic;
    private ImageButton getMyData;
    private ImageButton getMyAchivement;
    private ImageView imageProfile;
    private TextView nameTextView;

//    @Nullable @BindView(R.id.rvMyAcc)  RecyclerView rvMyAcc;
//    @Nullable @BindView(R.id.progress_pic)  ProgressBar progress_pic;
//    @Nullable @BindView(R.id.swipeRefreshLayout)  SwipeRefreshLayout swipeRefreshLayout;
//    @Nullable @BindView(R.id.getMyPic)  ImageButton getMyPic;
//    @Nullable @BindView(R.id.getMyData)  ImageButton getMyData;
//    @Nullable @BindView(R.id.getMyAchivement)  ImageButton getMyAchivement;
//    @Nullable @BindView(R.id.imageProfile)  ImageView imageProfile;
//    @Nullable @BindView(R.id.nameTextView)  TextView nameTextView;

    private int position = -1;
    private MyTrainProgramsProvider myTrainProgramsProvider;
    private MyAccountProvider myAccountProvider;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_account_data, container,false);
//        ButterKnife.bind(getActivity(), mView);
        rvMyAcc = mView.findViewById(R.id.rvMyAcc);
        progress_pic = mView.findViewById(R.id.progress_pic);
        swipeRefreshLayout = mView.findViewById(R.id.swipeRefreshLayout);
        getMyPic = mView.findViewById(R.id.getMyPic);
        getMyData = mView.findViewById(R.id.getMyData);
        getMyAchivement = mView.findViewById(R.id.getMyAchivement);
        imageProfile = mView.findViewById(R.id.imageProfile);
        nameTextView = mView.findViewById(R.id.nameTextView);

        swipeRefreshLayout.setOnRefreshListener(this);
        getMyPic.setOnClickListener(this);
        getMyData.setOnClickListener(this);
        getMyAchivement.setOnClickListener(this);
        imageProfile.setOnClickListener(this);

        rvAdapterAccount = new RvAdapterAccount(items);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rvMyAcc.setLayoutManager(mLayoutManager);
        rvMyAcc.setNestedScrollingEnabled(false);
        rvMyAcc.setAdapter(rvAdapterAccount);
        rvAdapterAccount.setOnZeroProgramClickListener(this);
        rvAdapterAccount.setOnTrainingProgramClickListener(this);
        rvAdapterAccount.setOnLikeProgramClick(this);
        update();

        return mView;
    }

    private void update() {
        progress_pic.setVisibility(View.VISIBLE);

        myTrainProgramsProvider = new MyTrainProgramsProvider(user.getUid());
        myTrainProgramsProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(myTrainProgramsProvider);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_NEW_DATA){
            update();
        }
        if(requestCode == LookingTrainProgramActivity.REQUEST_UPDATE_TRAININGPROGRAM){
            if(resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    int like = bundle.getInt(SettingsConstants.ADD_LIKE, 1);
                    if (like == 0) {
                        int position = bundle.getInt(SettingsConstants.PROGRAM_POSITION,-1);
                        if(position != -1){
                            items.remove(position);
                            rvAdapterAccount.notifyItemRemoved(position);
                        }
                    }
                }
            }
        }
    }
    @Override
    public void onIntentToPrograms() {
        ((MainWorkSpace)getActivity()).moveToTrainProgram();
    }
    @Override
    public void onResume() {
        super.onResume();
//        update();
    }

    @Override
    public void setOnTrainingProgramClickListener(String idTrain, View v1, int type, String title, int position) {
        if(idTrain!= null && !idTrain.isEmpty()) {
            Intent intent = new Intent(getContext(), LookingTrainProgramActivity.class);
            intent.putExtra(SettingsConstants.PROGRAM_ID_INTENT, idTrain);
            intent.putExtra(SettingsConstants.PROGRAM_TYPE_PIC_INTENT, type);
            intent.putExtra(SettingsConstants.PROGRAM_TITLE_INTENT, title);
            intent.putExtra(SettingsConstants.PROGRAM_POSITION, position);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(), new Pair<View, String>(v1.findViewById(R.id.imageViewBack),getString(R.string.transition_name_circle)));
            ActivityCompat.startActivityForResult(getActivity(), intent
                    , LookingTrainProgramActivity.REQUEST_UPDATE_TRAININGPROGRAM ,options.toBundle());
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        ActivityOptionsCompat options;
        switch (v.getId()){
            case R.id.getMyPic:
                intent = new Intent(getContext(), ChangePictureActivity.class);
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(), new Pair<View, String>(mView.findViewById(R.id.imageProfile),getString(R.string.transition_name_circle)));
                ActivityCompat.startActivityForResult(getActivity(), intent,REQUEST_NEW_DATA ,options.toBundle());
                break;
            case R.id.getMyData:
                intent= new Intent(getContext(), ChangeDataActivity.class);
                startActivityForResult(intent, REQUEST_NEW_DATA);
                break;
            case R.id.getMyAchivement:
                intent= new Intent(getContext(), AchievementActivity.class);
                startActivity(intent);
                break;
            case R.id.imageProfile:
                if(imageProfile.getDrawable()!=null) {
                    intent = new Intent(getContext(), PhotoPostActivity.class);
                    intent.putExtra(SettingsConstants.PHOTO_PROFILE_INTENT, user.getUid());
                    options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            getActivity(), new Pair<View, String>(v.findViewById(R.id.imageProfile),
                                    getString(R.string.transition_name_circle)));
                    ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
                }break;
        }
    }
    @Override
    public void onRefresh() {
        RemoveCacheData removeCacheData = new RemoveCacheData(getContext());
        removeCacheData.deleteOnePic(user.getUid());
        update();
    }

    @Override
    public void callingBack(AccountMy accountMy, int qualification) {
//        items.clear();
        items.add(accountMy);
        nameTextView.setText(accountMy.getName());
        imageProfile.setImageDrawable(null);
        if (imageProfile.getDrawable() == null) {
            LoadPictures loadPictures = new LoadPictures(mStorageRef, getContext(), imageProfile, progress_pic);
            loadPictures.addFullPicToPrifile(SettingsConstants.USER_PICTURES, user.getUid());
        }
        myRef.keepSynced(true);
        progress_pic.setVisibility(View.GONE);
        if (items.size() == 1) {
            items.add(0, null);
        }
        rvAdapterAccount.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void callingBackTrainProgram(ArrayList<ItemGrouper> itemsProgram) {
        items.clear();
        if(itemsProgram!=null){
        items.addAll(itemsProgram);}
        myAccountProvider = new MyAccountProvider(user.getUid(), getContext());
        myAccountProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(myAccountProvider);
    }

    @Override
    public void callingBackTrainProgramsLike(int isLike) {
        if (position != -1) {
            items.remove(position);
            rvAdapterAccount.notifyItemRemoved(position);
        }
    }

    @Override
    public void setOnLikeProgramClick(String idProgram, int position) {
        this.position=position;
        SendLikeProvider sendLikeProvider = new SendLikeProvider(myRef,user.getUid(),idProgram);
        sendLikeProvider.registerCallBack(this);
        myRef.addListenerForSingleValueEvent(sendLikeProvider);
    }

    @Override
    public void onDestroy() {
        myRef.removeEventListener(myTrainProgramsProvider);
        myRef.removeEventListener(myAccountProvider);
        super.onDestroy();
    }
}
