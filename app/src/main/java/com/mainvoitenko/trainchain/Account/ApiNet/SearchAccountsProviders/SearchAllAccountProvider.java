package com.mainvoitenko.trainchain.Account.ApiNet.SearchAccountsProviders;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class SearchAllAccountProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackAll(ArrayList<ItemGrouper> accountFriends);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<ItemGrouper> accountFriends = new ArrayList<>();
        BaseAccountSearchProvider bs;
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            if(!postSnapshot.getKey().equals(SettingsConstants.TRAINING_PROGRAMS)){
            bs = new BaseAccountSearchProvider(postSnapshot.getKey());
            accountFriends.add(bs.getDataForSearch(dataSnapshot));
            }
        }
        callback.callingBackAll(accountFriends);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) { }


}
