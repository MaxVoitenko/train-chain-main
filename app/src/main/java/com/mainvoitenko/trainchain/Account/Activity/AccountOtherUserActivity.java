package com.mainvoitenko.trainchain.Account.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mainvoitenko.trainchain.Account.ApiNet.FriendAccountProvider;
import com.mainvoitenko.trainchain.Training.ApiNet.PostsProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Activity.Friends.AchievementOtherUserActivity;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterPost;
import com.mainvoitenko.trainchain.Account.Entity.AccountFriend;
import com.mainvoitenko.trainchain.Providers.RemoveCacheData;
import com.mainvoitenko.trainchain.Providers.Net.CheckAchievement;
import com.mainvoitenko.trainchain.Providers.Net.LoadPictures;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.Interface.Clickers.OnPictureClickListener;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.FixAppBarLayoutBehavior;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.UI.Activity.PhotoPostActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class AccountOtherUserActivity extends AppCompatActivity implements OnUniversalClick
                                            ,OnPictureClickListener
                                            ,SwipeRefreshLayout.OnRefreshListener
                                            ,FriendAccountProvider.Callback
                                            ,PostsProvider.Callback {

     @BindView(R.id.recyclerView)  RecyclerView rvFriend;
     @BindView(R.id.progress_pic)  ProgressBar progress_pic;
     @BindView(R.id.imageProfile)  ImageView imageProfile;
     @BindView(R.id.nameTextView)  TextView nameTextView;
     @BindView(R.id.swipeRefreshLayout)  SwipeRefreshLayout swipeRefreshLayout;
     @BindView(R.id.addToFriends) ImageButton addToFriends;
     @BindView(R.id.getAchivementEqual) ImageButton getAchivementEqual;
     @BindView(R.id.clickBack) AppBarLayout clickBack;
     @BindView(R.id.appbarfix) AppBarLayout abl;

    private ArrayList<ItemGrouper> items = new ArrayList<>();
    private RvAdapterPost recyclerAdapterForPost;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private StorageReference mStorageRef;

    private String personId;
    private int countItems = 0;
    private AccountFriend friend;

    private PostsProvider postsProvider;
    private FriendAccountProvider friendAccountProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_friend);
        ButterKnife.bind(this);
        ((CoordinatorLayout.LayoutParams) abl.getLayoutParams()).setBehavior(new FixAppBarLayoutBehavior());
        clickBack.setVisibility(View.VISIBLE);
        myRef.keepSynced(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        swipeRefreshLayout.setOnRefreshListener(this);

        Intent intent = getIntent();
        personId = intent.getStringExtra(SettingsConstants.PERSON_ID_INTENT);

        recyclerAdapterForPost = new RvAdapterPost(items, this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rvFriend.setHasFixedSize(true);
        recyclerAdapterForPost.setOnUniversalClick(this);
        recyclerAdapterForPost.setOnPictureClickListener(this);
        rvFriend.setLayoutManager(mLayoutManager);
        rvFriend.setNestedScrollingEnabled(false);
        rvFriend.setAdapter(recyclerAdapterForPost);

        updateData();
    }

    @Optional @OnClick(R.id.imageProfile)
    public void imageProfileClick(View v) {
        if (imageProfile.getDrawable() != null) {
            Intent intent = new Intent(this, PhotoPostActivity.class);
            intent.putExtra(SettingsConstants.PHOTO_PROFILE_INTENT, personId);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this, new Pair<View, String>(v.findViewById(R.id.imageProfile),
                            getString(R.string.transition_name_circle)));
            ActivityCompat.startActivity(this, intent, options.toBundle());
        }
    }

    @Optional  @OnClick(R.id.addToFriends)
    public void addToFriendsClick(View v) {
        addUserToFriend();
    }

    @Optional  @OnClick(R.id.getAchivementEqual)
    public void getAchivementEqualClick(View v) {
        Intent intentAchivement = new Intent(AccountOtherUserActivity.this, AchievementOtherUserActivity.class );
        intentAchivement.putExtra(SettingsConstants.PERSON_ID_INTENT, friend.getPersonId());
        startActivity(intentAchivement);
    }

    @Optional  @OnClick(R.id.clickBack)
    public void clickBackClick(View v) {
        finish();
    }

    private void updateData() {
        if (!personId.equals("0")) {
            countItems = countItems + 10;
            progress_pic.setVisibility(View.VISIBLE);

            postsProvider = new PostsProvider(mStorageRef,SettingsConstants.POST_WITHOUT_BUTTON_MENU, personId);
            postsProvider.registerCallBack(this);
            myRef.child(personId).child(SettingsConstants.TRAINING).limitToLast(countItems).addListenerForSingleValueEvent(postsProvider);

        }
    }

    @Override
    public void onUniversalClickListener() {
        updateData();
    }

    @Override
    public void setOnPictureClickListener(String id, View v, int position) {
        if(id != null && !id.isEmpty()) {
            Intent intent = new Intent(this, PhotoPostActivity.class);
            intent.putExtra(SettingsConstants.PHOTO_POST_INTENT, id);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this,
                    new Pair<View, String>(v.findViewById(R.id.picture_training),
                            getString(R.string.transition_name_circle)));
            ActivityCompat.startActivity(this, intent, options.toBundle());
        }
    }

    private void addUserToFriend() {

        if (friend != null) {
            if (addToFriends.getTag().equals(SettingsConstants.THIS_PERSON_ALREADY_ADD)) {//todo here
                if (!friend.getPersonId().equals("")) {
                    myRef.child(user.getUid()).child(SettingsConstants.FRIENDS).child(friend.getPersonId()).setValue(friend.getPersonId(),
                            new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    myRef.child(friend.getPersonId()).child(SettingsConstants.FOLLOWERS).child(user.getUid()).setValue(user.getUid(),
                                            new DatabaseReference.CompletionListener() {
                                                @Override
                                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                    Toast.makeText(AccountOtherUserActivity.this, R.string.add_to_friend_tost, Toast.LENGTH_SHORT).show();
                                                    addToFriends.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_friend));
                                                    addToFriends.setTag(SettingsConstants.THIS_PERSON_IS_NOT_ADD);
                                                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            CheckAchievement checkAchievement = new CheckAchievement(dataSnapshot,user.getUid(), AccountOtherUserActivity.this);
                                                            checkAchievement.firstFriend();
                                                            checkAchievement.firstFollow();
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {
                                                        }
                                                    });
                                                }
                                            });
                                }
                            });
                }
            } else {
                if (!friend.getPersonId().equals("")) {//todo here
                    myRef.child(user.getUid()).child(SettingsConstants.FRIENDS).child(friend.getPersonId()).removeValue();
                    myRef.child(friend.getPersonId()).child(SettingsConstants.FOLLOWERS).child(user.getUid()).removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            Toast.makeText(AccountOtherUserActivity.this, R.string.delete_to_friend_tost, Toast.LENGTH_SHORT).show();
                            addToFriends.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_add));
                            addToFriends.setTag(SettingsConstants.THIS_PERSON_ALREADY_ADD);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        imageProfile.setImageDrawable(null);
        RemoveCacheData removeCacheData = new RemoveCacheData(this);
        removeCacheData.deleteOnePic(personId);
        countItems=0;
        updateData();
    }

    @Override
    public void callingBack(AccountFriend accountFriend, int friendOrNot, String myQual) {
        if (accountFriend != null) {
            friend = accountFriend;
            items.add(accountFriend);
        imageProfile.setImageDrawable(null);
        if(imageProfile.getDrawable()== null) {
            LoadPictures loadPictures = new LoadPictures(mStorageRef, AccountOtherUserActivity.this, imageProfile);
            loadPictures.addFullPicToPrifile(SettingsConstants.USER_PICTURES, personId);
        }
        nameTextView.setText(accountFriend.getName());
            if (accountFriend.getFriendOrNot() == 1) {
                addToFriends.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_friend));
                addToFriends.setTag(SettingsConstants.THIS_PERSON_IS_NOT_ADD);
            } else {
                addToFriends.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_add));
                addToFriends.setTag(SettingsConstants.THIS_PERSON_ALREADY_ADD);
            }}

        progress_pic.setVisibility(View.GONE);
        if (countItems == 11) {
            rvFriend.smoothScrollToPosition(items.size());
        }
        if (items.size() == countItems + 1) {
            items.add(0, null);
        }
        swipeRefreshLayout.setRefreshing(false);
        recyclerAdapterForPost.getMyEqual(Integer.parseInt(myQual));
        recyclerAdapterForPost.notifyDataSetChanged();
    }

    @Override
    public void callingBackPosts(ArrayList<ItemGrouper> posts) {
        if(posts!=null){
            items.clear();
            items.addAll(posts);
            friendAccountProvider = new FriendAccountProvider(personId, user.getUid());
            friendAccountProvider.registerCallBack(this);
            myRef.addListenerForSingleValueEvent(friendAccountProvider);
        }
    }

    @Override
    protected void onDestroy() {
        myRef.removeEventListener(postsProvider);
        myRef.removeEventListener(friendAccountProvider);
        super.onDestroy();
    }
}
