package com.mainvoitenko.trainchain.Account.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.mainvoitenko.trainchain.Account.ApiNet.SearchAccountsProviders.SearchAllAccountProvider;
import com.mainvoitenko.trainchain.Account.ApiNet.SearchAccountsProviders.SearchFollowerAccountProvider;
import com.mainvoitenko.trainchain.Account.ApiNet.SearchAccountsProviders.SearchFriendAccountProvider;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterSearch;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Interface.Clickers.OnSearchClickListener;
import com.mainvoitenko.trainchain.Interface.Clickers.OnUniversalClick;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


public class SearchAccountActivity extends AppCompatActivity implements OnUniversalClick
                                                                , SwipeRefreshLayout.OnRefreshListener
                                                                , OnSearchClickListener
                                                                , SearchAllAccountProvider.Callback
                                                                , SearchFriendAccountProvider.Callback
                                                                , SearchFollowerAccountProvider.Callback {
    @BindView(R.id.splash_no_people) ImageView splash_no_data;
    @BindView(R.id.clickBack) ImageView clickBack;
    @BindView(R.id.searchFriendText) EditText search_edit_text;
    @BindView(R.id.search_button) ImageButton search_button;
    @BindView(R.id.titleSearch) TextView titleSearch;
    @BindView(R.id.swipe_container) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView rvMyFriends;
    @BindView(R.id.progress_pic) ProgressBar progress_pic;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private ArrayList<ItemGrouper> people = new ArrayList<>();
    private RvAdapterSearch recyclerAdapterForSearchFriends;

    private int countItems =  0;
    private String searchData= "";
    private Bundle bundle;
    private String idPers="-1";

    private SearchFriendAccountProvider searchFriendAccountProvider;
    private SearchFollowerAccountProvider searchFollowerAccountProvider;
    private SearchAllAccountProvider searchAllAccountProvider;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        clickBack.setVisibility(View.VISIBLE);

        myRef.keepSynced(true);
        refreshLayout.setOnRefreshListener(this);
        recyclerAdapterForSearchFriends = new RvAdapterSearch(people);
        rvMyFriends.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvMyFriends.setHasFixedSize(true);
        rvMyFriends.setAdapter(recyclerAdapterForSearchFriends);
        recyclerAdapterForSearchFriends.setOnUniversalClick(this);
        recyclerAdapterForSearchFriends.setOnSearchClickListener(this);
        rvMyFriends.setItemViewCacheSize(10);
        Intent intent = getIntent();
        bundle = intent.getExtras();

        if(bundle!= null) {
            int whatShow = bundle.getInt(SettingsConstants.SEARCH_DATA_INTENT);
            if (whatShow == SettingsConstants.JUST_SEARCH) {
                searchData = bundle.getString(SettingsConstants.SEARCH_DATA_INTENT_STRING);
                if (searchData != null) {
                    search_edit_text.setText(searchData);
                }
            }
            onRefresh();
        }else{
            progress_pic.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        countItems=0;
        int whatShow = bundle.getInt(SettingsConstants.SEARCH_DATA_INTENT);
        if (whatShow == SettingsConstants.MY_FOLLOWERS) {
            idPers = bundle.getString(SettingsConstants.SEARCH_ID_INTENT_STRING);
            titleSearch.setVisibility(View.VISIBLE);
            search_edit_text.setVisibility(View.GONE);
            search_button.setVisibility(View.GONE);
            titleSearch.setText(getString(R.string.followers_label));
            addPeopleFollows();
        } else if (whatShow== SettingsConstants.MY_FRIEND) {
            idPers = bundle.getString(SettingsConstants.SEARCH_ID_INTENT_STRING);
            titleSearch.setVisibility(View.VISIBLE);
            search_edit_text.setVisibility(View.GONE);
            search_button.setVisibility(View.GONE);
            titleSearch.setText(getString(R.string.friends_label));
            addPeopleFriends();
        }else if(whatShow== SettingsConstants.JUST_SEARCH) {
            titleSearch.setVisibility(View.GONE);
            search_edit_text.setVisibility(View.VISIBLE);
            search_button.setVisibility(View.VISIBLE);
                addPeopleSearch();
        }
    }

  private void addPeopleFriends(){
      progress_pic.setVisibility(View.VISIBLE);
      searchFriendAccountProvider= new SearchFriendAccountProvider(idPers);
      searchFriendAccountProvider.registerCallBack(this);
      myRef.addListenerForSingleValueEvent(searchFriendAccountProvider);
}
  private void addPeopleFollows() {
      progress_pic.setVisibility(View.VISIBLE);
      searchFollowerAccountProvider= new SearchFollowerAccountProvider(idPers);
      searchFollowerAccountProvider.registerCallBack(this);
      myRef.addListenerForSingleValueEvent(searchFollowerAccountProvider);
  }


    public  void addPeopleSearch(){
        progress_pic.setVisibility(View.VISIBLE);
        countItems = countItems+10;
        searchAllAccountProvider = new SearchAllAccountProvider();
        searchAllAccountProvider.registerCallBack(this);
        if(search_edit_text.getText().toString().equals("")) {
            myRef.limitToFirst(countItems).orderByChild(SettingsConstants.NAME).addListenerForSingleValueEvent(searchAllAccountProvider);
        }else{
            Query query = myRef.orderByChild(SettingsConstants.NAME).equalTo(search_edit_text.getText().toString()).limitToFirst(countItems);
            query.addListenerForSingleValueEvent(searchAllAccountProvider);
        }
    }

    public void onUniversalClickListener() {
        addPeopleSearch();
    }

    @Optional  @OnClick(R.id.search_button)
    public void search_buttonClick(View v) {
        onRefresh();
    }

    @Optional @OnClick(R.id.clickBack)
    public void clickBackClick(View v) {
        onBackPressed();
    }

    @Override
    public void setOnSearchClickListener(String id, View v, int position) {
        if(id != null && !id.isEmpty()) {
            Intent intent = new Intent(this, AccountOtherUserActivity.class);
            intent.putExtra(SettingsConstants.PERSON_ID_INTENT, id);
            startActivity(intent);
        }
    }

    @Override
    public void callingBackAll(ArrayList<ItemGrouper> accountFriends) {
        if (accountFriends != null) {
                people.clear();
                people.addAll(accountFriends);
            if (people.size() == countItems - 1) {
                people.add(null);
            }
            if (people.size() == 0) {
                Toast.makeText(SearchAccountActivity.this, R.string.no_data_toast_recycler, Toast.LENGTH_SHORT).show();
            }
            recyclerAdapterForSearchFriends.notifyDataSetChanged();
            progress_pic.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void callingBackFriends(ArrayList<ItemGrouper> accountFriends) {
        if (accountFriends != null) {
            if (accountFriends.size() == 0) {
                splash_no_data.setVisibility(View.VISIBLE);
            } else {
                splash_no_data.setVisibility(View.GONE);
                people.clear();
                people.addAll(accountFriends);
                recyclerAdapterForSearchFriends.notifyDataSetChanged();
                progress_pic.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void callingBackFollowers(ArrayList<ItemGrouper> accountFriends) {
        if (accountFriends != null) {
            if (accountFriends.size() == 0) {
                splash_no_data.setVisibility(View.VISIBLE);
            } else {
                splash_no_data.setVisibility(View.GONE);
                people.clear();
                people.addAll(accountFriends);
            }
        }
        recyclerAdapterForSearchFriends.notifyDataSetChanged();
        progress_pic.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        if(searchFriendAccountProvider!=null)
        myRef.removeEventListener(searchFriendAccountProvider);
        if(searchFollowerAccountProvider!=null)
        myRef.removeEventListener(searchFollowerAccountProvider);
        if(searchAllAccountProvider!=null)
        myRef.removeEventListener(searchAllAccountProvider);
        super.onDestroy();
    }
}
