package com.mainvoitenko.trainchain.Account.ApiNet.SearchAccountsProviders;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class SearchFriendAccountProvider implements ValueEventListener {

    private Callback callback;
    public interface Callback{ void callingBackFriends(ArrayList<ItemGrouper> accountFriends);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    private String myId;

    public SearchFriendAccountProvider(String myId) {
        this.myId = myId;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<String> idList = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.child(myId).child(SettingsConstants.FRIENDS).getChildren()){
            idList.add(postSnapshot.getValue().toString());
        }
        ArrayList<ItemGrouper> accountFriends = new ArrayList<>();
        BaseAccountSearchProvider bs;
        for(int i=0; i<idList.size(); i++) {
            if (dataSnapshot.child(idList.get(i)).exists()) {
                bs = new BaseAccountSearchProvider(idList.get(i));
                accountFriends.add(bs.getDataForSearch(dataSnapshot));
            }
        }
        callback.callingBackFriends(accountFriends);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
    }
}
