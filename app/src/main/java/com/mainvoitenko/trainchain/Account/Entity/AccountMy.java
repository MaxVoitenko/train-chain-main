package com.mainvoitenko.trainchain.Account.Entity;


import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

public class AccountMy implements ItemGrouper {
    private String name;
    private String city;
    private String weight;
    private String height;
    private String gym;
    private String countMyFriends;
    private String countMyFollowers;


    private String qualificationAccount;
    private String myUrlAdress;

    private String qualificationAccountMY;


    public String getCountMyFriends() {
        return countMyFriends;
    }

    public void setCountMyFriends(String countMyFriends) {
        this.countMyFriends = countMyFriends;
    }

    public String getCountMyFollowers() {
        return countMyFollowers;
    }

    public void setCountMyFollowers(String countMyFollowers) {
        this.countMyFollowers = countMyFollowers;
    }

    public String getMyUrlAdress() {
        return myUrlAdress;
    }

    public void setMyUrlAdress(String myUrlAdress) {
        this.myUrlAdress = myUrlAdress;
    }

    public AccountMy(){
    }

    @Override
    public int getItemType() {return SettingsConstants.GROUPER_MY_ACCOUNT;}

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int ShowButtonMenu() {
        return 0;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setGym(String gym) {
        this.gym = gym;
    }


    public String getHeight() {
        return height;
    }

    public String getGym() {
        return gym;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }


    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }
    public String getCityGym(){
    return getCity()+", " + getGym();
    }
    public String getHeightWeight(){
        return getWeight()+", " + getHeight();
    }

    public String getWeight() {
        return weight;
    }

    public String getQualificationAccount() {
        return qualificationAccount;
    }

    public void setQualificationAccount(String qualificationAccount) {
        this.qualificationAccount = qualificationAccount;
    }

    public String getQualificationAccountMY() {
        return qualificationAccountMY;
    }

    public void setQualificationAccountMY(String qualificationAccountMY) {
        this.qualificationAccountMY = qualificationAccountMY;
    }
}
