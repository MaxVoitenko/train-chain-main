package com.mainvoitenko.trainchain.Account.ApiNet;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mainvoitenko.trainchain.Account.Entity.AccountMy;
import com.mainvoitenko.trainchain.Providers.Net.CheckAchievement;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class MyAccountProvider implements ValueEventListener {

    private String personId;
    private Context context;

    public MyAccountProvider(String personId, Context context) {
        this.personId = personId;
        this.context = context;
    }

    private Callback callback;
    public interface Callback{ void callingBack(AccountMy accountMy, int qualification);}
    public void registerCallBack(Callback callback){ this.callback = callback; }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        AccountMy accountMy =getMyAllData(dataSnapshot);
        int qual = checkUpdateQualificationAccount(dataSnapshot);
        CheckAchievement checkAchievement = new CheckAchievement(dataSnapshot,personId,context);
        checkAchievement.trainer();
        callback.callingBack(accountMy, qual);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    public AccountMy getMyAllData(DataSnapshot dataSnapshot){


        BaseAccountDataProvider baseAccountDataProvider = new BaseAccountDataProvider(personId,dataSnapshot);
        final AccountMy baseAccount = new AccountMy();
        baseAccount.setName(baseAccountDataProvider.loadName());
        ArrayList temp = baseAccountDataProvider.loadCityAndGym();
        baseAccount.setCity(temp.get(0).toString());
        baseAccount.setGym(temp.get(1).toString());
        temp = baseAccountDataProvider.loadHeightAndWeight();
        baseAccount.setHeight(temp.get(0).toString());
        baseAccount.setWeight(temp.get(1).toString());
        baseAccount.setCountMyFollowers(baseAccountDataProvider.loadCountMyFollowers());
        baseAccount.setCountMyFriends(baseAccountDataProvider.loadCountMyFriends());

        baseAccount.setQualificationAccount(baseAccountDataProvider.loadQualAccount());
        baseAccount.setMyUrlAdress(baseAccountDataProvider.loadUrlAdress());
        return baseAccount;
    }

    private int checkUpdateQualificationAccount(DataSnapshot dataSnapshot){
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).exists() &&
                dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).getValue().toString().equals("2")){
            return 2;
//            if(dataSnapshot.child(user.getUid()).child(SettingsConstants.ACCOUNT).child(SettingsConstants.MY_URL_ADRESS).exists()){
//                newUrl.setText(dataSnapshot.child(user.getUid()).child(SettingsConstants.ACCOUNT).child(SettingsConstants.MY_URL_ADRESS).getValue().toString());
//            }
        }else return 1;
    }
}
