package com.mainvoitenko.trainchain.Account.ApiNet;

import com.google.firebase.database.DataSnapshot;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

public class BaseAccountDataProvider {

    private String personId;
    private DataSnapshot dataSnapshot;

    public BaseAccountDataProvider(String personId, DataSnapshot dataSnapshot) {
        this.personId = personId;
        this.dataSnapshot = dataSnapshot;
    }

    public String loadName() {
        String name = "";
        if(dataSnapshot.child(personId).child(SettingsConstants.NAME).exists()) {
            name = dataSnapshot.child(personId).child(SettingsConstants.NAME).getValue().toString();
        }else{
            name = "";}
        return name;
    }

    public String loadQualAccount (){
        String qualificationAccount = "1";
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).exists()){
            qualificationAccount = dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).getValue().toString();
        }else{
            qualificationAccount = "1";
        }
        return qualificationAccount;
    }
    public   String loadTrainCount(){
        String count = "0";
        if (dataSnapshot.child(personId).child(SettingsConstants.TRAINING).exists()) {
            count = String.valueOf(dataSnapshot.child(personId).child(SettingsConstants.TRAINING).getChildrenCount());
        }else {
            count = ("0");
        }
        return count;
    }

    public ArrayList<String> loadCityAndGym(){
        ArrayList<String> cityGym = new ArrayList<>();
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.CITY).exists()){
            cityGym.add(0,dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.CITY).getValue().toString());
        }else{
            cityGym.add(0,"");
        }
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.GYM).exists()) {
            cityGym.add(1,dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.GYM).getValue().toString());
        }else{
            cityGym.add(1,"");
        }
        return cityGym;
    }

    public ArrayList<String> loadHeightAndWeight(){
        ArrayList<String> HeightWeight = new ArrayList<>();
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.HEIGHT).exists()){
            HeightWeight.add(0,dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.HEIGHT).getValue().toString());
        }else{
            HeightWeight.add(0,"0");
        }
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.WEIGHT).exists()) {
            HeightWeight.add(1,dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.WEIGHT).getValue().toString());
        }else{
            HeightWeight.add(1,"0");
        }
        return HeightWeight;
    }
    public String loadUrlAdress(){
        String myUrlAdress = "";
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.MY_URL_ADRESS).exists()){
            myUrlAdress = dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.MY_URL_ADRESS).getValue().toString();
        }else{
            myUrlAdress = "";
        }
        return myUrlAdress;
    }

    public String loadQualificationAdress(){
        String myUrlAdress = "-";
        if(dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).exists()){
            myUrlAdress = dataSnapshot.child(personId).child(SettingsConstants.ACCOUNT).child(SettingsConstants.QUALIFICATION).getValue().toString();
        }else{
            myUrlAdress = "-";
        }
        return myUrlAdress;
    }

    public String loadCountMyFriends(){
        String myUrlAdress = "0";
        if(dataSnapshot.child(personId).child(SettingsConstants.FRIENDS).exists()){
            myUrlAdress = String.valueOf(dataSnapshot.child(personId).child(SettingsConstants.FRIENDS).getChildrenCount());
        }else{
            myUrlAdress = "0";
        }
        return myUrlAdress;
    }
    public String loadCountMyFollowers(){
        String myUrlAdress = "0";
        if(dataSnapshot.child(personId).child(SettingsConstants.FOLLOWERS).exists()){
            myUrlAdress = String.valueOf(dataSnapshot.child(personId).child(SettingsConstants.FOLLOWERS).getChildrenCount());
        }else{
            myUrlAdress = "0";
        }
        return myUrlAdress;
    }
}
