package com.mainvoitenko.trainchain.Interface.Clickers;

import android.view.View;

public interface OnTrainingProgramClickListener {
    void setOnTrainingProgramClickListener(String idTrain, View v, int type, String title, int position);
}
