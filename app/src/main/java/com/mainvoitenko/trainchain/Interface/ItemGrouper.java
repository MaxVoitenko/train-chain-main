package com.mainvoitenko.trainchain.Interface;

public interface ItemGrouper {
        int getItemType();
        String getId();
        int ShowButtonMenu();
}

