package com.mainvoitenko.trainchain.Training.ApiRoom;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.mainvoitenko.trainchain.Training.Entity.HardEx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HardConvertor {
    @TypeConverter
    public String HardListToString(List<HardEx> hardEx){
        List<String> hardStringList = new ArrayList<>();
        for(HardEx hEx: hardEx){
            hardStringList.add(new Gson().toJson(hEx));
        }
        String result =  "";
        for (String s : hardStringList) {
            result += (s + "@@@");
        }
        return result;
    }

    @TypeConverter
    public List<HardEx> StringToHardList(String s){
        List<HardEx> hardList = new ArrayList<>();
        ArrayList<String> stringList = new ArrayList<>(Arrays.asList(s.split("@@@")));
        for(int i=0; i<stringList.size();i++){
            hardList.add(new Gson().fromJson(stringList.get(i), HardEx.class));
        }
        return hardList;
    }
}
