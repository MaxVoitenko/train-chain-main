package com.mainvoitenko.trainchain.Training.Activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.TrainingHelper.TrainingHelper;
import com.mainvoitenko.trainchain.Interface.CallbackInterface.CallBackDialogTrainProgram;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterHardExercise;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.UI.CustomView.CustomChronometrDialog;
import com.mainvoitenko.trainchain.UI.CustomView.TrainProgramBuilderDialog;

import java.util.Calendar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import durdinapps.rxfirebase2.RxFirebaseDatabase;

public class ConstructorFirstPartActivity extends AppCompatActivity implements CallBackDialogTrainProgram {

    @BindView(R.id.addNewExercise) Button addNewExercise;
    @BindView(R.id.statistBut) ImageButton statistBut;
    @BindView(R.id.tonnageNow) TextView tonnageNow;
    @BindView(R.id.kpwNow) TextView kpwNow;
    @BindView(R.id.intencityNow) TextView intencityNow;
    @BindView(R.id.timeForTrain) TextView timeForTrain;
    @BindView(R.id.exCount) TextView exCount;
    @BindView(R.id.myWeightFromProfile) TextView myWeightFromProfile;
    @BindView(R.id.rvExercise) RecyclerView rvExercise;

    private final int MENU_ID = 1;
    private RvAdapterHardExercise rvAdapter;
    private long timeStatic;
    private String curTime;
    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();
    private FullTraining fullTraining;
    private int deleteTrain;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constructor_first_part);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Calendar DataStatic = Calendar.getInstance();
        timeStatic = DataStatic.getTimeInMillis();

        fullTraining = new FullTraining();
        fullTraining.setStartTime(timeStatic);

        rvExercise.setItemViewCacheSize(15);
        rvExercise.setHasFixedSize(true);
        deleteTrain = 0;

        rvAdapter = new RvAdapterHardExercise(fullTraining.getHardEx());
        rvExercise.setAdapter(rvAdapter);

        RxFirebaseDatabase.observeSingleValueEvent(myRef
                .child(user.getUid()).child(SettingsConstants.ACCOUNT+"/"+SettingsConstants.WEIGHT))
                .subscribe(dataSnapshotRxFirebaseChildEvent -> {
                    myWeightFromProfile.setText(dataSnapshotRxFirebaseChildEvent.getValue().toString());
                    fullTraining.setWeightTraining(dataSnapshotRxFirebaseChildEvent.getValue().toString());});


        final AlertDialog.Builder builderTrainPrograms = new AlertDialog.Builder(ConstructorFirstPartActivity.this);
        builderTrainPrograms.setTitle("Сохрененные тренировочные программы");
        builderTrainPrograms.setMessage("Добавить упражнения?");
        builderTrainPrograms.setPositiveButton(R.string.yes_dialog, (dialog, which) -> callFragmentDialog());
        builderTrainPrograms.setNegativeButton(R.string.cancel_dialog, null);
        builderTrainPrograms.show();
    }

    @SuppressLint("CheckResult")
    @Optional @OnClick(R.id.addNewExercise)
    public void addNewExerciseClick(View v) {
        if (fullTraining.getHardEx().size() < 15) {
            fullTraining.getHardEx().add(TrainingHelper.addEmptyHardExercice());
            rvAdapter.notifyItemInserted(fullTraining.getHardEx().size());
            rvExercise.smoothScrollToPosition(fullTraining.getHardEx().size());
            saveToRoom();
        } else { Toast.makeText(this, getResources().getString(R.string.toast), Toast.LENGTH_SHORT).show(); }
    }

    @Optional  @OnClick(R.id.statistBut)
    public void statistButClick() {
        Calendar c = Calendar.getInstance();
        long tempTime = c.getTimeInMillis() - timeStatic;
        curTime = TrainingHelper.bildFormatTime(tempTime);
        timeForTrain.setText(curTime);
        tonnageNow.setText(String.valueOf(TrainingHelper.calcFullTonnage(fullTraining.getHardEx())));
        kpwNow.setText(String.valueOf(TrainingHelper.calcFullKpw(fullTraining.getHardEx())));
        exCount.setText(String.valueOf(fullTraining.getHardEx().size()));
        float intencity = 0f;
        if (TrainingHelper.calcFullKpw(fullTraining.getHardEx()) != 0) {
            intencity = TrainingHelper.calcFullIntencity(fullTraining.getHardEx());
            intencityNow.setText(String.valueOf(TrainingHelper.roundIntencity(intencity)));
        } else {intencityNow.setText("0.0"); }
        fullTraining.setTonnageTraining(String.valueOf(TrainingHelper.calcFullTonnage(fullTraining.getHardEx())));
        fullTraining.setKpw(String.valueOf(TrainingHelper.calcFullKpw(fullTraining.getHardEx())));
        fullTraining.setIntencityTraining(String.valueOf(TrainingHelper.roundIntencity(intencity)));
        fullTraining.setTimeTraining(curTime);
        fullTraining.setCountTraining(String.valueOf(fullTraining.getHardEx().size()));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_for_exercise, menu);
        menu.add(0,MENU_ID, 1, null).setIcon(R.drawable.ic_timer_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.endTrain:
                int temp = 0;
                for (int i = 0; i < fullTraining.getHardEx().size(); i++) {
                    if (fullTraining.getHardEx().get(i).getNameExercise().isEmpty()) { temp = 1;break;}
                    else if (fullTraining.getHardEx().get(i).getCountExercise().get(0) == 0 || fullTraining.getHardEx().get(i).getWeghtExercise().get(0) == 0.0) { temp = 2;break;}
                    else if (fullTraining.getHardEx().get(i).getTimerExercise().isEmpty()) { temp = 3;break;}
                }
                switch (temp){
                    case 1: Toast.makeText(this, R.string.dont_have_name_ex, Toast.LENGTH_SHORT).show();break;
                    case 2:Toast.makeText(this, R.string.dont_have_first_data_ex, Toast.LENGTH_SHORT).show();break;
                    case 3: Toast.makeText(this, R.string.dont_have_time_rest, Toast.LENGTH_SHORT).show();break;
                    default:
                        startActivityForResult(new Intent(ConstructorFirstPartActivity.this, ConstructorSecondPartActivity.class), 1);
                }
                break;
            case MENU_ID:
                CustomChronometrDialog customChronometrDialog = new CustomChronometrDialog(this);
                customChronometrDialog.setCancelable(false);
                customChronometrDialog.show();
                break;
            case R.id.allTrain:
                startActivity(new Intent(this,PreviewMyTrainings.class));
                break;
            case android.R.id.home:
              onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                int endTraining = 0;

                if (data != null) {
                    Bundle bundle = data.getExtras();
                    endTraining =  bundle.getInt(SettingsConstants.END_THIS_TRAIN_INTENT, 0);
                    Intent intentEnd = new Intent();
                    if (endTraining == 1) {
                        intentEnd.putExtra(SettingsConstants.END_THIS_TRAIN_INTENT, 1);
                    }else{
                        intentEnd.putExtra(SettingsConstants.END_THIS_TRAIN_INTENT, 0);
                    }
                        setResult(RESULT_OK, intentEnd);
                    finish();

                }
            }
        }
    }

    @Override
    public void onBackPressed() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.training_exit_title);
            builder.setMessage(R.string.training_exit);
            builder.setPositiveButton(R.string.yes_dialog_preview, (dialog, which) -> {
                    TrainingHelper.deleteTrainFromRoom(getApplicationContext());
                    deleteTrain=1;
                    finish();
            });
            builder.setNegativeButton(R.string.not_dialog, (dialog, which) -> finish());
            builder.show();
    }

    private void callFragmentDialog(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        TrainProgramBuilderDialog dialogFragment = new TrainProgramBuilderDialog();
        dialogFragment.setCallBackDialogTrainProgram(this);
        dialogFragment.show(ft, "dialog");
    }

    @Override
    public void OnCallBackDialogTrainProgram(String programDay) {
            fullTraining.getHardEx()
                    .addAll(TrainingHelper.ProgramToTraining(programDay));
            rvAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onPause() {
        if(deleteTrain != 1){
        saveToRoom();
        }
        super.onPause();
    }

    private  void  saveToRoom(){
        this.statistButClick();
        fullTraining.setTonnageTraining(tonnageNow.getText().toString());
        fullTraining.setIntencityTraining(intencityNow.getText().toString());
        fullTraining.setKpw(kpwNow.getText().toString());
        fullTraining.setCountTraining(exCount.getText().toString());
        fullTraining.setWeightTraining(myWeightFromProfile.getText().toString());
        fullTraining.setStartTime(timeStatic);
        TrainingHelper.insertTrainToRoom(getApplicationContext(), fullTraining).subscribe();
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onStart() {
        super.onStart();
        TrainingHelper.getTrainFromRoom(getApplicationContext())
                .subscribe(trainingOtherDataHere -> {
                    TrainingHelper.updateAllData(fullTraining,trainingOtherDataHere);
                    if(trainingOtherDataHere.getHardEx().get(0)!=null){
                        rvExercise.setAdapter(rvAdapter);
                        rvAdapter.notifyDataSetChanged();
                    }
                }, throwable -> {});
    }
}





