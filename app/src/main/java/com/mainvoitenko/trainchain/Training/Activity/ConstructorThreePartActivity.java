package com.mainvoitenko.trainchain.Training.Activity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.TrainingHelper.TrainingHelper;
import com.mainvoitenko.trainchain.Providers.GetPermissions;
import com.mainvoitenko.trainchain.Providers.PhotoRedactor;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SingleTonUser;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class ConstructorThreePartActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {


    @BindView(R.id.mainLiner) LinearLayout mainLiner;
    @BindView(R.id.cardioLiner) LinearLayout cardioLiner;
    @BindView(R.id.textTime) EditText textTime;
    @BindView(R.id.data_person_post) TextView data_person_post;
    @BindView(R.id.tonnageNow) TextView tonnageNow;
    @BindView(R.id.kpwNow) TextView kpwNow;
    @BindView(R.id.intencityNow) TextView intencityNow;
    @BindView(R.id.timeForTrain) TextView timeForTrain;
    @BindView(R.id.exCount) TextView exCount;
    @BindView(R.id.myWeightFromProfile) TextView myWeightFromProfile;
    @BindView(R.id.seekBar) SeekBar seekBar;
    @BindView(R.id.picture_training) RoundedImageView getMyPic;

    private FirebaseUser user = SingleTonUser.getInstance().getUser();
    private DatabaseReference myRef = SingleTonUser.getInstance().getMyRef();

    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private String currentDateandTime;
    private int emotion = 2;
    private String currentPhotoPath;
    private String path;
    private String mImageUri = "";
    private String bitmapUri = "-1";
    private PhotoRedactor photoRedactor;
    private int deleteTrain;

    private FullTraining fullTraining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constructor_three_part);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        seekBar.setOnSeekBarChangeListener(this);
        photoRedactor = new PhotoRedactor(this);
        fullTraining = new FullTraining();
        deleteTrain = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.okay_change_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("CheckResult")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Name_Yes:

                if(fullTraining.getHardEx().size() == 0 && fullTraining.getCardioEx().size() == 0){
                    deleteTrain=1;
                        TrainingHelper.deleteTrainFromRoom(getApplicationContext());
                            Intent intentBack = new Intent();
                            setResult(RESULT_OK, intentBack);
                            intentBack.putExtra(SettingsConstants.END_THIS_TRAIN_INTENT, 0);
                            finish();
                }else{
                    fullTraining.setDataTraining(data_person_post.getText().toString());
                    fullTraining.setEmotion(String.valueOf(emotion));
                    TrainingHelper.insertTrainToRoom(getApplicationContext(), fullTraining).subscribe(() -> {
                        myRef.child(user.getUid()).child(SettingsConstants.ACCOUNT).child(SettingsConstants.WEIGHT)
                                .setValue(fullTraining.getWeightTraining());
                        Intent intentBack = new Intent();
                        setResult(RESULT_OK, intentBack);
                        intentBack.putExtra(SettingsConstants.END_THIS_TRAIN_INTENT, 1);
                        finish();
                    });

                }
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Optional @OnClick(R.id.textTime)
    public void textTimeClick(View v){
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, (view, hourOfDay, minute) -> {
        textTime.setText(String.format(("%02d:%02d"), hourOfDay, minute));
        timeForTrain.setText(String.format(("%02d:%02d"), hourOfDay, minute));
        }, Integer.parseInt(textTime.getText().toString().substring(0, 2))
                , Integer.parseInt(textTime.getText().toString().substring(3, 5)), true);
        timePickerDialog.show();
        }


    @Optional  @OnClick(R.id.picture_training)
    public void pictureTrainingClick(View v){
        Toast.makeText(this, R.string.wait_fr, Toast.LENGTH_SHORT).show();
        if(GetPermissions.checkPermissions(this)) {
            addPhoto();
        }
    }

    private void addPhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = photoRedactor.createImageFile();
            currentPhotoPath = photoFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (photoFile != null && takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.mainvoitenko.trainchain.fileprovider", photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (progress){
            case 0:seekBar.setThumb(getResources().getDrawable(R.drawable.ic_emotion1));
                emotion= 0;break;
            case 1:seekBar.setThumb(getResources().getDrawable(R.drawable.ic_emotion2));
                emotion= 1;break;
            case 2:seekBar.setThumb(getResources().getDrawable(R.drawable.ic_emotion3));
                emotion= 2;break;
            case 3:seekBar.setThumb(getResources().getDrawable(R.drawable.ic_emotion4));
                emotion= 3;break;
            case 4:seekBar.setThumb(getResources().getDrawable(R.drawable.ic_emotion5));
                emotion= 4;break;
        }
    }
    @Override public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override public void onStopTrackingTouch(SeekBar seekBar) {}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
          if (mImageUri != null) {
              photoRedactor.showFiles(getFilesDir());
                bitmapUri = photoRedactor.compressBitmap(Uri.parse(currentPhotoPath), path).toString();
                Picasso.with(this).load(Uri.parse(bitmapUri)).into(getMyPic);
          }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isCameraPermissionGranted = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean isWritePermissionGranted = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if(isCameraPermissionGranted && isWritePermissionGranted) {
            addPhoto();
        }
    }

private void updateActivityData(){
    Calendar c = Calendar.getInstance();
    long tempTime = c.getTimeInMillis() - fullTraining.getStartTime();
    Log.d("mLog", fullTraining.getStartTime()+"");
    @SuppressLint("DefaultLocale") String curTime = String.format("%02d:%02d",tempTime / 3600000,(tempTime / 60000)-(tempTime / 3600000)*60);
    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
    currentDateandTime = sdf.format(new Date());

    textTime.setText(curTime);
    data_person_post.setText(currentDateandTime);
    tonnageNow.setText(fullTraining.getTonnageTraining());
    kpwNow.setText(fullTraining.getKpw());
    intencityNow.setText(fullTraining.getIntencityTraining());
    timeForTrain.setText(curTime);
    exCount.setText(fullTraining.getCountTraining());
    myWeightFromProfile.setText(fullTraining.getWeightTraining());


    mainLiner.removeAllViews();
    cardioLiner.removeAllViews();
    for(int ex = 0; ex< fullTraining.getHardEx().size(); ex++){ //спискок силовых
        final View view = getLayoutInflater().inflate(R.layout.conteiner_preview, null);
        TextView[] count = new TextView[SettingsConstants.COUNT_FOR_HARD_EXERCISE];
        TextView[] weight= new TextView[SettingsConstants.COUNT_FOR_HARD_EXERCISE];
        count[0] = view.findViewById(R.id.count1);
        count[1] = view.findViewById(R.id.count2);
        count[2] = view.findViewById(R.id.count3);
        count[3] = view.findViewById(R.id.count4);
        count[4] = view.findViewById(R.id.count5);
        count[5] = view.findViewById(R.id.count6);
        weight[0] = view.findViewById(R.id.weight1);
        weight[1] = view.findViewById(R.id.weight2);
        weight[2] = view.findViewById(R.id.weight3);
        weight[3] = view.findViewById(R.id.weight4);
        weight[4] = view.findViewById(R.id.weight5);
        weight[5] = view.findViewById(R.id.weight6);
        TextView timerExerciseBtn = view.findViewById(R.id.timerExerciseBtn);
        TextView numberExercise = view.findViewById(R.id.numberExercise);
        TextView nameExercise = view.findViewById(R.id.nameExercise);

        List<Integer> cnt = fullTraining.getHardEx().get(ex).getCountExercise();
        List<Float> wgh = fullTraining.getHardEx().get(ex).getWeghtExercise();
        for (int i = 0; i< SettingsConstants.COUNT_FOR_HARD_EXERCISE; i++) {
            count[i].setText(String.valueOf(cnt.get(i)));
            weight[i].setText(String.valueOf(wgh.get(i)));
        }
        numberExercise.setText(String.valueOf(ex+1));
        nameExercise.setText(fullTraining.getHardEx().get(ex).getNameExercise());
        timerExerciseBtn.setText(String.valueOf(fullTraining.getHardEx().get(ex).getTimerExercise()));
        mainLiner.addView(view);
    }
    for(int crdo = 0; crdo < fullTraining.getCardioEx().size(); crdo++) { //список кардио
        final View view = getLayoutInflater().inflate(R.layout.conteiner_preview_two, null);
        TextView numbCardio = view.findViewById(R.id.numbCardio);
        TextView textCardio = view.findViewById(R.id.textCardio);
        TextView timeCardio = view.findViewById(R.id.timeCardio);

        numbCardio.setText(String.valueOf(crdo+1));
        textCardio.setText(fullTraining.getCardioEx().get(crdo).getName());
        timeCardio.setText(String.valueOf(fullTraining.getCardioEx().get(crdo).getTime()));
        cardioLiner.addView(view);
    }
}

    @SuppressLint("CheckResult")
    @Override
    protected void onStart() {
        super.onStart();
        TrainingHelper.getTrainFromRoom(getApplicationContext())
                .subscribe(trainingOtherDataHere -> {
                    TrainingHelper.updateAllData(fullTraining,trainingOtherDataHere);
                    updateActivityData();
                }, throwable -> {});
    }

    @Override
    protected void onPause() {
        if(deleteTrain != 1) {
            fullTraining.setDataTraining(currentDateandTime);
            fullTraining.setEmotion(String.valueOf(emotion));
            TrainingHelper.insertTrainToRoom(getApplicationContext(), fullTraining).subscribe();
        }
        super.onPause();
    }

}
