package com.mainvoitenko.trainchain.Training.Activity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.TrainingHelper.TrainingHelper;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterCardioExercise;
import com.mainvoitenko.trainchain.UI.Adapters.RvAdapterCardioExerciseImage;
import com.mainvoitenko.trainchain.Interface.Clickers.OnImageCardioClick;
import com.mainvoitenko.trainchain.Model.ImageCardio;
import com.mainvoitenko.trainchain.Training.Entity.CardioEx;
import com.mainvoitenko.trainchain.R;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class ConstructorSecondPartActivity extends AppCompatActivity   implements OnImageCardioClick {



    @BindView(R.id.nameCardioBut) EditText nameCardioBut;
    @BindView(R.id.timeCardioBut) EditText timeCardioBut;
    @BindView(R.id.addCardio) Button addCardio;
    @BindView(R.id.textWeight) EditText textWeight;
    @BindView(R.id.rvCardio) RecyclerView rvCardio;
    @BindView(R.id.cardioExerciceAlreadyReady) RecyclerView cardioExerciceAlreadyReady;

    private String name;
    private String time;

    private RvAdapterCardioExercise rvAdapterCardio;
    private RvAdapterCardioExerciseImage rvAdapterImageCardio;
    private ArrayList<ImageCardio> exerciseImages = new ArrayList<>();
    private FullTraining fullTraining;


    @Override
    public void onImageClickForName(int tempPosition) {
     nameCardioBut.setText(exerciseImages.get(tempPosition).getName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constructor_two_part);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fullTraining = new FullTraining();
        rvAdapterCardio = new RvAdapterCardioExercise(fullTraining.getCardioEx());
        rvCardio.setItemViewCacheSize(5);
        rvCardio.setHasFixedSize(true);
        cardioExerciceAlreadyReady.setHasFixedSize(true);

        rvCardio.setAdapter(rvAdapterCardio);
        rvAdapterCardio.notifyDataSetChanged();

        rvAdapterImageCardio= new RvAdapterCardioExerciseImage(exerciseImages,this);
        TrainingHelper.loadImageCardioDataFromResoure(this,exerciseImages);
        cardioExerciceAlreadyReady.setAdapter(rvAdapterImageCardio);
        rvAdapterImageCardio.notifyDataSetChanged();
    }


    @Optional  @OnClick(R.id.addCardio)
    public void addCardioClick(View v) {
        if(fullTraining.getCardioEx().size()<5) {
            if(!nameCardioBut.getText().toString().equals("") && !timeCardioBut.getText().toString().equals("")){
                name = nameCardioBut.getText().toString();
                time = timeCardioBut.getText().toString();
                fullTraining.getCardioEx().add(new CardioEx(name, time));
                rvAdapterCardio.notifyItemInserted(fullTraining.getCardioEx().size());
                rvCardio.smoothScrollToPosition(fullTraining.getCardioEx().size());
                TrainingHelper.insertTrainToRoom(getApplicationContext(), fullTraining);
                nameCardioBut.setText("");
                timeCardioBut.setText("");
            }else{Toast.makeText(this, getResources().getString(R.string.toast2), Toast.LENGTH_SHORT).show();}
        }else{Toast.makeText(this, getResources().getString(R.string.toast), Toast.LENGTH_SHORT).show();}
    }

    @Optional  @OnClick(R.id.timeCardioBut)
    public void timeCardioButClick(View v) {
        TimePickerDialog timePickerDialog2=  new TimePickerDialog(this, (view, hourOfDay, minute) -> timeCardioBut.setText(String.format(("%02d:%02d"),hourOfDay,minute)),00,00,true);
        timePickerDialog2.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_for_exercise, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.endTrain:
                startActivityForResult(new Intent(ConstructorSecondPartActivity.this, ConstructorThreePartActivity.class), 1);
                break;
            case R.id.allTrain:
                Intent intent =new Intent(this,PreviewMyTrainings.class);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    int endTraining =  bundle.getInt(SettingsConstants.END_THIS_TRAIN_INTENT, 0);
                    Intent intentEnd = new Intent();
                    if (endTraining== 1) {
                        intentEnd.putExtra(SettingsConstants.END_THIS_TRAIN_INTENT, 1);
                    }else{
                        intentEnd.putExtra(SettingsConstants.END_THIS_TRAIN_INTENT, 0);
                    }
                    setResult(RESULT_OK, intentEnd);
                    finish();
                }
            }

        }
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onStart() {
        super.onStart();
        TrainingHelper.getTrainFromRoom(getApplicationContext())
                .subscribe(trainingOtherDataHere -> {
                    TrainingHelper.updateAllData(fullTraining,trainingOtherDataHere);
                    if(trainingOtherDataHere.getCardioEx().get(0)!=null){
                        rvCardio.setAdapter(rvAdapterCardio);
                        rvAdapterCardio.notifyDataSetChanged();
                    }
                    textWeight.setText(fullTraining.getWeightTraining());
                }, throwable -> {});
    }


    @Override
    protected void onPause() {
        fullTraining.setWeightTraining(textWeight.getText().toString());
        TrainingHelper.insertTrainToRoom(getApplicationContext(), fullTraining).subscribe();
        super.onPause();
    }
}