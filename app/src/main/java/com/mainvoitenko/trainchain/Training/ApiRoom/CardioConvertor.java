package com.mainvoitenko.trainchain.Training.ApiRoom;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import com.google.gson.Gson;
import com.mainvoitenko.trainchain.Training.Entity.CardioEx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CardioConvertor {

    @TypeConverter
    public List<CardioEx> StringToCardioList(String s){
        List<CardioEx> cardioList = new ArrayList<>();
        ArrayList<String> stringList = new ArrayList<>(Arrays.asList(s.split("@@@")));
        for(int i=0; i<stringList.size();i++){
            cardioList.add(new Gson().fromJson(stringList.get(i), CardioEx.class));
        }
        return cardioList;
    }
    @TypeConverter
    public String CardioListToString(List<CardioEx> cardioEx){
        List<String> cardioStringList = new ArrayList<>();
        for(CardioEx cEx: cardioEx){
            cardioStringList.add(new Gson().toJson(cEx));
        }
        StringBuilder result = new StringBuilder();
        for (String s : cardioStringList) {
            result.append(s).append("@@@");
        }
        return result.toString();
    }
}
