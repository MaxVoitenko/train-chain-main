package com.mainvoitenko.trainchain.Training.Entity;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.TypeConverter;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.gson.Gson;

@IgnoreExtraProperties
public class CardioEx {

    @PropertyName("Name")
    public String name;
    @PropertyName("Time")
    public String time;

    public CardioEx(String name, String time){
        this.name = name;
        this.time = time;
    }
    public CardioEx(){}

    @Exclude
    public String getName() {
        return name;
    }
    @Exclude
    public String getTime() {
        return time;
    }
    @Exclude
    public void setName(String name) {
        this.name = name;
    }
    @Exclude
    public void setTime(String time) {
        this.time = time;
    }

}
