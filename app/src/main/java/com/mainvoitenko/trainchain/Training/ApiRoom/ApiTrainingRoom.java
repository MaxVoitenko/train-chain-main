package com.mainvoitenko.trainchain.Training.ApiRoom;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.mainvoitenko.trainchain.Training.Entity.FullTraining;

import io.reactivex.Single;

@Database(entities = {FullTraining.class}, version = 1, exportSchema = false)
public abstract class ApiTrainingRoom extends RoomDatabase {
    @Dao
    public interface DaoTraining {

        @Query("SELECT * FROM FullTraining WHERE keyRoom = :key")
        Single<FullTraining> getTraining(int key);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(FullTraining item);

        @Query("DELETE FROM FullTraining WHERE keyRoom = :userId")
        void deleteByUserId(long userId);
    }

    private static volatile ApiTrainingRoom instance;//Синглтон проверка создан ли
    public  static ApiTrainingRoom getInstance(Context context) { //проверка на синглтон
        if (instance == null)
            synchronized (ApiTrainingRoom.class) {
                if (instance == null)
                    instance = Room.databaseBuilder(context,
                            ApiTrainingRoom.class,
                            "database").build();
            }
        return instance;
    }
    public ApiTrainingRoom() {}
    public static void destroyInstance() { instance= null; }
    public abstract DaoTraining daoTraining();
}
