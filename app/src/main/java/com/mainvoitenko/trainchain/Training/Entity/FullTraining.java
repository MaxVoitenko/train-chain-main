package com.mainvoitenko.trainchain.Training.Entity;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.mainvoitenko.trainchain.Interface.ItemGrouper;
import com.mainvoitenko.trainchain.Settings.SettingsConstants;
import com.mainvoitenko.trainchain.Training.ApiRoom.CardioConvertor;
import com.mainvoitenko.trainchain.Training.ApiRoom.HardConvertor;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@Entity
@IgnoreExtraProperties
public class FullTraining implements ItemGrouper{
    @Exclude
    @PrimaryKey @NotNull
    private int keyRoom;

    public String Kpw;
    @PropertyName("Data")
    public String dataTraining;
    @PropertyName("Tonnage")
    public String tonnageTraining;
    @PropertyName("Intencity")
    public String intencityTraining;
    @PropertyName("CountAllExercise")
    public String countTraining;
    @PropertyName("myWeight")
    public String weightTraining;
    @PropertyName("TimeTrain")
    public String timeTraining;
    @PropertyName("EmotionTrain")
    public String emotion;

    @PropertyName("HardEx")
    @TypeConverters({HardConvertor.class})
    public List<HardEx> hardEx;
    @PropertyName("CardioEx")
    @TypeConverters({CardioConvertor.class})
    public List<CardioEx> cardioEx;

    @Exclude
    private String bitmapUri;
    @Exclude
    private Long startTime;
    @Exclude @Ignore
    private String idTrain;
    @Exclude @Ignore
    private String internetId;
    @Exclude @Ignore
    private int showMenu;
    @Exclude @Ignore
    private int itemType;

    public FullTraining(){
        this.hardEx = new ArrayList<>();
        this.cardioEx = new ArrayList<>();
        this.keyRoom = -1;
        this.weightTraining= "0";
        this.countTraining = "0";
        this.tonnageTraining = "0.0";
        this.Kpw = "0";
        this.intencityTraining = "0.0";
        this.timeTraining = "0";
        this.startTime = 0L;
        this.dataTraining= "0";
        this.emotion = "2";
    }

    @Ignore
    public FullTraining(ArrayList<String> data){
        this.dataTraining = data.get(0);
        this.Kpw = data.get(1);
        this.tonnageTraining = data.get(2);
        this.intencityTraining = data.get(3);
        this.countTraining = data.get(4);
        this.weightTraining = data.get(5);
        this.timeTraining = data.get(6);
        this.emotion = data.get(7);
        this.bitmapUri = data.get(8);
    }
    @Ignore
    public FullTraining(String dataTraining, String Kpw,
                        String tonnageTraining, String intencityTraining,
                        String countTraining, String weightTraining,
                        String timeTraining, String emotion,
                        String bitmapUri) {
        this.dataTraining = dataTraining;
        this.Kpw = Kpw;
        this.tonnageTraining = tonnageTraining;
        this.intencityTraining = intencityTraining;
        this.countTraining = countTraining;
        this.weightTraining = weightTraining;
        this.timeTraining = timeTraining;
        this.emotion = emotion;
        this.bitmapUri = bitmapUri;
    }


    @Exclude
    public Long getStartTime() {
        return startTime;
    }
    @Exclude
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    @Exclude
    public String getDataTraining() {
        return dataTraining;
    }
    @Exclude
    public void setDataTraining(String dataTraining) {
        this.dataTraining = dataTraining;
    }
    @Exclude
    public String getKpw() {
        return Kpw;
    }
    @Exclude
    public void setKpw(String kpw) {
        this.Kpw = kpw;
    }
    @Exclude
    public String getTonnageTraining() {
        return tonnageTraining;
    }
    @Exclude
    public void setTonnageTraining(String tonnageTraining) {
        this.tonnageTraining = tonnageTraining;
    }
    @Exclude
    public String getIntencityTraining() {
        return intencityTraining;
    }
    @Exclude
    public void setIntencityTraining(String intencityTraining) {
        this.intencityTraining = intencityTraining;
    }
    @Exclude
    public String getCountTraining() {
        return countTraining;
    }
    @Exclude
    public void setCountTraining(String countTraining) {
        this.countTraining = countTraining;
    }
    @Exclude
    public String getWeightTraining() {
        if(weightTraining== null || weightTraining.isEmpty() || weightTraining.equals("")){
            weightTraining = "0";
        }
        return weightTraining;
    }
    @Exclude
    public void setWeightTraining(String weightTraining) {
        this.weightTraining = weightTraining;
    }
    @Exclude
    public String getTimeTraining() {
        return timeTraining;
    }
    @Exclude
    public void setTimeTraining(String timeTraining) {
        this.timeTraining = timeTraining;
    }
    @Exclude
    public String getEmotion() {
        if(emotion == null || emotion.isEmpty()){
            emotion="2";
        }
        return emotion;
    }
    @Exclude
    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }
    @Exclude
    public String getBitmapUri() {
        return bitmapUri;
    }
    @Exclude
    public void setBitmapUri(String bitmapUri) {
        this.bitmapUri = bitmapUri;
    }
    @Exclude
    public List<HardEx> getHardEx() {
        return hardEx;
    }
    @Exclude
    public void setHardEx(List<HardEx> hardEx) {
        this.hardEx = hardEx;
    }
    @Exclude
    public List<CardioEx> getCardioEx() {
        return cardioEx;
    }
    @Exclude
    public void setCardioEx(List<CardioEx> cardioEx) {
        this.cardioEx = cardioEx;
    }
    @Exclude
    @NotNull
    public int getKeyRoom() {return keyRoom; }
    @Exclude
    public void setKeyRoom(@NotNull int keyRoom) {
        this.keyRoom = keyRoom;
    }
    @Exclude
    @Override
    public int getItemType() {
        return itemType;
    }
    @Exclude
    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Exclude
    @Override
    public String getId() {
        return internetId;
    }
    @Exclude
    @Override
    public int ShowButtonMenu() {
        return showMenu;
    }
    @Exclude @Ignore
    public void setInternetId(String internetId) {
        this.internetId = internetId;
    }
    @Exclude @Ignore
    public String getInternetId() {
        return internetId;
    }

    @Exclude @Ignore
    public void setShowMenu(int showMenu) {
        this.showMenu = showMenu;
    }
}