package com.mainvoitenko.trainchain.Training.TrainingHelper;

import android.content.Context;
import android.content.res.TypedArray;

import com.mainvoitenko.trainchain.Training.ApiRoom.ApiTrainingRoom;
import com.mainvoitenko.trainchain.Training.Entity.FullTraining;
import com.mainvoitenko.trainchain.Training.Entity.HardEx;
import com.mainvoitenko.trainchain.Model.ImageCardio;
import com.mainvoitenko.trainchain.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TrainingHelper {

    //hard
    public static float calcFullTonnage(List<HardEx> hardExList) {
        float fullTonnage=0;
        for(HardEx hEx: hardExList){
            fullTonnage = fullTonnage + hEx.getTonnageExercise();
        }
        return fullTonnage;
    }
    public static int calcFullKpw(List<HardEx> hardExList) {
        int fullKpw=0;
        for(HardEx hEx: hardExList){
            fullKpw = fullKpw + hEx.getKpwExercise();
        }
        return fullKpw;
    }
    public static float calcFullIntencity(List<HardEx> hardExList) {
        return calcFullTonnage(hardExList) / TrainingHelper.calcFullKpw(hardExList);
    }
    public static float roundIntencity(float number) {
        int pow = (int) Math.pow(10, 1);
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
    public static String bildFormatTime(long tempTime ){
        return String.format("%02d:%02d",tempTime / 3600000,(tempTime / 60000)-(tempTime / 3600000)*60);
    }
    public static List<HardEx> ProgramToTraining(String programDay) {
        List<HardEx> hardExes = new ArrayList<>();
        if (programDay != null) {
            for (int i = 1; i < programDay.split("\n").length; i++) {
                if (programDay.indexOf(i + ". ") > 0) {
                    String result = programDay.substring(programDay.indexOf(i + ". ") + 2);
                    String nameResult = result.substring(0, result.indexOf("\n"));
                    String exercise;
                    int tmp = i + 1;
                    if (!result.contains("\n" + tmp)) {
                        exercise = result.substring(nameResult.length() + 1);
                    } else {
                        exercise = result.substring(nameResult.length() + 1, result.indexOf("\n" + tmp));
                    }
                    exercise = exercise.replaceAll("\\s+", "");
                    exercise = exercise.replaceAll("1x", "");
                    Pattern pattern = Pattern.compile(",");
                    String[] resl = pattern.split(exercise);
                    List<Integer> reslInt = new ArrayList<>();
                    for (int g = 0; g < resl.length; g++) {
                        reslInt.add(g, Integer.parseInt(resl[g]));
                    }
                    List<Integer> tempCount = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0));
                    if (reslInt.size() < 6) {
                        for (int u = 0; u < 6; u++) {
                            if (reslInt.size() > u) {
                                tempCount.set(u, reslInt.get(u));
                            }
                        }
                    }
                    hardExes.add(new HardEx(nameResult, new ArrayList<>(Arrays.asList(0F, 0F, 0F, 0F, 0F, 0F)), tempCount, ""));

                }
            }
        }
        return hardExes;
    }
    public static HardEx addEmptyHardExercice(){
        return new HardEx("",
                new ArrayList<>(Arrays.asList(0F, 0F, 0F, 0F, 0F, 0F)),
                new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0)),
                "");
    }
    //hard

    //cardio
    public   static  void loadImageCardioDataFromResoure(Context context, ArrayList<ImageCardio> exerciseImages) {
        String[] cardio_exercise_name = context.getResources().getStringArray(R.array.cardio_exercise); //имена
        TypedArray cardio_exercise_image = context.getResources().obtainTypedArray(R.array.cardio_exercise_image);//картинки
        for(int i=0; i<cardio_exercise_name.length; i++){
            exerciseImages.add(new ImageCardio(cardio_exercise_name[i], cardio_exercise_image.getDrawable(i)));
        }
    }

    //cardio
    public static void updateAllData(FullTraining oldData, FullTraining newData){
            if (newData.getCardioEx().get(0) != null) {
                oldData.getCardioEx().clear();
                oldData.getCardioEx().addAll(newData.getCardioEx());
            }
            if (newData.getHardEx().get(0) != null) {
                oldData.getHardEx().clear();
                oldData.getHardEx().addAll(newData.getHardEx());
            }
            if (newData.getWeightTraining() != null)
                oldData.setWeightTraining(newData.getWeightTraining());
            if (newData.getCountTraining() != null)
                oldData.setCountTraining(newData.getCountTraining());
            if (newData.getTonnageTraining() != null)
                oldData.setTonnageTraining(newData.getTonnageTraining());
            if (newData.getKpw() != null)
                oldData.setKpw(newData.getKpw());
            if (newData.getIntencityTraining() != null)
                oldData.setIntencityTraining(newData.getIntencityTraining());
            if (newData.getTimeTraining() != null)
                oldData.setTimeTraining(newData.getTimeTraining());
            if (newData.getStartTime() != null)
                oldData.setStartTime(newData.getStartTime());
            if(newData.getDataTraining() != null)
                oldData.setDataTraining(newData.getDataTraining());
            if(newData.getEmotion() != null)
                oldData.setEmotion(newData.getEmotion());
    }

    //roomHelper
        public static Single<FullTraining> getTrainFromRoom(Context context) {
            return ApiTrainingRoom.getInstance(context)
                    .daoTraining()
                    .getTraining(-1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
        public static void deleteTrainFromRoom(Context context) {
            Completable.fromAction(() -> ApiTrainingRoom.getInstance(context)
                    .daoTraining()
                    .deleteByUserId(-1))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
        public static Completable insertTrainToRoom(Context context, FullTraining fullTraining) {
            return Completable.fromAction(() -> ApiTrainingRoom.getInstance(context)
                    .daoTraining()
                    .insert(fullTraining))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    //roomHelper

}
